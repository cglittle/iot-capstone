import os.path
import configparser
import traceback
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
import newapi
from threading import Thread, Semaphore
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime
import livestream 
CONFIG_FILE = 'iot4config.ini'
MAIN_CONFIG_FILE = 'pi_setup.ini'
import time




print_sem = Semaphore(1)

old_print = print
def print(*args, **kw):
    print_sem.acquire()
    old_print(*args, **kw)
    print_sem.release()



latest_images = {}

class CustomStreamingHandler(livestream.StreamingHandler):
    def path_terminator(self):
    
        # https://localhost:8001/Printer 001  'Printer 001'
        self.printer_name = self.path[len('/'):].replace('%20', ' ')

        print('Printer name:', repr(self.printer_name))
        print('Latest images:', list(latest_images))
        
        if self.printer_name not in latest_images:
            self.send_error(404, 'No such printer!')
            return False
        self.next_image_num = 0
        return True
        


    def callback(self):
        # filename
        while True:
            image_data, image_num = latest_images[self.printer_name]
            if image_num < self.next_image_num:
                time.sleep(0.05)
                continue
            self.next_image_num = image_num + 1
            return image_data

        
    

def streaming_thread():
    address = ('', 8001)
    server = livestream.StreamingServer(address, CustomStreamingHandler)
    server.serve_forever()

t = Thread(target = streaming_thread, args = (), daemon = True)
t.start()

# Control if we are sending data to the server
#SEND_TO_SERVER = True


# Create a class for makerbot printers
class MakerbotPrinter:
    def __init__(self, ip, auth_code):
        self.ip = ip
        self.auth_code = auth_code
        self.ready = False

    def connect(self):
        self.makerbot = newapi.MakerbotAPI(self.ip, self.auth_code)
        print('Connecting...', self.ip)
        self.makerbot.connect()
        print('Authenticating...')
        self.makerbot.authenticate_json_rpc()
        print('Getting system info...')
        self.name = self.makerbot.get_system_information()["result"]["machine_name"]
        self.ready = True

    # send the info of the Makerbot printer
    def send_info(self, current_printers):
        try:
            print('About to connect!')
            self.connect()
            print('Loop.')
            makerbot_printer_send(self.name, self.makerbot, current_printers)
        except Exception as e:
            print(traceback.format_exc())
    def send_camera_stream(self):
        image_num = 0
        while not self.ready:
            pass
        while True:
            # Get latest image
            _, width, height, _, yuv_image = self.makerbot.get_camera_image()
            latest_images[self.name] = (yuv_image, image_num)
            image_num += 1
           





#USE_CONFIG_FILE = False



config = configparser.ConfigParser()
if os.path.exists(CONFIG_FILE):
    config.read(CONFIG_FILE)

influxdb_instances = {}

print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    influxdb_instances[name] = config[name]
    
config = configparser.ConfigParser()
if os.path.exists(MAIN_CONFIG_FILE):
    config.read(MAIN_CONFIG_FILE)

SEND_TO_SERVER = config['DEFAULT']['send_to_server']
BROADCAST_INTERFACE = config['DEFAULT'].get('broadcast_interface')
if BROADCAST_INTERFACE is not None:
    BROADCAST_INTERFACE = BROADCAST_INTERFACE == 'True'
authentication_dict = {}
print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    print(name)
    printer_info = config[name]
    print(printer_info)
    
    if printer_info['enable'] == 'True':
        authentication_dict[name] = printer_info['auth']     
    
    



if not influxdb_instances:
    raise Exception('No influxdb database listed!')

#influxdb_instances = {}

# Always send to local influxdb.


for name, use_info in influxdb_instances.items():
    bucket = use_info['bucket']
    org = use_info['org']
    token = use_info['token']
    url = use_info['url']
    print()
    print('InfluxDB info:')
    print(repr(name))
    print('Using Bucket:', repr(bucket))
    print('Using Org:', repr(org))
    print('Using Token:', repr(token))
    print('Using url:', repr(url))
    print()


# Step Encodings
STEP_NUMBERS = {'idle': 0,
                'clear_build_plate': 7,
                'initial_heating': 1,
                'homing': 2,
                'transfer': 10,
                'final_heating': 3,
                'printing': 4,
                'cleaning_up': 5,
                'completed': 6,
                'cooling': 8,
                'cancelling': 9,
                'verify_firmware': 11,
                'writing': 12,
                'suspended': 13,  # 0
                'suspending': 14,
                'unloading_filament': 15,
                'handling_recoverable_filament_jam': 16,
                'preheating_resuming': 17,
                }
# extrusion, 


influx_sem = Semaphore(1)
    
def makerbot_printer_send(name, machine, current_printers):
    try:
        #image_num = 0
        prev_step = -1
        while True:
            print('Getting system info...')
            # Retrieve data from system
            data = machine.get_system_information()
            
            print("Got system info")
            print(data["result"])#["machine_name"])
            if not SEND_TO_SERVER:
                time.sleep(30)
                continue
            current_process = data['result']['current_process']
            # Send the name of the device and the extruder temperature to influxdb
            p = influxdb_client.Point("send efficient 2") \
                .tag("Name", data["result"]["machine_name"]) \
                .field("Target Temperature", data["result"]["toolheads"]["extruder"][0]["target_temperature"]) \
                .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"])

            # Set the step to be idle and percentage done to be 0 as default

            step_name = 'idle'
            perc_done = 0

            # if the printer is printing get the step and the percentage done of the job
            if current_process is not None:
                step_name = current_process['step']
                perc_done = current_process.get('progress', 0)
                print_job_name = current_process.get('filepath')
                if print_job_name is not None:
                    print_job_name = os.path.split(print_job_name)
                    print_job_name = print_job_name[1]
                time_remaining = current_process.get('time_remaining')
                time_estimation = current_process.get('time_estimation')
                filiment_weight = current_process.get('extrusion_mass_g')
                if filiment_weight is not None:
                    filiment_weight = filiment_weight[0]
                time_elapsed = current_process.get('elapsed_time')
                p = p.field('Print Job Name', print_job_name) \
                    .field('Time Remaining', time_remaining) \
                    .field('Time Estimation',  time_estimation) \
                    .field('Filament Weight',  filiment_weight) \
                    .field('Elapsed Time', time_elapsed) 
                
            step_num = STEP_NUMBERS[step_name]
            p = p.field("Percentage Done", perc_done) \
                .field("Step", step_num)
                
            
            # Get latest image
            #_, width, height, _, yuv_image = machine.get_camera_image()
            #latest_images[name] = (yuv_image, image_num)
            #image_num += 1

            did_send_event = False
                       
            if step_num == STEP_NUMBERS['idle']:
                tmp = 0
            if step_num == STEP_NUMBERS['printing'] and prev_step != step_num and       prev_step != -1:
                p = p.field("Print Event", 1)
                did_send_event = True
            
            # change STEP_NUMBERS TO "completed" 	
            if step_num == STEP_NUMBERS["completed"] and prev_step != step_num and prev_step != -1: 
                # machine.save_camera_jpg(f"test_{name}.jpg")
                p = p.field("Print Event", 3)
                did_send_event = True
            
            if step_num == STEP_NUMBERS['idle'] and prev_step == 5:
                p = p.field('Print Event', 2)
                did_send_event = True
           
            if not did_send_event:
                p = p.field('Print Event', 0)


            # if the printer is not printing send data every 30 seconds
            if current_process is None:
                #await asyncio.sleep(30)
                time.sleep(30)

            # if the printer is printing send data every 5 seconds
            else:
                #await asyncio.sleep(5)
                time.sleep(5)

            print('Waiting to send to influxdb...')
            # write the data into influxdb

            write_influx(p)
            print('sent')
            #print(p)
            prev_step = step_num

    except Exception as e:
        print(traceback.format_exc())
    try:
        print(name, 'is being turned off!')
        del current_printers[name]
        print(name, "has turned off!")
        machine.finish()
        print(name, "is finished!")
        p = p.field("Step", -1)
        write_influx(p)

    except Exception as e:
        print(traceback.format_exc())
            
message_queue = []
def write_influx(record):
    current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    print(repr(current_time))
    #print(record)
    record = record.time(current_time)
    #print(record)
    message_queue.append((record,[]))
    print('Written')
    
    
def influxdb_writer():
    while True:
        while not message_queue:
            time.sleep(0.01)
        message = message_queue.pop(0)
        record = message[0]
        submitted_to = message[1]           
        for name, use_info in influxdb_instances.items():
            successfully_submitted = True
            if name in submitted_to:
                continue
                
            enabled = use_info['enabled']

            print(name, 'enabled:', repr(enabled))

            if not enabled:
                continue

            bucket = use_info['bucket']
            org = use_info['org']
            token = use_info['token']
            url = use_info['url']



            print('ACQUIRE')
            influx_sem.acquire()
            try:
                client = influxdb_client.InfluxDBClient(
                    url=url,
                    token=token,
                    org=org
                )
                write_api = client.write_api(write_options=SYNCHRONOUS)
                write_api.write(bucket=bucket, record=record)
            except:
                print(traceback.format_exc())
                successfully_submitted = False
                time.sleep(5)
            
            finally:
                if successfully_submitted:
                    submitted_to.append(name)
                print('RELEASE')
                influx_sem.release()
        if len(submitted_to) < len(influxdb_instances):
            message_queue.insert(0, message)

SEC = 1
MIN = 60*SEC
HOUR = 60*MIN

def printers_that_are_off(current_printers):

    # Figure out whether there is a thread running for each printer.
    while True:
        # We will use authentication_dict as a list of printers for now,
        # later we should write the printers to an external file so that
        # they can dynamically change.
        print('Thread checking...')

        for name in authentication_dict:
            if name in current_printers:
                print('Skipped printer', name)
                continue


            print('Off printer:', name, 'has had print event 0.')
            p = influxdb_client.Point("send efficient 2") \
                    .tag("Name", name) \
                    .field("Print Event", 0) \
                    .field("Step", -1)

            # Send that the printer
            write_influx(p)

        time.sleep(0.5*HOUR)


def main():
    t = Thread(target=influxdb_writer, args=(), daemon=True)
    t.start()
    print('Sending offs...')
    for name in authentication_dict:
        p = influxdb_client.Point("send efficient 2") \
                .tag("Name", name) \
                .field("Extruder Temperature", 0) \
                .field("Target Temperature", 0) \
                .field("Percentage Done", 0) \
                .field("Step", -1)
                

        write_influx(p)
    print('We have declared that the printers are off!')

    current_printers = {}

    t = Thread(target=printers_that_are_off, args=(current_printers,), daemon=True)
    t.start()
    print('This is the authenticaton dict', authentication_dict)
    discovery = newapi.MakerbotDiscovery(device = BROADCAST_INTERFACE)
    print("start")
    while True:
        while len(current_printers) == len(authentication_dict):
            time.sleep(1)
        found = discovery.discover(delay_broadcast = True)
        print('Found:', found)
        for ip, name, identity in found:
            if name not in current_printers and name in authentication_dict:
                print("found new printer:", name)
                printer = MakerbotPrinter(ip, authentication_dict[name])
                t = Thread(target=printer.send_info, args=(current_printers,), daemon=True)
                t.start()
                current_printers[name] = t

                t = Thread(target=printer.send_camera_stream, daemon=True)
                t.start()
                print("Added Printer")


if __name__ == '__main__':
    main()

