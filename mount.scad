
//include <MCAD/boxes>

//cube([10, 10, 10]);

$fn = 100;

eps = 0.01;

pi_width = 49;
pi_height = 57.5;
pi_hole_dist = 5;

pi_mount_thick = 2;

base_thick = 5;
base_width = 50;
base_length = 70;

top_hole_dist = 5;
base_hole_dist = 5;

height = 150 - base_thick;
outside = 20;

M2_tol = 2.4/2;
M3_tol = 3.5/2;
M3 = (3-0.2)/2;

wall = 2;
innerside_clear = 0.5;
innerside = outside - wall*2 - innerside_clear*2;

module gen_fillet(fillet_r, thick) {
    difference() {
        translate([0, -fillet_r, -fillet_r]) cube([thick, fillet_r+eps, fillet_r+eps]);
        translate([-eps, -fillet_r, -fillet_r]) rotate([0, 90, 0]) cylinder(h=thick+2*eps, r=fillet_r);
    }
}

module fillet() {
    fillet_r = (pi_width+2*pi_hole_dist - outside) / 2;
    gen_fillet(fillet_r, pi_mount_thick);
}

module pi_tab() {
    difference() {
        translate([outside/2, -pi_width/2-pi_hole_dist, 0]) rotate([0, -90, 0]) cube([2*pi_hole_dist, pi_width+2*pi_hole_dist, pi_mount_thick]);
        for (i = [-1, 1]) {
            translate([0, i*pi_width/2, pi_hole_dist]) rotate([0, 90, 0]) cylinder(h=outside/2+eps, r=M3_tol);
        }
    }
    for (i = [-1, 1]) {
        translate([outside/2-pi_mount_thick, i*(outside/2), 0]) scale([1, -i*1, 1]) fillet();
    }
}

extra_pi_mount_height = 20;
TOP_TAB = false;

module base_mounter(base_thick, hole_r) {
    difference() {
        translate([-base_length/2, -base_width/2, -base_thick]) cube([base_length, base_width, base_thick]);
        for (i = [-1, 1]) {
            for (j = [-1, 1]) {
                translate([i*(base_length/2-base_hole_dist), j*(base_width/2-base_hole_dist), -base_thick-eps]) cylinder(h=base_thick+2*eps, r=hole_r);
            }
        }
    }
}

module bot_part() {
    difference() {
        translate([-outside/2, -outside/2, 0]) cube([outside, outside, height]);
        cutside = outside - wall*2;
        translate([-cutside/2, -cutside/2, -eps]) cube([cutside, cutside, height+2*eps]);
        translate([0, 0, height-top_hole_dist]) rotate([90, 0, 0]) cylinder(h=outside+eps, r=M3);
    }
    base_mounter(base_thick, M3_tol);
    if (TOP_TAB && extra_pi_mount_height > 0) {
        translate([outside/2-wall, -outside/2, height-eps]) cube([wall, outside, extra_pi_mount_height+2*eps]);
    }
    translate([0, 0, extra_pi_mount_height+height-(pi_height+2*pi_hole_dist)]) {
        pi_tab();
        if (TOP_TAB) {
            translate([0, 0, pi_height]) pi_tab();
        }
    }
}

cam_mount_thick = 3;
cam_hole_dist = 4;
cam_width = 21;
cam_height = 13;

eyehole_d = 15;

module top_part() {
    translate([-innerside/2, -innerside/2, -height]) cube([innerside, innerside, height]);
    the_offset = 5 + innerside/2 - cam_mount_thick;
    difference() {
        union() {
            translate([innerside/2-cam_mount_thick, -cam_width/2-cam_hole_dist, -eps]) cube([cam_mount_thick, cam_width+2*cam_hole_dist, the_offset+cam_height+2*cam_hole_dist+eps]);

            fillet_r = innerside-cam_mount_thick;
            translate([innerside/2-cam_mount_thick, -innerside/2, 0]) rotate([0, 180, -90]) gen_fillet(fillet_r, innerside);
        }
    //}
        translate([0, 0, cam_hole_dist+cam_height]) {
                //rotate([0, 90, 0]) translate([0, 0, -innerside/2-eps]) cylinder(r=eyehole_d/2, h=innerside+2*eps);
                translate([-innerside/2, 0, 0]) translate([-eps, -eyehole_d/2, -eyehole_d/2]) cube([innerside+2*eps, eyehole_d, eyehole_d+the_offset]);
            }
        translate([0, 0, the_offset]) for (i = [-1, 1]) {
            for (j = [-1, 1]) {
                translate([innerside/2-cam_mount_thick-eps, i*(cam_width/2), cam_mount_thick+cam_height/2+j*(cam_height/2)]) rotate([0, 90, 0]) cylinder(h=cam_mount_thick+2*eps, r=M2_tol);
            }
        }
    }//
}

module screw_stand(r, hole_r, h) {
	difference() {
		cylinder(h=h, r=r);
		translate([0, 0, -eps]) cylinder(g=h+2*eps, r=hole_r);
	}
}

screw_stand_r = 4;

module camera_mount() {
	//screw_stand(screw_stand_r, M2);
    the_offset = 10 + innerside/2 - cam_mount_thick;
    difference() {
        union() {
            translate([0, -cam_width/2-cam_hole_dist, -eps]) cube([cam_mount_thick, cam_width+2*cam_hole_dist, the_offset+cam_height+2*cam_hole_dist+eps]);
        }
    //}
        translate([0, 0, cam_hole_dist+cam_height]) {
                //rotate([0, 90, 0]) translate([0, 0, -innerside/2-eps]) cylinder(r=eyehole_d/2, h=innerside+2*eps);
                translate([0, 0, 0]) translate([-eps, -eyehole_d/2, -eyehole_d/2]) cube([innerside+2*eps, eyehole_d, eyehole_d+the_offset]);
            }
        translate([0, 0, the_offset]) for (i = [-1, 1]) {
            for (j = [-1, 1]) {
                translate([-eps, i*(cam_width/2), cam_mount_thick+cam_height/2+j*(cam_height/2)]) rotate([0, 90, 0]) cylinder(h=cam_mount_thick+2*eps, r=M2_tol);
            }
        }
    }//
}

module hinge_thing(length, outer_r, hole_r, stand=0) {
	difference() {
		union() {
			cylinder(h=length, r=outer_r);
			translate([-outer_r, 0, 0]) cube([outer_r*2, outer_r+stand+eps, length]);
		}
		translate([0, 0, -eps]) cylinder(h=length+2*eps, r=hole_r);
	}
}

hinge_outer_r = 7.5/2;
hinge_hole_r = M3;
hinge_hole_tol = 0.5/2;

camera_mount_hinge_stand = 2;

module top_camera_mount() {
	camera_mount();
	//hinge_thing(innerside/2, hinge_outer_r, hinge_hole_r+hinge_hole_tol, stand=0);
	translate([-hinge_outer_r-camera_mount_hinge_stand, -innerside/2, hinge_outer_r]) rotate([-90, -90, 0]) hinge_thing(innerside/2, hinge_outer_r, hinge_hole_r+hinge_hole_tol, stand=camera_mount_hinge_stand);
}

hinge_slider_stand = 1;

module hinge_slider() {
    translate([-innerside/2, -innerside/2, -height]) cube([innerside, innerside, height]);
	translate([-hinge_outer_r, 0, hinge_slider_stand+hinge_outer_r]) rotate([-90, 0, 0]) hinge_thing(innerside/2, hinge_outer_r, hinge_hole_r, stand=hinge_slider_stand);
}

wedge_length = 210+20-0.75;
wedge_side = 13;

below_room = 2;
below_extent = 10;

wedge_cut_side = 15;

max_dim = 220;

stop_wall_thick = 3;
stop_wall_height = 40;
stop_wall_right = wedge_length - wedge_cut_side;

max_chute_length = max_dim - below_extent - wedge_side;

extra_left = innerside/2;//max_dim - wedge_length;

module printer_wedge() {
	// https://homehack.nl/openscad-polygon-and-polyhedron/
	// function circle(radius) = [for (phi = [0 : 1 : 720]) [radius * cos(phi/2), radius * sin(phi)]];
	// color("red") polygon(circle(20));
	difference() {
		union() {
			translate([-extra_left, eps, below_room-eps]) rotate([0, 90, 0]) linear_extrude(wedge_length + extra_left) polygon([[0, 0], [0, -wedge_side-eps], [-wedge_side-eps, 0], [0, 0]]);
			translate([-extra_left, -wedge_side-below_extent, 0]) cube([wedge_length+extra_left-wedge_cut_side, wedge_side+below_extent+eps, below_room]);
		}
		translate([eps+wedge_length, 2*eps, below_room+wedge_side+eps]) rotate([-90, 0, 180]) linear_extrude(wedge_side+4*eps) polygon([[0, 0], [0, wedge_cut_side+eps], [wedge_cut_side+eps, 0], [0, 0]]);
	}
	//translate([wedge_length, 0, 0]) cylinder(r=0.01, h=1000);
	translate([-extra_left, 0, 0]) cube([stop_wall_right + extra_left, stop_wall_thick, stop_wall_height]);
	translate([-innerside/2, 0, outside/2-innerside/2]) {
		cube([innerside, max_chute_length, innerside]);
		/*
		difference() {
			cube([outside, max_chute_length, outside]);
			translate([wall, 0, wall]) cube([outside-2*wall, max_chute_length+eps, outside-2*wall]);
			translate([outside/2, max_chute_length-top_hole_dist, outside/2]) rotate([0, 90, 0]) cylinder(r=M3, h=outside/2+eps);
		}
		*/
	}
}

max_slider_length = max_dim - base_length;

module base_slider() {
	//base_width/2
	thickness = outside;  // outside/2 + innerside/2;
	rim = 10;
	cut_width = base_width - 2*rim;
	cut_length = base_length - 2*rim - cut_width;
	difference() {
		translate([base_length/2, 0, 0]) base_mounter(thickness, M3);
		translate([base_length/2-cut_length/2, -cut_width/2, -thickness-eps]) cube([cut_length, cut_width, thickness+2*eps]);
		for (i=[1,-1]) {
			translate([base_length/2+i*cut_length/2, 0, -thickness-eps]) cylinder(r=cut_width/2, h=thickness+2*eps);
		}
	}
	//translate([-max_slider_length, -innerside/2, -innerside]) cube([max_slider_length+eps, innerside, innerside]);
	translate([0, outside/2, 0]) rotate([0, 180, 90]) difference() {
		translate([0, -eps, 0]) cube([outside, max_slider_length+eps, outside]);
		translate([wall, -eps, wall]) cube([outside-2*wall, max_slider_length+2*eps, outside-2*wall]);
		translate([outside/2, max_slider_length-top_hole_dist, outside/2]) rotate([0, 90, 0]) cylinder(r=M3, h=outside/2+eps);
	}
}

WHICH = 6;

extra_height = 100;

translate((WHICH == 0) ? [0, 0, height+extra_height] : [0, 0, 0]) if (WHICH == 0 || WHICH == 1) top_part();
if (WHICH == 0 || WHICH == 2) bot_part();
if (WHICH == 0 || WHICH == 3) top_camera_mount();
if (WHICH == 0 || WHICH == 4) hinge_slider();
if (WHICH == 0 || WHICH == 5) printer_wedge();
if (WHICH == 0 || WHICH == 6) base_slider();

difference() {
    //cylinder(h=100, r=50, $fn=1000);
    //translate([0, 0, 10]) cylinder(h=90+eps, r=40);
}


