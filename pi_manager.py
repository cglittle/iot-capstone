
import os
import os.path
this_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(this_path)

import streamlit as st
import random
import string
import time
from streamlit_autorefresh import st_autorefresh
import pandas as pd
from st_click_detector import click_detector
from pi_manager_backend import MachinesBackend, machine_files
import configparser
from misc import modules_in_path
import json
import traceback


#Page title
st.set_page_config(page_title='Pi Management', page_icon=None, layout='wide', initial_sidebar_state='collapsed', menu_items=None)


#Get rid of streamlit
st.markdown("<style> header {display: none !important;} </style>", unsafe_allow_html=True)
st.markdown("<style> footer {display: none !important;} </style>", unsafe_allow_html=True)
st.markdown('''\
# Mission Control''')

#st.write(st.session_state)

AUTO_RELOAD = False

AUTO_AUTHENTICATE = False
SEC = 1000

if AUTO_RELOAD:
    count = st_autorefresh(interval=1*SEC, key='reloader')  # limit=100,


class GenerateList:
    def __init__ (self, id_name, col_format, titles, rows):
        self.id_name = id_name
        self.col_format = col_format
        self.titles = titles
        self.rows = rows
        
    def show_lst(self):
        colms = st.columns(self.col_format)
        for col, title in zip(colms, self.titles):
            col.write(title)
        for i,row in enumerate(self.rows):
            cols = st.columns(self.col_format)
            uid = self.get_uid_for_row(i,row)
            cols[0].checkbox(' ', value=False, label_visibility='collapsed',            
                                key=f'{uid}_{self.id_name}_selected')
            self.make_column(row, cols[1:])                    
        
    def get_selected(self):
        selected_lst = []
        for i,row in enumerate(self.rows):
            uid = self.get_uid_for_row(i,row)
            if st.session_state[f'{uid}_{self.id_name}_selected']:
                selected_lst.append(self.selection_item(row))
        return selected_lst
    
    def selection_item(self, row):
        return row
    
    def get_uid_for_row(self, i, row):
        return i
        
    def make_column(self, row, cols):
        pass

class PiList(GenerateList):
    def __init__(self, id_name, rows):
        # checkbox, name, ip, edit
        super().__init__(id_name, (1,2,2,2), ['', 'Name', 'IP', ''], rows)

    def get_uid_for_row(self, i, row):
        return row['name']

    def make_column(self, row, cols):
        pi_name = row['name']
        key_row_edit = f'{pi_name}_editing'
        if key_row_edit not in st.session_state:
            st.session_state[key_row_edit] = False

        def edit_pressed():
            st.session_state[key_row_edit] = True
        def apply_pressed():
            print('New name:', new_pi_name)
            mb.edit_pi(pi_name, new_pi_name)
            st.session_state[key_row_edit] = False
        if st.session_state[key_row_edit]:
            new_pi_name = cols[0].text_input(' ', placeholder=pi_name, key=f'{pi_name}_edit_name', label_visibility= 'collapsed')
            cols[1].write(row['ip'])
            cols[2].button('Apply', on_click=apply_pressed, key=f'{pi_name}_edit')
            # TODO: Cancel button needs to be added...
        else:
            cols[0].write(pi_name)
            cols[1].write(row['ip'])
            # on_click=edit_pressed
            cols[2].button('Edit', on_click=None, key=f'{pi_name}_edit')

    def selection_item(self, row):
        return row['name']
        
class MachineList(GenerateList):
    def __init__ (self, id_name, rows):
        # checkbox, name, type, dash name, parent, edit
        super().__init__(id_name, (1,2,2,2,2,2,2,2), ['', 'Name', 'Type', 'Dashboard Name', 'Parent Pi', 'Template', '', ''], rows)

    def get_uid_for_row(self, i, row):
        #print(row)
        return row['name']
        
    def make_column(self, row, cols):
        machine_name = row['name']
        machine_type = row['type']
        dash_name = row['dash_name']

        key_row_edit = f'{machine_name}_editing'
        if key_row_edit not in st.session_state:
            st.session_state[key_row_edit] = False

        def edit_pressed():
            # TODO: Also allow choice of changing template machine name (and implicitly the template dashboard name)...
            #       NO: Do this in a separate list, and have it depend on the machine type, rather than the machine!
            #pass
            st.session_state[key_row_edit] = True
        def apply_pressed():
            print('New name:', new_machine_name)
            print('New type:', new_machine_type)
            print('New dash name:', new_dash_name)
            prev_pi_name = row['parent']
            mb.edit_machine(prev_pi_name, machine_name, new_machine_name, new_machine_type, new_dash_name, new_pi_name)
            st.session_state[key_row_edit] = False
        if st.session_state[key_row_edit]:
            new_machine_name = cols[0].text_input(' ', placeholder=machine_name, key=f'{machine_name}_edit_name', label_visibility= 'collapsed')  # Name
            new_machine_type = cols[1].text_input(' ', placeholder=machine_type, key=f'{machine_name}_edit_type', label_visibility='collapsed')  # Type
            new_dash_name = cols[2].text_input(' ', placeholder=dash_name, key=f'{machine_name}_edit_dash_name', label_visibility='collapsed')
            new_pi_name = cols[3].text_input(' ', placeholder=row['parent'], key=f'{machine_name}_edit_pi_name', label_visibility='collapsed')
            #cols[3].write(row['ip'])
            cols[4].write(row['template'])
            cols[5].button('Apply', on_click=apply_pressed, key=f'{machine_name}_edit')
            # TODO: Cancel button needs to be added...
        else:
            if 'link' in row:
                dashboard_link = row['link']
                cols[0].write(f'[{machine_name}]({dashboard_link})')  # Name
            else:
                cols[0].write(machine_name)  # Name
            cols[1].write(machine_type)  # Type
            cols[2].write(dash_name)
            pi_name = row['parent']
            cols[3].write(pi_name)
            cols[4].write(row['template'])
            # on_click=edit_pressed
            cols[5].button('Edit', on_click=None, key=f'{machine_name}_edit')
            def setup_pressed():
                #pass
                st.session_state['page'] = row  # (pi_name, machine_name, machine_type)
            cols[6].button('Setup', on_click=setup_pressed, key=f'{machine_name}_setup')
   
    def selection_item(self, row):
        return row['name']
        
        
class ProgramList(GenerateList):
    def __init__ (self, id_name, rows):
        super().__init__(id_name, (1,1,1), ['', 'File', 'Runnable'], rows)

    def get_uid_for_row(self, i, row):
        return row.fname
        
    def make_column(self, uploadable_file, cols):
        program_name = uploadable_file.fname
        cols[0].write(program_name) 
        cols[1].write(uploadable_file.run) 

# Manages a list of programs that runs either on the server or on the pi for a given machine (just one)
class OneListToRuleThemAll(GenerateList):
    def __init__ (self, id_name, rows):
        # TODO: What are the rows?
        super().__init__(id_name, (1,1,1), ['', 'Program Name', 'Running'], rows)

    def get_uid_for_row(self, i, row):
        return row['program_type']
        
    def make_column(self, row, cols):
        program_type = row['program_type']
        program_names = row['program_names']
        running = len(program_names) > 0
        # row['running']
        cols[0].write(program_type) 
        cols[1].write(running) 

    def selection_item(self, row):
        return row['program_type']
        

def one_program_list_to_rule_them_all(mb, name_id, server, pi_name, machine_info):
    machine_name = machine_info['name']
    machine_type = machine_info['type']
    if server:
        all_programs = mb.available_server_programs()
    else:
        all_programs = mb.available_programs(pi_name)
    #st.write(all_programs)
    machine_specific_programs = all_programs.get(machine_type, [])
    binned_running_programs = {}
    for program_type in machine_specific_programs:
        binned_running_programs[program_type] = []
    if server:
        running_programs = mb.running_server_programs(machine_name)
    else:
        running_programs = mb.running_programs(pi_name, machine_name)
    for program_info in running_programs:
        program_name = program_info['name']
        program_type = program_info['type']
        binned_running_programs[program_type].append(program_name)
    program_stats = []
    for program_type, program_names in binned_running_programs.items():
        program_stats.append({'program_type': program_type, 'program_names': program_names})
    ruling_list = OneListToRuleThemAll(f'{name_id}_list', program_stats)
    ruling_list.show_lst()
    def stop_programs():
        selected_programs = ruling_list.get_selected()
        for program_type in selected_programs:
            print(f'Stopping {program_type} for {machine_name}...')
            if server:
                mb.stop_server_program(machine_name, program_type)
            else:
                mb.stop_program(pi_name, machine_name, program_type)
            print('Stopped!')
    st.button('Stop', on_click=stop_programs, key=f'{name_id}_stop')
    def restart_programs():
        selected_programs = ruling_list.get_selected()
        for program_type in selected_programs:
            print(f'Restarting {program_type} for {machine_name}...')
            if server:
                mb.restart_server_programs(pi_name, machine_name, program_type)
            else:
                mb.restart_programs(pi_name, machine_name, program_type)
            print('Restarted!')
    st.button('Restart', on_click=restart_programs, key=f'{name_id}_restart')
    
# @st.cache_data
# def get_machine_modules():
#     return modules_in_path('machines')


def main():

    # TODO: Test refresh button, and also test if selecting a checkbox reloads the page (we want it to...)
    if not AUTO_RELOAD:
        refreshed = st.button('Refresh Page', key='refresh')

    mb = MachinesBackend()

    the_page = st.session_state.get('page')
    if the_page is not None:
        # Show a setup page or something
        # TODO: machine setup pages come from the machine module... Need to have those loaded and cached...
        row = the_page
        pi_name = row['parent']
        machine_name = row['name']
        machine_type = row['type']
        dash_name = row['dash_name']

        machine_modules = modules_in_path('machines')
        machine_modules_dict = {}
        for machine_module in machine_modules:
            if not hasattr(machine_module, 'machine_type'):
                continue
            machine_modules_dict[machine_module.machine_type] = machine_module

        # pi_configs = mb.list_pi_configurations()
        # config = pi_configs[pi_name]
        machine_configs = mb.list_machine_configurations()
        config = machine_configs[machine_name]

        machine_module = machine_modules_dict[machine_type]

        machine_module.setup_streamlit_page(st, mb, config, pi_name, machine_name, machine_type, dash_name, row)

        def return_to_home():
            st.session_state['page'] = None
        st.button('Return', on_click=return_to_home, key='return_home')

        return

    # Reload the machine modules in pi_manager_manager, due
    # to a change in the one of the templates in the machines folder,
    # so that new server programs and server api calls are updated, etc...
    def reload_modules():
        pass
        # TODO: Make an appropriate API call to mb...
    #st.button('Reload machine modules', on_click=reload_modules, key='reload_modules')

    machines = mb.total_machines()

    dashboard_links = mb.dashboard_links()

    # A new list of machine types and which machine name to use as a template... (or dashboard name as template? That would be more general...)
    dash_templates = mb.list_dash_templates()

    #online_machines = mb.list_machines()

    # What about the online pis?
    pis = mb.total_pis()
    #st.write(pis)
    online_pis = mb.list_pis()

    pi_lst = PiList('pis', [pi for pi_name, pi in pis.items()])

    for pi_name, pi_info in pis.items():
        pi_info['ip'] = online_pis.get(pi_name, None)

    st.markdown('## Pis')
    pi_lst.show_lst()

    def update_pis():
        update_lst = pi_lst.get_selected()
        print(update_lst)
        mb.update(update_lst)
        print('Updated Programs')
    st.button('Update Programs', on_click=update_pis, key='update')

    def disconnect_pis():
        update_lst = pi_lst.get_selected()
        print(update_lst)
        mb.disconnect_pis(update_lst)
        print('Disconnected Pis')
    st.button('Disconnect', on_click=disconnect_pis, key='disconnect')

    def delete_pis():
        update_lst = pi_lst.get_selected()
        print(update_lst)
        for pi_name in update_lst:
            print(f'Deleteing {pi_name}...')
            mb.delete_pi(pi_name)
        print('Deleted Pis!')
    #st.button('Delete', on_click=delete_pis, key='delete_pis')





    # Machine configuration dictionary json for assigning cameras, sensors, machine identifiers, etc.
    #pi_new_config = st.text_input('New config:', key='pi_new_config', placeholder='Machine configuration dictionary JSON for assigning cameras, sensors, machine identifiers, etc.')  # , label_visibility='collapsed')
    pi_new_config = st.text_input('New config:', key='pi_new_config', placeholder="{'cameras': {'pi_cam1': {'type': 'picam', 'options': {}, ...}}}")  # , label_visibility='collapsed')
    listed_pis = pi_lst.get_selected()
    def replace_pi_config():
        try:
            new_config_json = json.loads(pi_new_config)
        except:
            print(traceback.format_exc())
            st.session_state['pi_config_error'] = True
            return
        update_lst = pi_lst.get_selected()
        for pi_name in update_lst:
            mb.replace_pi_config(pi_name, new_config_json)
    if st.session_state.get('pi_config_error', False):
        st.write('Error: Config must be valid JSON syntax.')
        st.session_state['pi_config_error'] = False
    st.button('Replace Configuration', on_click=replace_pi_config, key='pi_replace_config')
    pi_configs = mb.list_pi_configurations()
    with st.expander('Pi Configurations', expanded=False):
        for pi_name in listed_pis:
            pi_config = pi_configs[pi_name]
            st.write(pi_name)
            st.write(pi_config)




    # {'cameras': {'pi_cam1': {'type': 'picam', 'options': {}, ...}}}

    # NOTE: No add button because pis are automatically discovered.

    parents = {}

    machine_names = []
    
    for pi_name in listed_pis:
        pi_info = pis[pi_name]
        machine_names.extend([machine['name'] for machine in pi_info['machines']])
        for machine in pi_info['machines']:
            parents[machine['name']] = pi_name

    #machines = {key: value for key, value in sorted(machines.items())}
    machine_names.sort()

    machine_rows = [machines[name] for name in machine_names]
    for machine_row in machine_rows:
        machine_name = machine_row['name']
        machine_type = machine_row['type']
        machine_row['parent'] = parents[machine_name]
        template_machine_name, template_dash_name = dash_templates[machine_type]
        machine_row['template'] = template_machine_name == machine_name

    for dash_name, (machine_name, dashboard_link) in dashboard_links.items():
        #print(f'dashboard link for machine name is {dashboard_link}, and the machine name is {machine_name}')
        if machine_name is None or machine_name not in machines:
            continue
        machine_row = machines[machine_name]
        machine_row['link'] = dashboard_link

    machine_lst = MachineList('machines', machine_rows)

    st.markdown('## Machines')

    machine_lst.show_lst()

    if 'adding_machine' not in st.session_state:
        st.session_state.adding_machine = False 
    if st.session_state.get('add', False):
        st.session_state.adding_machine = True

    if st.session_state.adding_machine:
        cols = st.columns(machine_lst.col_format)
        cols[0].checkbox(' ', value=False, label_visibility='collapsed',   key='new_selected')
        name = cols[1].text_input(' ', key='new_name', placeholder='Machine Name', label_visibility='collapsed')
        machine_type = cols[2].text_input(' ', key='new_type', placeholder='Machine Type', label_visibility='collapsed')
        dash_name = cols[3].text_input(' ', key='new_dash', placeholder='Dashboard Name', label_visibility='collapsed')
        pi_name = cols[4].text_input(' ', key='new_pi_name', placeholder='Pi Name', label_visibility='collapsed')
        
    def apply_hit():
        mb.add_machine(name, machine_type, dash_name, pi_name)
        print(name, machine_type)
        st.session_state.adding_machine = False
        
    # col1, col2, col3 = st.columns((3,3,50))
        
    #st.button('Add', on_click=None, key='add')

    if st.session_state.adding_machine:
        st.button('Apply', on_click=apply_hit, key='apply')
         
    def update_dash():
        update_lst = machine_lst.get_selected()
        mb.update_dashboards(update_lst)
    st.button('Update Dashboard', on_click=update_dash, key='update_dash')

    def delete_machine():
        update_lst = machine_lst.get_selected()
        mb.delete_machines(update_lst)
    #st.button('Delete', on_click=delete_machine, key='delete')


    # Machine configuration dictionary json for assigning cameras, sensors, machine identifiers, etc.
    new_config = st.text_input('New config:', key='new_config', placeholder="{'cameras': {'maincam': 'pi_cam1'}, 'pi_programs': {'program_type': {options...}}, 'server_programs': {...},...}")  # , label_visibility='collapsed')

    #'Machine configuration dictionary JSON for assigning cameras, sensors, machine identifiers, etc.'
    def replace_config():
        try:
            new_config_json = json.loads(new_config)
        except:
            print(traceback.format_exc())
            st.session_state['config_error'] = True
            return
        update_lst = machine_lst.get_selected()
        for machine_name in update_lst:
            pi_name = parents[machine_name]
            mb.replace_config(pi_name, machine_name, new_config_json)
    if st.session_state.get('config_error', False):
        st.write('Error: Config must be valid JSON syntax.')
        st.session_state['config_error'] = False
    st.button('Replace Configuration', on_click=replace_config, key='replace_config')
    machine_configs = mb.list_machine_configurations()
    update_lst = machine_lst.get_selected()
    with st.expander('Machine Configurations', expanded=False):
        for machine_name in update_lst:
            machine_config = machine_configs[machine_name]
            st.write(machine_name)
            st.write(machine_config)
    
    machine_types = []
    listed_machines = machine_lst.get_selected()
    for machine_name in listed_machines:
        machine = machines[machine_name]
        
        if machine['type'] not in machine_types:
            machine_types.append(machine['type'])

    
    # Expander or tabs here:
    with st.expander('Program and File Manager', expanded=False):#, label_visibility='collapsed'):

        st.write('Programs:')

        left_col, right_col = st.columns((1, 1))

        with right_col:
            if listed_machines:
                machine_name = listed_machines[-1]
                pi_name = parents[machine_name]
                machine_info = machines[machine_name]
                machine_type = machine_info['type']
                st.write(f'For machine {machine_name} of type {machine_type}:')
                # List of thread program stats for bottom most selected machine on both the server and the pi
                server_tab, pi_tab = st.tabs(['Server Programs', 'Pi Programs'])
                with server_tab:
                    one_program_list_to_rule_them_all(mb, 'server_programs', True, pi_name, machine_info)
                with pi_tab:
                    one_program_list_to_rule_them_all(mb, 'pi_programs', False, pi_name, machine_info)

        st.write('Files:')

        with left_col:
            
            tabs = []
            if machine_types:
                tabs = st.tabs(machine_types)
            
            for tab, machine_type in zip(tabs, machine_types):
            
                def local_context(machine_type, programs):
                
                    program_lst = ProgramList(f'printer_programs_{machine_type}', programs)
                
                
                    def get_selected():
                        return [machine_name for machine_name in machine_lst.get_selected() if machines[machine_name]['type'] == machine_type]
                    
                    def terminate_program():
                        update_lst = get_selected()
                        print(f'Terminating on machines: {update_lst}')
                        for uploadable_file in program_lst.get_selected():
                            program = uploadable_file.fname
                            if not uploadable_file.run:
                                print('Cannot terminate non-runnable file:', program)
                                continue
                            mb.kill_program(update_lst, program)  
                            print(f'Terminatated {program}!!!')
                            
                    def run_program():
                        update_lst = get_selected()
                        print(f'Running on machines: {update_lst}')
                        for uploadable_file in program_lst.get_selected():
                            program = uploadable_file.fname
                            if not uploadable_file.run:
                                print('Cannot run non-runnable file:', program)
                                continue
                            mb.run_program(update_lst, program)
                            print(f'Running {program}!!!')
                            
                    def upload_program():
                        update_lst = get_selected()
                        print(f'Uploading on machines: {update_lst}')
                        for uploadable_file in program_lst.get_selected():
                            program = uploadable_file.fname
                            mb.upload_program(update_lst, program)  
                            print(f'Uploading {program}!!!')
                
                    with tab:
                    
                        program_lst.show_lst()
                        
                        st.button('Upload Program', on_click = upload_program, key= f'upload_{machine_type}')
                        
                        #st.button('Terminate Program', on_click = terminate_program, key= f'terminate_{machine_type}')
                        
                        #st.button('Run Program', on_click = run_program, key= f'run_{machine_type}')
                        
                        
                programs = machine_files[machine_type]#+['pi_server_api.py']
                
                local_context(machine_type, programs)
                
                


if __name__ == '__main__':

    if 'logged_in' not in st.session_state:
        st.session_state.logged_in = AUTO_AUTHENTICATE
    if 'failed_login' not in st.session_state:
        st.session_state.failed_login = False

    if st.session_state.logged_in:
        main()
    else:
        printer_info = configparser.ConfigParser()
        printer_info.read('pi_infos.ini')
        #printer_info.read('grafana_config.ini')
        #api_key = printer_info['default']['api_key']
        default = printer_info['DEFAULT']
        the_username = default['username']
        the_password = default['password']
        if 'username' in st.session_state and 'password' in st.session_state:
            print('BEFORE:', repr(st.session_state['username']), repr(st.session_state['password']))
        with st.form('login_form'):
            st.subheader('Login')
            username = st.text_input('Username', key='username', autocomplete='username', value=the_username)
            password = st.text_input('Password', key='password', autocomplete='new-password', type='password')
            def do_login():
                username = st.session_state['username']
                password = st.session_state['password']
                # print(repr(username), repr(password))
                if username == the_username and password == the_password:  
                    st.session_state.logged_in = True
                else:
                    st.session_state.failed_login = True
            st.form_submit_button('Login', on_click=do_login)
            if st.session_state.failed_login:
                st.error('Login invalid.')
                st.session_state.failed_login = False

