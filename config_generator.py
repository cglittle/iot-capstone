
import os.path
import os
import copy
import json

def write_default_file(fname, description, content, existing_inputs=None, **inputs):

    if os.path.exists(fname):
        print(f'Skipping {fname}...')
        return
    print(f'Gathering info for {fname}...')
    print(description)
    print()
    print('Format:')
    print(content)
    print()
    for name in inputs:
        inputs[name] = input(inputs[name])
    
    if existing_inputs is not None:
        inputs.update(existing_inputs)
        
    f = open(fname, 'w')
    f.write(content.format(**inputs))
    f.close()
    
    return inputs

def ask_overwrite(fname, content):

    overwrite = True
    if os.path.exists(fname):
        overwrite = input(f'Overwrite remote_config.json (y/n)? ') == 'y'
    if overwrite:
        print(f'Overwriting {fname}...')
        print(json.dumps(content, indent=2))
        with open(fname, 'w') as f:
            f.write(json.dumps(content, indent=2))
        print('Done.')
    else:
        print(f'Did not change {fname}.')

org = input('InfluxDB Organisation? ')
bucket = input('InfluxDB Bucket? ')
token = input('InfluxDB Token? ')
measurement = input('InfluxDB Measurement? ')

print('Remote URL example: https://influxdb.yourdomain.name')
remote_url = input('Remote InfluxDB URL? ')

remote_influx_instances = {
    "local": {
        "enabled": True,
        "bucket": bucket,
        "org": org,
        "token": token,
        "url": remote_url,  # https://influxdb.yourdomain.name, for example
        "measurement": measurement
    }
}

print('Remote control URL example: https://control.yourdomain.name')
remote_control_url = input('Remote control URL? ')
print('Remote image URL example: https://images.yourdomain.name')
remote_images_url = input('Remote images URL? ')

remote_config = {
    "influxdb_instances": remote_influx_instances,
    "amigos_control": remote_control_url,
    "amigos_images": remote_images_url
}

ask_overwrite('remote_config.json', remote_config)




print('Local URL example: http://192.168.0.219:8086')
print('Note: This should use the server static IP on the local network that the Raspberry Pis connect to.')
local_url = input('Local InfluxDB URL? ')

print('Local control URL example: http://192.168.0.219:10000')
control_url = input('Local control URL? ')
print('Local image URL example: http://192.168.0.219:10001')
images_url = input('Local images URL? ')

influx_instances = copy.deepcopy(remote_influx_instances)
influx_instances['local']['url'] = local_url

#images_url = input('Local images URL? ')

print('Local Grafana URL example: http://localhost:3000')
print('Note: This is used for the management back end to update Grafana.')
local_grafana_url = input('Local Grafana URL? ')

grafana_api_key = input('Grafana API key? ')

print('Example port: 8000')
fastapi_port = int(input('FastAPI internal port? '))

print('Example port: 10000')
control_port = int(input('Control port? '))
print('Example port: 10001')
images_port = int(input('Images port? '))

print('Example Grafana display URL: https://grafana.yourdomain.name')
print('Note: This is used for making clickable links in the management interface.')
grafana_display_url = input('Grafana display URL? ')

print('Example dashboard camera URL: https://camera.yourdomain.name')
print('Note: This is used for embedding camera streams into the dashboards.')
camera_url = input('Dashboard camera URL? ')

# print('Example port: 8001')
# camera_port = int(input('Camera port? '))  # TODO: Add this to config, so image_streaming_wrangler can use it...

# TODO:
# manage_port = int(input('Management interface port? '))

# TODO:
# redirector_port = int(input('Main URL redirector port? '))

default_config = {
    "influxdb_instances": influx_instances,
    "amigos_control": control_url,
    "amigos_images": images_url,
    "grafana": local_grafana_url,
    "grafana_api_key": grafana_api_key,
    "fastapi_internal_port": fastapi_port,
    "amigos_control_port": control_port,
    "amigos_images_port": images_port,
    "grafana_display_url": grafana_display_url,
    "dash_camera_url": camera_url,
    "general_dashboards": {
        "Test_Homepage": {
            "name": "Test_Homepage",
            "folder": "Homepage",
            "type": "printer_info",
            "config": {
                "list_tag": "machine",
                "list_folder": "Machine Info"
            }
        },
    },
    "default_machine_folder": "Machine Info",
    "machine_folders": {
        #"Sim Makerbot Printer": "Overridden Simulated Printers"
    },
    "email": {
    }
}

ask_overwrite('config.json', default_config)

write_default_file(
'email.ini',
'This allows emailing of pdf reports and alerts.',
'''\
[default]
server = {smtp}
username = {email}
password = {password}
receiver = {email}
''',
email='Enter your email: ',
password='Enter email password: ',
smtp='Enter SMTP server hostname: '
)

"""
write_default_file(
'grafanaconfig.ini',
'This allows dashboard generation automatically for new printers.',
'''\
[default]
api_key = {apikey}
''',
apikey='Enter grafana api key: '
)

inputs = write_default_file(
'iot4config.ini',
'This allows the server to upload the gathered data for workshop equipment to influxdb. Although not supported by this setup script, more instances than just local can be added. As of now, a local instance is required, even if it is just the name.',
'''\
[local]
enabled = true
bucket = {bucket}
org = {org}
token = {token}
# Store the URL of your InfluxDB instance
url = http://{host}:8086
''',
host='Enter the IP or hostname of the InfluxDB server on the local network: ',
bucket='Enter InfluxDB bucket name for data to go into (e.g. Test, as long as it has been created in InfluxDB already): ',
org='Enter InfluxDB organisation name for data to go into (e.g. Capstone2023, as long as it has been created in InfluxDB already): ',
token='Enter an InfluxDB token (make one if none currently exists): '
)

write_default_file(
'iot4config_no_cloud.ini',
'This allows the pis to upload their gathered data for workshop equipment to influxdb. Although not supported by this setup script, more instances than just local can be added. As of now, a local instance is required, even if it is just the name.',
'''\
[local]
enabled = true
bucket = {bucket}
org = {org}
token = {token}
# Store the URL of your InfluxDB instance
url = http://{host}:8086
''',
existing_inputs=inputs
)

inputs = write_default_file(
'pi_infos.ini',
'This stores the ips for the pis and the login credentials for ssh, so that pi_control.py can automatically upload new files and run them. It also serves as a list of printer names for some other purposes. Eventually, the ips here will be added dynamically.',
'''\
[DEFAULT]
username = {username}
password = {password}

# Note that the machine type is implied by the name, e.g. 'Printer' at the start
[{machine_name}]
host = {machine_ip}
''',
machine_name='Enter a machine name (e.g. Printer 001): ',
machine_ip='Enter the IP for the Pi which is associated with this machine (Ensure it has a static IP): ',
username='Enter the username for the SSH login on this Pi (use the same login for all Pis): ',
password='Enter the password for the SSH login on this Pi (use the same password for all Pis): '
)

inputs = write_default_file(
'pi_setup.ini',
'This sets basic things that differ between running the pi_data_collection.py script on the server and pi, such as the interface to broadcast and look for printers on, and whether any data is sent to InfluxDB. It additionally stores the authentication tokens for each makerbot printer for automatic connection.',
'''\
[DEFAULT] 
send_to_server = True
broadcast_interface = {interface}

[{machine_name}]
auth = {auth}
enable = True
''',
existing_inputs=inputs,
auth='Assuming this machine is a makerbot, enter the authentication token (TODO: interactively tell the user to press the button on the machine to get it automatically): ',
interface='Enter the network interface that the server connects to the local network through (e.g. enp3s0): '
)

inputs = write_default_file(
'pi_setup_no_cloud.ini',
'This sets basic things that differ between running the pi_data_collection.py script on the server and pi, such as the interface to broadcast and look for printers on, and whether any data is sent to InfluxDB. It additionally stores the authentication tokens for each makerbot printer for automatic connection.',
'''\
[DEFAULT] 
send_to_server = True

[{machine_name}]
auth = {auth}
enable = True
''',
existing_inputs=inputs
)
"""
