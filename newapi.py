
import json
import socket
import ctypes
import urllib3
import urllib
import socket
import time
import threading
import urllib.parse
import urllib.request
import select
import struct


TOKEN_CONTEXTS = ['jsonrpc', 'put', 'camera']

PRINT_SEND = False
PRINT_RECV = False
WIRELESS = False


class JsonRPC:

    def __init__(self, host, port):
        self.running = True
        self.host = host
        self.port = port

        self.req_id = -1

        self.messages = {}
        self.unsolicited_messages = []

        #self.connect()

    def connect(self):
        self.rpc_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print('Connecting:', self.host, self.port)
        self.rpc_sock.connect((self.host, self.port))
        print('Starting thread')
        t = threading.Thread(target=self.rpc_reader_task)
        t.setDaemon(True)
        t.start()

    def rpc_reader_task(self):
        buf = b''
        while self.running:
            if not select.select([self.rpc_sock], [], [], 0)[0]:
                time.sleep(0.01)
                continue
            resp = self.rpc_sock.recv(4096)
            if not resp:
                # Do something useful?
                #raise Exception('Null response!!!')
                print('Null response!!')
                break
            buf += resp
            while True:
                msg, buf = self.next_message(buf)
                if msg is None:
                    break
                self.handle_message(msg.decode('utf8'))

    def next_message(self, buf):
        if not buf:
            return None, buf
        depth = 0
        if not buf.startswith(b'{'):
            raise Exception('Invalid buf: %s' % buf)
        for i in range(len(buf)):
            c = buf[i:i+1]
            if c == b'{':
                depth += 1
                continue
            elif c == b'}':
                depth -= 1
            else:
                continue
            if depth <= 0:
                msg = buf[:i+1]
                return msg, buf[i+1:]
        return None, buf

    def handle_message(self, msg):
        if PRINT_RECV:
            print('Received:', msg)
        msg = json.loads(msg)
        if 'id' in msg:
            resp_id = msg['id']
            self.messages[resp_id] = msg
        else:
            self.unsolicited_messages.append(msg)

    def request_response(self, method, params):
        req_id = self.next_request_id()
        jsonrpc = self.generate_json_rpc(method, params, req_id)
        self.send(jsonrpc)
        #print('Awaiting response...')
        resp = self.wait_response(req_id)
        #print('Got response!')
        if 'error' in resp:
            err_code = err['code']
            msg = err['message']
            raise Exception('RPC Error: %s Message: %s' % (err_code, msg))
        return resp

    def next_request_id(self):
        self.req_id += 1
        return self.req_id

    def generate_json_rpc(self, method, params, req_id):
        jsonrpc = {     'id': req_id,
                   'jsonrpc': '2.0',
                    'method': method,
                    'params': params}
        return json.dumps(jsonrpc)

    def send(self, jsonrpc):
        if PRINT_SEND:
            print('Sent:', jsonrpc)
        self.rpc_sock.sendall(jsonrpc.encode('utf8'))

    def wait_response(self, req_id, timeout=30):
        start_t = time.time()
        while time.time() < start_t + timeout:
            if req_id not in self.messages:
                continue
            return self.messages.pop(req_id)
        return None
    
    def finish(self):
        self.running = False
        self.rpc_sock.shutdown(socket.SHUT_RDWR)
        self.rpc_sock.close()

class MakerbotAPI:

    def __init__(self, host, auth_code=None, usename=None):
        
        self.auth_code = auth_code

        # TODO: Specify this in the constructor?
        self.client_id = 'MakerWare'
        self.client_secret = 'python-makerbotapi'

        self.username = 'conveyor'
        if usename is not None:
            self.username = username

        self.username_params = {'username': usename,
                                'host_version': '1.0'}

        self.host = host

        # Stores: builder, commit, firmware_version, isserial,
        #         machine_name, machine_type, vid, pid, and bot_type.
        self.handshake_result = None

        print('Creating jsonrpc...')
        self.json_rpc = JsonRPC(host, 9999)
        
    def finish(self):
        self.json_rpc.finish()
        
    def connect(self):
        self.json_rpc.connect()
        print('Rpc connected.')
        self.do_handshake()
        #print('Handshake complete'))

    def do_handshake(self):
        while True:
            print('Trying handshake...')
            resp = self.json_rpc.request_response('handshake', self.username_params)
            if resp is not None:
                break
        self.handshake_result = resp['result']

    def authenticate_fcgi(self, timeout=120):
        query_args = {'response_type': 'code',
                          'client_id': self.client_id,
                      'client_secret': self.client_secret}
        response = self.send_fcgi('auth', query_args)

        answer_code = response['answer_code']

        query_args = {'response_type': 'answer',
                          'client_id': self.client_id,
                      'client_secret': self.client_secret,
                        'answer_code': answer_code}
        start_t = time.time()
        while True:
            resp = self.send_fcgi('auth', query_args)

            answer = resp['answer']

            if answer == 'accepted':
                self.auth_code = resp['code']
                break
            elif answer == 'rejected':
                raise Exception('Press a button on the printer to disable pairing.')

            if time.time() >= start_t + timeout:
                raise Exception('Authentication timed out.')

            time.sleep(5)

    def authenticate_json_rpc(self):
        resp = self.json_rpc.request_response('authenticate',
                {'access_token': self.get_access_token('jsonrpc')})

    def get_access_token(self, context):
        # OAuth FCGI interface

        if context not in TOKEN_CONTEXTS:
            raise Exception('Unrecognised context: %s' % repr(context))

        query_args = {'response_type': 'token',
                          'client_id': self.client_id,
                      'client_secret': self.client_secret,
                          'auth_code': self.auth_code,
                            'context': context}
        resp = self.send_fcgi('auth', query_args)

        if resp['status'] == 'success':
            return resp['access_token']
        else:
            raise Exception('Authentication error: %s' % repr(resp['message']))

    def send_fcgi(self, path, query_args):
        encoded_args = urllib.parse.urlencode(query_args)

        url = 'http://%s/%s?%s' % (self.host, path, encoded_args)

        resp = urllib.request.urlopen(url)

        return json.load(resp)

    def get_system_information(self):
        resp = self.json_rpc.request_response('get_system_information', {})
        return resp

    def get_camera_image(self):
        token = self.get_access_token('camera')
        url = 'http://%s/camera?token=%s' % (self.host, token)
        data = urllib.request.urlopen(url).read()
        return struct.unpack('!IIII%ss' % (len(data) - ctypes.sizeof(ctypes.c_uint32 * 4)), data)

    def save_camera_jpg(self, fname):
        _, width, height, _, yuv_image = self.get_camera_image()
        # print('dim:', width, height, 'data len:', len(yuv_image))
        f = open(fname, 'wb')
        f.write(yuv_image)
        f.close()

    # TODO: Way of changing printer names...


class MakerbotDiscovery:

    def __init__(self, device = None):

        self.broadcast_sock = None
        self.answer_sock = None

        self.create_sockets(device= device)

    def create_sockets(self, device=None):

        self.broadcast_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.broadcast_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        if device is not None:
            print('Using device:', device)
            self.broadcast_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, device.encode('utf8'))
        self.broadcast_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.broadcast_sock.bind(('', 12309))

        self.answer_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.answer_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.answer_sock.bind(('', 12308))
        self.answer_sock.setblocking(0)
    
    def broadcast(self):    
        discover_req = json.dumps({"command": "broadcast"})
        self.broadcast_sock.sendto(discover_req.encode('utf8'), ('255.255.255.255', 12307))


    def discover(self, known_ips=None, wait=5, delay_broadcast = False):
        # Returns: ('<ip address>', '<machine name>', '<serial>')
        if known_ips is None:
            known_ips = []

        if not delay_broadcast:
            self.broadcast()


        answers = []
        print(answers)

        start_t = time.time()
        while time.time() <= start_t + wait:
            try:
                data, fromaddr = self.answer_sock.recvfrom(1024)
                ip = fromaddr[0]
                if ip not in known_ips:
                    info = json.loads(data)
                    machine_name = info['machine_name']
                    serial = info['iserial']
                    answers.append((ip, machine_name, serial))
            except socket.error:
                time.sleep(0.1)
        if delay_broadcast:
            self.broadcast()

        return answers

    def finish(self):

        for sock in [self.broadcast_sock, self.answer_sock]:

            sock.shutdown(socket.SHUT_RDWR)
            sock.close()

