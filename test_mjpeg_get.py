
import socket

def read_mjpeg_boundary(sock):

    request_headers = {
        'User-Agent': 'Python',
        'Accept': '*/*',
    }
    
    request_head = b'GET /stream.mjpg HTTP/1.1\r\n'
    for name, value in request_headers.items():
        request_head += f'{name}: {value}\r\n'.encode('utf8')
    request_head += b'\r\n'
    
    print('Head:')
    print(repr(request_head))
    
    sock.send(request_head)

    buf = b''
    while True:
        print('Getting...')
        buf += sock.recv(1024**2)
        print(buf)
        #input()
        print(b'\r\n\r\n' in buf)
        if b'\r\n\r\n' in buf:
        
            headers, buf = buf.split(b'\r\n\r\n', 1)
            break
    
    lines = headers.decode('utf8').split('\r\n')
    lines.pop(0)
    
    response_headers = {}
    for line in lines:
        print(repr(line))
        name, value = line.split(':', 1)
        name = name.lower().strip()
        value = value.strip()
        response_headers[name] = value
    
    content_type = response_headers['content-type']
    expect_type = 'multipart/x-mixed-replace; boundary='
    if not content_type.startswith(expect_type):
        print(repr(headers))
        print(repr(content_type))
        raise Exception('Bad content type for mjpeg!')
   
    boundary = content_type[len(expect_type):]
    
    return boundary, buf


if __name__ == '__main__':

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('192.168.0.66', 8000))
    
    boundary, buf = read_mjpeg_boundary(sock)
    
    print('Got boundary:', repr(boundary))
    
    sock.close()
    
