#import makerbotapi
import asyncio
import traceback
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
import newapi
import time
import os 
import threading, socket, SocketServer, http.server





# pymjpeg:
# https://github.com/damiencorpataux/pymjpeg

boundary = '--boundarydonotcross'

def pymjpeg_request_headers():
    return {
        'Cache-Control': 'no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0',
        'Connection': 'close',
        'Content-Type': 'multipart/x-mixed-replace;boundary=%s' % boundary,
        'Expires': 'Mon, 1 Jan 2030 00:00:00 GMT',
        'Pragma': 'no-cache',
		'Access-Control-Allow-Origin': '*' # CORS
    }

def pymjpeg_image_headers(filename):
    return {
        'X-Timestamp': time.time(),
        'Content-Length': os.path.getsize(filename),
        #FIXME: mime-type must be set according file content
        'Content-Type': 'image/jpeg',
    }

# FIXME: should take a binary stream
def pymjpeg_image(filename):
    with open(filename, "rb") as f:
        # for byte in f.read(1) while/if byte ?
        byte = f.read(1)
        while byte:
            yield byte
            # Next byte
            byte = f.read(1)



# https://stackoverflow.com/questions/46210672/python-2-7-streaming-http-server-supporting-multiple-connections-on-one-port

class Handler(http.server.BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path != '/':
            self.send_error(404, "Object not found")
            return
        self.send_response(200)
        self.send_header('Content-type', 'text/html; charset=utf-8')
        self.end_headers()

        # serve up an infinite stream
        i = 0
        while True:
            self.wfile.write("%i " % i)
            time.sleep(0.1)
            i += 1


class AsyncGetServer:
    
    def __init__(self, host='', port=8000, handler):
        self.host = host
        self.port = port

        self.handler = handler

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.sock.listen(5)

        self.threads_ready = 0
        self.thread_pool = []

        t = threading.Thread(target=self.refill_threads_task)
        t.setDaemon(True)
        t.start()

    def refill_threads_task(self):
        while True:

            if self.threads_ready >= 5:
                time.sleep(1)
                continue

            t = threading.Thread(target=self.handle_serve)
            t.setDaemon(True)
            t.start()
            self.threads_ready += 1

    def handle_serve(self):
        httpd = http.server.HTTPServer((self.host, self.port), self.handler, False)

        # Prevent the HTTP server from re-binding every handler.
        # https://stackoverflow.com/questions/46210672/
        httpd.socket = sock
        httpd.server_bind = self.server_close = lambda self: None

        httpd.serve_forever()




# Control if we are sending data to the server
SEND_TO_SERVER = True

from threading import Thread

class AsyncLoopThread(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.loop = asyncio.new_event_loop()

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.loop.run_forever()


# Create a class for makerbot printers
class MakerbotPrinter:
    def __init__(self, ip, auth_code):
        self.ip = ip
        self.auth_code = auth_code

    def connect(self):
        self.makerbot = newapi.MakerbotAPI(self.ip, self.auth_code)
        # self.makerbot.auth_code = auth_code
        print('Connecting...', self.ip)
        self.makerbot.connect()
        # self.makerbot.get_access_token('jsonrpc')
        print('Authenticating...')
        self.makerbot.authenticate_json_rpc()
        print('Getting system info...')
        self.name = self.makerbot.get_system_information()["result"]["machine_name"]

    # send the info of the Makerbot printer
    async def send_info(self, current_printers):
        try:
            print('About to connect!')
            self.connect()
            print('Loop.')
            await makerbot_printer_send(self.name, self.makerbot, current_printers)
        except Exception as e:
            print(traceback.format_exc())


# Information for Influxdb
#bucket = "Test"
#org = "Capstone2023"
#token = "4aFaY-CgICA4eRIzKCUuiYX-_yShjxqn6QDegWWua7Qtxw_tKF_e_JdmcrsmrsHPePdaBV-0UK2xA7inQSN8xg=="
# Store the URL of your InfluxDB instance
#url = "http://10.42.1.1:8086"

bucket = "Test"
org = "Capstone2023"
token = "4aFaY-CgICA4eRIzKCUuiYX-_yShjxqn6QDegWWua7Qtxw_tKF_e_JdmcrsmrsHPePdaBV-0UK2xA7inQSN8xg=="
# Store the URL of your InfluxDB instance
url = "http://192.168.0.103:8086"

# "initial_heating = 1"
# "homing = 2"
# "final heating = 3"
# "printing = 4"
# "cleaning_up = 5"
# "completed = 6"

authentication_dict = {"Printer 001" : 'OZnEMWIUOwVYFZHpdXJcJPteACJxbWnH',
                       "Printer 002" : 'vAzoexoBEKpMjWelEKkPOQzGhFPpqOtQ'}

# Step Encodings
# TODO: Add cancel steps!
STEP_NUMBERS = {'idle': 0,
                'clear_build_plate': 7,
                'initial_heating': 1,
                'homing': 2,
                'final_heating': 3,
                'printing': 4,
                'cleaning_up': 5,
                'completed': 6,
                'cooling': 8,
                'cancelling': 9}
# extrusion, 


async def makerbot_printer_send(name, machine, current_printers):
    try:
        prev_step = 0
        while True:
            print('Getting system info...')
            # Retrieve data from system
            # TODO: Make the below async compatible (especially printer timeout on off becomes problematic for gathering information from the other printers.)
            data = machine.get_system_information()
            print("Got system info")
            #print(data["result"])#["machine_name"])
            if not SEND_TO_SERVER:
                await asyncio.sleep(30)
                continue
            current_process = data['result']['current_process']
            # Send the name of the device and the extruder temperature to influxdb
            p = influxdb_client.Point("send efficient 2") \
                .tag("Name", data["result"]["machine_name"]) \
                .field("Target Temperature", data["result"]["toolheads"]["extruder"][0]["target_temperature"]) \
                .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"])

            # Set the step to be idle and percentage done to be 0 as default

            step_name = 'idle'
            perc_done = 0

            # if the printer is printing get the step and the percentage done of the job
            if current_process is not None:
                step_name = current_process['step']
                perc_done = current_process.get('progress', 0)
            step_num = STEP_NUMBERS[step_name]
            p = p.field("Percentage Done", perc_done) \
                .field("Step", step_num)
                       
            if step_num == STEP_NUMBERS['idle']:
                tmp = 0
            if step_num == STEP_NUMBERS['initial_heating'] and prev_step != step_num:
            	p = p.field("Print Event", 1)
            	
            if step_num == STEP_NUMBERS["completed"] and prev_step != step_num:
                machine.save_camera_jpg(f"test_{name}.jpg")
                p = p.field("Print Event", 0)
            
            if step_num == STEP_NUMBERS['idle'] and prev_step == 5:
            	p = p.field('Print Event', 2)
           


            # if the printer is not printing send data every 30 seconds
            if current_process is None:
                await asyncio.sleep(2)

            # if the printer is printing send data every 5 seconds
            else:
                await asyncio.sleep(5)

            # write the data into influxdb
            async with InfluxDBClientAsync(url=url, token=token, org=org) as client:
                await client.write_api().write(bucket=bucket, record=p)
            print('sent')
            print(p)
            prev_step = step_num

    except Exception as e:
        print(traceback.format_exc())
    try:
        print(name, "has turned off!")
        p = p.field("Step", -1)
        async with InfluxDBClientAsync(url=url, token=token, org=org) as client:
            await client.write_api().write(bucket=bucket, record=p)
        del current_printers[name]
        machine.finish()
    except Exception as e:
        print(traceback.format_exc())
            
async def main():
    print('Sending offs...')
    for name in authentication_dict:
        p = influxdb_client.Point("send efficient 2") \
                .tag("Name", name) \
                .field("Extruder Temperature", 0) \
                .field("Target Temperature", 0) \
                .field("Percentage Done", 0) \
                .field("Step", -1)
        async with InfluxDBClientAsync(url=url, token=token, org=org) as client:
            await client.write_api().write(bucket=bucket, record=p)
    print('We have declared that the printers are off!')
    loop_handler = AsyncLoopThread()
    loop_handler.start()
    discovery = newapi.MakerbotDiscovery(device=b'enp7s0')
    current_printers = {}
    print("start")
    while True:
        found = discovery.discover()
        print('Found:', found)
        for ip, name, identity in found:
            if name not in current_printers:
                print("found new printer:", name)
                current_printers[name] = asyncio.run_coroutine_threadsafe(MakerbotPrinter(ip, authentication_dict[name]).send_info(current_printers), loop_handler.loop)
                print("Added Printer")



asyncio.run(main())
