# TODO: Check if we have a file on the server for the cnc machine in the machines folder...

# Simple example of reading the MCP3008 analog input channels using its
# differential mode.  Will print the difference of channel 0 and 1.
# Author: Tony DiCola
# License: Public Domain
import time
import traceback

# Import SPI library (for hardware SPI) and MCP3008 library.
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008


# Software SPI configuration:
# CLK  = 18
# MISO = 23
# MOSI = 24
# CS   = 25
# mcp = Adafruit_MCP3008.MCP3008(clk=CLK, cs=CS, miso=MISO, mosi=MOSI)

# Hardware SPI configuration:

import serial
import matplotlib.pyplot as plt
import numpy as np
import time
import numpy.fft 
from threading import Semaphore, Thread
from iot_influxdb import InfluxDBWriter
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS



SPI_PORT   = 0
SPI_DEVICE = 0
mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))



print_sem = Semaphore(1)
old_print = print
def print(*args, **kw):
    print_sem.acquire()
    old_print(*args, **kw)
    print_sem.release()


PLOT_RAW = False

influx_writer = InfluxDBWriter()



import serial.tools.list_ports
ports = list(serial.tools.list_ports.comports())
for p in ports:
    print(p.description)
    if "Arduino" in p.description:
        print("This is an Arduino!")


plt.ion() # Stop matplotlib windows from blocking

# Setup figure, axis and initiate plot
fig, ax = plt.subplots()
xdata, ydata = [], []
ln, = ax.plot([], [], 'ro-')
ser = serial.Serial(ports[-1].device, 9600)
count = 0
ax.set_xlim([0, 800])
ax.set_ylim([0, 1000])
if PLOT_RAW:
    ax.set_ylim([800, 900])
fdata = []

t = time.time()

WINDOW = 800  # 16000

last_vals = []
print_start_time = None

prev_step = 0

offset = 512
while True:
    current_time = time.time()
    delay = 0.01 - (current_time - t)
    t = current_time
    if delay > 0 :
        time.sleep(delay)
    else:
        pass  # print(f'Something Wrong, {delay}')
    s = mcp.read_adc(7)
        
 
    
    # Get the new data
    xdata.append(count)
    ydata.append(s)
        
        
    if len(xdata) > WINDOW:
        xdata.pop(0)
        ydata.pop(0)
        fdata.append(numpy.max(ydata[-50:]))   
        if len(fdata) > WINDOW:
            fdata.pop(0)
         
        
    if count%WINDOW == 0 and count > WINDOW:
        # Reset the data in the plot
        if PLOT_RAW:
            tydata = ydata
        else:
            tydata = []
            if ydata:
            
                tydata = np.abs(numpy.fft.fft(np.array(ydata)- offset))
                tydata[0:180] = 0
                tydata[-180:] = 0
                max_current = np.max(tydata)

                print('Sub point:', max_current)
                last_vals.append(max_current)
                if len(last_vals) > 5:
                    last_vals.pop(0)
                else:
                    count += 1
                    continue
                max_current = sum(last_vals)/len(last_vals)

                print('Max current:', max_current)
                step = -1
                if max_current > 42000:  # 1000
                    step = 4
                elif max_current > 20000:
                    step = 0


                p = influxdb_client.Point("send efficient 2") \
                    .tag("Name", influx_writer.machine_name) \
                    .field("Step", step)\
                    .field("Current", max_current)

                if print_start_time is not None:
                    p = p.field("Elapsed Time", round(time.time()-print_start_time))
                if prev_step == 0 and step == 4:
                    p = p.field('Job Start Event', 1)
                    print_start_time = time.time()
                else:
                    p = p.field('Job Start Event', 0)
                if prev_step == 4 and step == 0:
                    p = p.field('Job Finish Event', 1)
                    print_start_time = None
                else:
                    p = p.field('Job Finish Event', 0)

                influx_writer.write_influx(p)
                print(f'This is the step: {step}')

                prev_step = step
                
              
    count += 1
'''
WINDOW = 1600  # 800


offset = 512
while True:
    current_time = time.time()
    delay = 0.01 - (current_time - t)
    t = current_time
    if delay > 0 :
        time.sleep(delay)
    else:
        pass  # print(f'Something Wrong, {delay}')
    s = mcp.read_adc(7)
        
 
    
    # Get the new data
    xdata.append(count)
    ydata.append(s)
        
        
    if len(xdata) > WINDOW:
        xdata.pop(0)
        ydata.pop(0)
        fdata.append(numpy.max(ydata[-50:]))   
        if len(fdata) > WINDOW:
            fdata.pop(0)
         
        
    if count%WINDOW == 0 and count > WINDOW:
        # Reset the data in the plot
        if PLOT_RAW:
            tydata = ydata
        else:
            tydata = []
            if ydata:
            
                tydata = np.abs(numpy.fft.fft(np.array(ydata)- offset))
                tydata[0:180] = 0
                tydata[-180:] = 0
                max_current = np.max(tydata)
                print('Max current:', max_current)
                step = -1
                if max_current > 950:  # 1000
                    step = 4
                elif max_current > 300:
                    step = 0
                
                p = influxdb_client.Point("send efficient 2") \
                    .tag("Name", influx_writer.machine_name) \
                    .field("Step", step)\
                    .field("Current", max_current)
                influx_writer.write_influx(p)
                print(f'This is the step: {step}')
                
              
    count += 1




'''
