import sys
from http.server import HTTPServer, BaseHTTPRequestHandler
import os
import os.path
import mimetypes

this_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(this_path)

WEB_DIR = 'web_stuff'
if not os.path.exists(WEB_DIR):
    os.mkdir(WEB_DIR)

class Redirect(BaseHTTPRequestHandler):
    def do_GET(self):
        '''
        self.send_response(302)
        self.send_header('Location', 'https://grafana.iotcapstone.space/d/Z1bDJxsVk/test-homepage?orgId=1&refresh=5s')
        self.end_headers()
        '''
        #self.send_header('Location', 'https://grafana.iotcapstone.space/public-dashboards/719e30ba8905498e8e9585f9744008f3')
        # http://localhost:3000/public-dashboards/719e30ba8905498e8e9585f9744008f3
        print(f'Path is {repr(self.path)}')
        if self.path == '/':
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            #if self.path == '/':
            with open(os.path.join(WEB_DIR, 'landing_page.html'), 'rb') as file: 
                self.wfile.write(file.read())
        elif self.path.startswith('/stuff/'):
            fname = os.path.join(WEB_DIR, self.path[len('/stuff/'):])
            fnames = [os.path.join(WEB_DIR, fname) for fname in os.listdir(WEB_DIR)]
            print(f'Did not find: {repr(fname)} among {fnames}.')
            if fname not in fnames:
                self.send_response(404)
                #self.send_header('Content-type', 'text/html')
                self.end_headers()
                return
            self.send_response(200)
            the_mime = mimetypes.guess_type(fname)[1]
            print(f'Mime for {fname} is {the_mime}...')
            self.send_header('Content-type', the_mime)
            self.end_headers()
            with open(fname, 'rb') as file: 
                self.wfile.write(file.read())
        else:
            self.send_response(404)
            #self.send_header('Content-type', 'text/html')
            self.end_headers()
        print('Stuff was sent!')

if __name__ == '__main__':
    HTTPServer(("", 8002), Redirect).serve_forever()

