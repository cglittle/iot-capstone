eps = 0.001;
$fn = 100;


module centered_cube(size) {
    translate([-size[0]/2, -size[1]/2, 0]) {
        cube(size);
    }
}

module main_cube() {
    centered_cube([length, width, height]);
}

module outer_shell() {
    minkowski() {
        main_cube();
        sphere(r=wall, $fn=20);
    }
}

module enclosed_box() {
    difference() {
        outer_shell();
        main_cube();
    }
}

module top_mask() {
    thicker_wall = wall + eps;
    centered_cube([length+2*thicker_wall, width+2*thicker_wall, height+thicker_wall]);
}

module bot_mask() {
    thicker_wall = wall + eps;
    translate([0, 0, -height-thicker_wall]) centered_cube([length+2*thicker_wall, width+2*thicker_wall, height+thicker_wall+eps]);
}

module stand_positions() {
    for (i = [-1, 1]) {
        for (j = [-1, 1]) {
            translate([i*(length/2-stand_off), j*(width/2-stand_off), 0]) {
                children();
            }
        }
    }    
}

module bottom_plate() {
    difference() {
        enclosed_box();
        top_mask();
        stand_positions() {
            translate([0, 0, -wall-eps]) cylinder(r=plate_hole_r, h=wall+2*eps);
        }
    }
}

module arch(width, height, thick) {
    translate([0, 0, height]) rotate([-90, 0, 0]) cylinder(r=width/2, h=thick);
    translate([-width/2, 0, 0]) cube([width, thick, height]);
}

module box_top() {
    difference() {
        enclosed_box();
        bot_mask();
        translate([0, 0, -eps]) arch(clamp_r*2, clamp_r+eps, width/2+wall+eps);
    	translate([0, -width/2+AC_from_right, -eps]) {
		for (i = [-1, 1]) {
			rotate([0, 0, i*90]) arch(AC_r*2, AC_r+eps, length/2+wall+eps);
		}
	}
    }
    //}
    stand_positions() difference() {
        cylinder(r=stand_r, h=height+eps);
        translate([0, 0, -eps]) cylinder(r=stand_hole_r, h=stand_hole_h+eps);
    }
}


AC_r = 7/2;
clamp_r = 3/2;

length = 90;
width = 80;
height = 40;

AC_from_right = 20;

wall = 2;

M2 = 1.8/2;
M2_tol = 2.2/2;

stand_off = 1;
stand_r = 4/2;
stand_hole_r = M2;
stand_hole_h = 20;

plate_hole_r = M2_tol;

WHICH = 1;

rotate([180, 0, 0])
if (WHICH == 1) {
    box_top();
} else if (WHICH == 2) {
    bottom_plate();
};
