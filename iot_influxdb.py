import time
from threading import Thread, Semaphore
import configparser
import os
import os.path
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime
import json
import traceback
import requests
from misc import MAIN_CONFIG_JSON, MACHINE_INFO_JSON


class InfluxDBWriter:
    def __init__(self, machine_info_file=MACHINE_INFO_JSON):

        # TODO: Use machine_info.json, and do not just provide a single name!
        with open(machine_info_file) as f:
            content = f.read()
        self.pi_info = json.loads(content)
        self.pi_name = self.pi_info['name']
        self.machine_name = None
        for machine in self.pi_info['machines']:
            self.machine_name = machine['name']

        with open(MAIN_CONFIG_JSON) as f:
            principal_config = json.loads(f.read())

        self.influxdb_instances = principal_config['influxdb_instances']
        #influxdb_infos = []
        #influxdb_options
        for name, use_info in self.influxdb_instances.items():
            bucket = use_info['bucket']
            org = use_info['org']
            token = use_info['token']
            url = use_info['url']
            measurement = use_info['measurement']
            print('InfluxDB:', name)
            print('Bucket:', bucket)
            print('Org:', org)
            print('Token:', token)
            print('URL:', url)
            print('measurement:', measurement)

        local_instance = self.influxdb_instances['local']
        self.measurement_name = local_instance['measurement']
        self.bucket = local_instance['bucket']
        self.org = local_instance['org']
        if not self.influxdb_instances:
            raise Exception('No influxdb database listed!')
    
        self.client = influxdb_client.InfluxDBClient(
                url=url,
                token=token,
                org=org
        )
        self.query_api = self.client.query_api()
        self.message_queue = []
        t = Thread(target=self.influxdb_writer, args=(), daemon=True)
        t.start()
        
    def write_influx(self,record, the_day=None):
        
        #current_time = datetime.utcnow().timestamp() * 1000
        #current_time = int(current_time)
        if the_day is None:
            the_day = datetime.utcnow()
        current_time = the_day.strftime('%Y-%m-%dT%H:%M:%SZ')
        #.strftime('%Y-%m-%dT%H:%M:%SZ')
        #current_time = current_time.encode('utf8')
        #print(repr(current_time))
        #record = record.field('time', current_time)
        record = record.time(current_time)
        #print(record)
        self.message_queue.append(record)
        
    
    def influxdb_writer(self):
        print('starting thread')
        while True:
            while not self.message_queue:
                time.sleep(0.01)
            record = self.message_queue.pop(0)           
            for name, use_info in self.influxdb_instances.items():

                enabled = use_info['enabled']

                #print(name, 'enabled:', repr(enabled))

                if not enabled:
                    continue

                bucket = use_info['bucket']
                org = use_info['org']
                token = use_info['token']
                url = use_info['url']



                #print('ACQUIRE:', record)
                #influx_sem.acquire()
                try:
                    client = influxdb_client.InfluxDBClient(
                        url=url,
                        token=token,
                        org=org
                    )
                    write_api = client.write_api(write_options=SYNCHRONOUS)
                    write_api.write(bucket=bucket, record=record)
                    print('MQ:SENT STUFF')
                except:
                    print(traceback.format_exc())
                    self.message_queue.insert(0, record)
                    print(self.message_queue)
                    time.sleep(5)
                    
                finally:
                    #print('RELEASE')
                    pass
                    #influx_sem.release()
                    
    def influxdb_reader(self):
      
        try:
            pass
            
        except:
            print(traceback.format_exc())
            time.sleep(5)
                    
    def change_measurement_name(self, old_name, new_name):
        
        if True:
            print('TODO: Change measurement to the name, do nto copy "send efficient 2"...')
            return
    
        for name, use_info in self.influxdb_instances.items():

            enabled = use_info['enabled']

            #print(name, 'enabled:', repr(enabled))

            if not enabled:
                continue

            bucket = use_info['bucket']
            org = use_info['org']
            token = use_info['token']
            url = use_info['url']

            if url.endswith('/'):
                url = url[:-len('/')]
            # r = requests.get(f'{url}/api/v2/buckets?limit=1&offset=50', headers={'Authorization': f'Token {token}'})
            '''
SELECT * INTO MyMeasurementCopy from MyMeasurement GROUP BY *
DROP MEASUREMENT MyMeasurement


curl --get "$INFLUX_HOST/query?org=$INFLUX_ORG&bucket=get-started" \
  --header "Authorization: Token $INFLUX_TOKEN" \
  --data-urlencode "db=get-started" \
  --data-urlencode "rp=autogen" \
  --data-urlencode "q=SELECT co,hum,temp,room FROM home WHERE time >= '2022-01-01T08:00:00Z' AND time <= '2022-01-01T20:00:00Z'"


They talk about a post instead of get for /query.

They say urlencode, so use data= for post or is it get and in params?
(for json it would be json=)

TODO: Look at /write api endpoint too in case that is needed for modifying the database...
            '''
            # https://docs.influxdata.com/influxdb/v2/get-started/query/?t=InfluxDB+API
            r = requests.get(f'{url}/query', params={'rp': autogen, 'org': org, 'bucket': bucket, 'db': bucket, 'q': f'SELECT * INTO "{new_name}" from "{old_name}" GROUP BY *'}, headers={'Authorization': f'Token {token}'})
            #r = requests.get(f'{url}?db=mydb', data={'q': f'SELECT *     FROM "{old_name}"'}, headers={'Authorization': f'Token {token}'})
            print('Response:')
            print(r.status_code)
            print(r.reason)
            print(r.content)
            data = json.loads(r.content)

            r = requests.get(f'{url}/query', params={'rp': autogen, 'org': org, 'bucket': bucket, 'db': bucket, 'q': f'DROP MEASUREMENT "{old_name}"'}, headers={'Authorization': f'Token {token}'})
            print('Response:')
            print(r.status_code)
            print(r.reason)
            print(r.content)
            data = json.loads(r.content)


