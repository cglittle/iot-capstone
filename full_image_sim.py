
import subprocess as sp
import json

N_MACHINES = 50

LARGE_SIM_CONFIG_FNAME = 'large_lab_config.json'
LARGE_SIM_OUT_FOLDER = 'large_virtual_lab'

TO_WAIT = True

lab_config = {}


start_i = 1
for i in range(start_i, start_i+N_MACHINES):
    
    pi_name = 'Sim Pi %03d' % i
    
    printer_name = 'Sim Printer %03d' % i
    
    machine_name = printer_name
    machine_type = 'Sim Makerbot Printer'
    dash_name = f'New Sim Printer {i}'
    config = {'folder': 'New Sim Printers'}  # The folder for the printer to be located in Grafana
    machine_info = {'name': machine_name, 'type': machine_type, 'dash_name': dash_name, 'config': config}
    
    lab_config[pi_name] = [machine_info]

with open(LARGE_SIM_CONFIG_FNAME, 'w') as f:
    f.write(json.dumps(lab_config, indent=2))

print('Wrote large lab config file!')
if False:
    print(json.dumps(lab_config, indent=2))
    exit()

print()
print('Running the lab:')

wait = []
if TO_WAIT:
    wait = ['--wait']
sp.call(['python3', 'virtual_lab.py', '--run', '--regenerate', '-f', LARGE_SIM_CONFIG_FNAME, '-o', LARGE_SIM_OUT_FOLDER]+wait)

