from pylatex import Document, Section, Command, Tabular, Subsection, Figure, TextColor
from pylatex.utils import NoEscape
import influxdb_client
from time import strftime, gmtime
import pytz
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import numpy as np
import configparser
import os.path
import os


USE_CLOUD = True

CONFIG_FILE = 'iot4config.ini'


config = configparser.ConfigParser()
if os.path.exists(CONFIG_FILE):
    config.read(CONFIG_FILE)

influxdb_instances = {}

print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    influxdb_instances[name] = config[name]

use_info = influxdb_instances['local']
bucket = use_info['bucket']
org = use_info['org']
token = use_info['token']
url = use_info['url']



def print_events(query_api, device_name, start, stop):
    if stop:
        query_events = f'from(bucket:"Test")\
            |> range(start: {start}, stop: {stop})\
            |> filter(fn:(r) => r._measurement == "send efficient 2")\
            |> filter(fn:(r) => r._field == "Print Event")\
            |> filter(fn:(r) => r.Name == "{device_name}")'
    else:
        query_events = f'from(bucket:"Test")\
            |> range(start: {start})\
            |> filter(fn:(r) => r._measurement == "send efficient 2")\
            |> filter(fn:(r) => r._field == "Print Event")\
            |> filter(fn:(r) => r.Name == "{device_name}")'
    events_table = query_api.query(org=org, query=query_events)
    events = []
    for table in events_table:
        for record in table.records:
            events.append(record.get_value())
    prints = events.count(1)
    completes = events.count(3)
    cancels = events.count(2)

    return prints, completes, cancels


def print_times(query_api, device_name):
    query_step = f'from(bucket:"Test")\
        |> range(start: -24h)\
        |> filter(fn:(r) => r._measurement == "send efficient 2")\
        |> filter(fn:(r) => r._field == "Step")\
        |> filter(fn:(r) => r.Name == "{device_name}")'

    prev_query = f'from(bucket:"Test")\
        |> range(start: -48h, stop: -24h)\
        |> filter(fn:(r) => r._measurement == "send efficient 2")\
        |> filter(fn:(r) => r._field == "Step")\
        |> filter(fn:(r) => r.Name == "{device_name}")\
        |> last()'

    current_time = datetime.utcnow()
    step_table = query_api.query(org=org, query=query_step)
    prev_step_table = query_api.query(org=org, query=prev_query)
    idle_time = 0
    printing_time = 0
    off_time = 0

    for table in prev_step_table:
        prev_step = table.records[0].get_value()

    for table in step_table:
        first_time = table.records[0].get_time()
        timestep = (first_time.replace(tzinfo=None) - (current_time - timedelta(days=1))).seconds
        if prev_step == 0 or prev_step == 6:
            idle_time += timestep
        elif prev_step == -1:
            off_time += timestep
        else:
            printing_time += timestep

    for table in step_table:
        for i in range(len(table.records) - 1):
            timestep = (table.records[i + 1].get_time() - table.records[i].get_time()).seconds
            step = table.records[i].get_value()
            if step == 0 or step == 6:
                idle_time += timestep
            elif step == -1:
                off_time += timestep
            else:
                printing_time += timestep

    for table in step_table:
        final_time = table.records[-1].get_time()
        final_step = table.records[-1].get_value()
        timestep = (current_time - final_time.replace(tzinfo=None)).seconds
        if final_step == 0 or final_step == 6:
            idle_time += timestep
        elif final_step == -1:
            off_time += timestep
        else:
            printing_time += timestep

    return idle_time, printing_time, off_time


def seconds_to_hm(times_in_secs):
    hm_times = []
    for time in times_in_secs:
        hm_times.append(strftime("%H:%M", gmtime(time)))
    return hm_times


def print_job_list(query_api, device_name):
    tz = pytz.timezone('Australia/Melbourne')
    query_events = f'from(bucket:"Test")\
            |> range(start: -24h)\
            |> filter(fn:(r) => r._measurement == "send efficient 2")\
            |> filter(fn:(r) => r._field == "Print Event")\
            |> filter(fn:(r) => r.Name == "{device_name}")'
    events_table = query_api.query(org=org, query=query_events)
    print_jobs = []
    complete_jobs = []
    print_active = False
    i = 0
    for table in events_table:
        for record in table.records:
            if record.get_value() == 1:
                print_active = True
                start_time = (record.get_time()).astimezone(tz)
            if record.get_value() == 2 and print_active:
                i += 1
                print_active = False
                end_time = (record.get_time()).astimezone(tz)
                print_time = end_time - start_time
                print_jobs.append([start_time.strftime("%H:%M"), end_time.strftime("%H:%M"),
                                   strftime("%H:%M", gmtime(print_time.seconds)), 0])
            if record.get_value() == 3 and print_active:
                i += 1
                print_active = False
                end_time = (record.get_time()).astimezone(tz)
                print_time = end_time - start_time
                print_jobs.append([start_time.strftime("%H:%M"), end_time.strftime("%H:%M"),
                                   strftime("%H:%M", gmtime(print_time.seconds)), 1])
                complete_jobs.append([device_name, record.get_time(), i])

        if print_active:
            current_time = datetime.now(tz=tz)
            print_time = current_time - start_time
            print_jobs.append([start_time.strftime("%H:%M"), 0, strftime("%H:%M", gmtime(print_time.seconds)), -1])
    return print_jobs, complete_jobs



if __name__ == '__main__':
    url = "https://influxdb.iotcapstone.space"
    list_of_devices = ["Printer 001", "Printer 002"]

    client = influxdb_client.InfluxDBClient(
        url=url,
        token=token,
        org=org
    )
    query_api = client.query_api()

    p1_events = print_events(query_api, "Printer 001", "-24h", None)
    p2_events = print_events(query_api, "Printer 002", "-24h", None)


    total_events_week = [0, 0, 0]
    total_events_month = [0, 0, 0]

    day_by_day_events_week = []

    for device in list_of_devices:
        week_events = print_events(query_api, device, "-7d", "-1s")
        # month_events = print_events(query_api, device, "-28d", "-1s")
        total_events_week = np.add(total_events_week, week_events)
        # total_events_month = np.add(total_events_month, month_events)

    daily_events_total = [0, 0, 0]
    for device in list_of_devices:
        daily_events = print_events(query_api, device, f"-7d", f"-6d")
        daily_events_total = np.add(daily_events_total, daily_events)
    day_by_day_events_week = np.array([daily_events_total])

    for i in range(6, 0, -1):
        daily_events_total = [0, 0, 0]
        for device in list_of_devices:
            daily_events = print_events(query_api, device, f"-{i}d", f"-{i - 1}d")
            daily_events_total = np.add(daily_events_total, daily_events)
        day_by_day_events_week = np.append(day_by_day_events_week, [daily_events_total], axis=0)

    print(day_by_day_events_week[:, 0])

    p1_times = print_times(query_api, "Printer 001")
    p2_times = print_times(query_api, "Printer 002")
    average_times = [0, 0, 0]
    average_times[0] = (p1_times[0] + p2_times[0]) / 2
    average_times[1] = (p1_times[1] + p2_times[1]) / 2
    average_times[2] = (p1_times[2] + p2_times[2]) / 2

    p1_times_hm = seconds_to_hm(p1_times)
    p2_times_hm = seconds_to_hm(p2_times)
    average_times_hm = seconds_to_hm(average_times)

    # PDF Code Starts Here
    geometry_options = {"tmargin": "2cm", "lmargin": "2cm", "rmargin": "2cm"}
    doc = Document(geometry_options=geometry_options, documentclass='article')
    doc.preamble.append(Command('title', 'IOT Capstone Daily Report'))
    doc.preamble.append(Command('date', NoEscape(r'\today')))
    doc.append(NoEscape(r'\maketitle'))

    with doc.create(Section('Print Jobs')):
        with doc.create(Tabular('|c|c|c|c|')) as table:
            table.add_hline()
            table.add_row(("", "Print Jobs Started", "Print Jobs Completed", "Print Jobs Cancelled"))
            table.add_hline()
            table.add_row(("Printer 001", p1_events[0], p1_events[1], p1_events[2]))
            table.add_hline()
            table.add_row(("Printer 002", p2_events[0], p2_events[1], p2_events[2]))
            table.add_hline()
            table.add_hline()
            table.add_row(
                ("Daily Total", p1_events[0] + p2_events[0], p1_events[1] + p2_events[1], p1_events[2] + p2_events[2]))
            table.add_hline()
            table.add_row(
                ("Weekly Total", total_events_week[0], total_events_week[1], total_events_week[2]))
            table.add_hline()
            # table.add_row(
            # ("Monthly Total", total_events_month[0], total_events_month[1], total_events_month[2]))
            # table.add_hline()
    with doc.create(Section('Machine Time')):
        with doc.create(Tabular('|c|c|c|c|')) as table2:
            table2.add_hline()
            table2.add_row(("", "Printing Time", "Idle Time", "Powered Off Time"))
            table2.add_hline()
            table2.add_row(("Printer 001", p1_times_hm[1], p1_times_hm[0], p1_times_hm[2]))
            table2.add_hline()
            table2.add_row(("Printer 002", p2_times_hm[1], p2_times_hm[0], p2_times_hm[2]))
            table2.add_hline()
            table2.add_hline()
            table2.add_row(("Average", average_times_hm[1], average_times_hm[0], average_times_hm[2]))
            table2.add_hline()
        with doc.create(Figure(position='htbp')) as plot:
            width = r'0.4\textwidth'
            labels = 'Idle', 'Printing', 'Powered Off'
            sizes = [p1_times[0], p1_times[1], p1_times[2]]
            fig, ax = plt.subplots()
            ax.pie(sizes, labels=labels, colors=['yellow', 'green', 'red'])
            plot.add_plot(width=NoEscape(width))
            plot.add_caption('Printer 1 Usage Today')

    with doc.create(Section('List of Print Jobs')):
        completes = []
        for device in list_of_devices:
            with doc.create(Subsection(device)):
                job_list, complete_dev = print_job_list(query_api, device)
                completes += complete_dev
                if len(job_list) == 0:
                    doc.append('No Prints Today')
                else:
                    i = 1
                    with doc.create(Tabular('|c|c|c|c|c|')) as table3:
                        table3.add_hline()
                        table3.add_row(("Print No.", "Start Time", "End Time", "Print Time", "Completion Status"))
                        table3.add_hline()
                    print(job_list)
                    for job in job_list:
                        if job[3] == 1:
                            table3.add_row((i, job[0], job[1], job[2], "Completed"))
                            table3.add_hline()
                            i += 1
                        elif job[3] == 0:
                            table3.add_row((i, job[0], job[1], job[2], "Cancelled"), color="red")
                            table3.add_hline()
                            i += 1
                        else:
                            table3.add_row((i, job[0], " ", job[2], "In Progress"), color="green")
                            table3.add_hline()
                            i += 1
    with doc.create(Section('Weekly Output')):
        with doc.create(Figure(position='htbp')) as weekly_plot:
            fig2, ax2 = plt.subplots()
            days = [1, 2, 3, 4, 5, 6, 7]
            starts = day_by_day_events_week[:, 0]
            comps = day_by_day_events_week[:, 1]
            cancels = day_by_day_events_week[:, 2]
            plt.plot(days, comps)
            plt.plot(days, cancels)
            plt.title('Weekly Prints')
            plt.xlabel('Day Number')
            plt.ylabel('Number of Prints')
            ax2.legend(['Prints Completed', 'Prints Cancelled'])
            weekly_plot.add_plot(width=NoEscape(width))
            weekly_plot.add_caption('Weekly Prints')


    with doc.create(Section('Completed Print Images')):
        print(completes)
        if len(completes) == 0:
            doc.append('No Prints Today')
        else:
            for i in range(len(completes)):
                doc.append(f"Device Name: {completes[i][0]}, Job Number: {completes[i][2]}")
                with doc.create(Figure(position='h!')) as print_job_fig:
                    image_filename = f"completed_prints/{completes[i][0]}_{completes[i][1]}.jpg"
                    print_job_fig.add_image(image_filename,
                              width=NoEscape(r'\linewidth'))

    doc.generate_pdf('Daily_Report', clean_tex=False, compiler='pdflatex')
