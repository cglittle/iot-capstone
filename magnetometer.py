import py_qmc5883l
import time
import smbus					#import SMBus module of I2C
from time import sleep    
import time
from threading import Thread, Semaphore
import configparser
import os
import os.path
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime


print_sem = Semaphore(1)

old_print = print
def print(*args, **kw):
    print_sem.acquire()
    old_print(*args, **kw)
    print_sem.release()


CONFIG_FILE = 'iot4config.ini'

config = configparser.ConfigParser()
if os.path.exists(CONFIG_FILE):
    config.read(CONFIG_FILE)

printer_info = configparser.ConfigParser()
printer_info.read('printer_info.ini')
printer_name = printer_info['default']['printer_name']
print(printer_name)


USE_GUI = False

influxdb_instances = {}
#influxdb_infos = []
#influxdb_options
print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    instance = config[name]
    influxdb_instances[name] = instance
    use_info = instance
    bucket = use_info['bucket']
    org = use_info['org']
    token = use_info['token']
    url = use_info['url']
    print('InfluxDB:', name)
    print('Bucket:', bucket)
    print('Org:', org)
    print('Token:', token)
    print('URL:', url)



if not influxdb_instances:
    raise Exception('No influxdb database listed!')
    
    
message_queue = []
def write_influx(record):
    
    #current_time = datetime.utcnow().timestamp() * 1000
    #current_time = int(current_time)
    current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    #.strftime('%Y-%m-%dT%H:%M:%SZ')
    #current_time = current_time.encode('utf8')
    print(repr(current_time))
    #record = record.field('time', current_time)
    record = record.time(current_time)
    print(record)
    message_queue.append(record)
    
    
def influxdb_writer():
    while True:
        while not message_queue:
            time.sleep(0.01)
        record = message_queue.pop(0)           
        for name, use_info in influxdb_instances.items():

            enabled = use_info['enabled']

            print(name, 'enabled:', repr(enabled))

            if not enabled:
                continue

            bucket = use_info['bucket']
            org = use_info['org']
            token = use_info['token']
            url = use_info['url']



            print('ACQUIRE:', record)
            #influx_sem.acquire()
            try:
                client = influxdb_client.InfluxDBClient(
                    url=url,
                    token=token,
                    org=org
                )
                write_api = client.write_api(write_options=SYNCHRONOUS)
                write_api.write(bucket=bucket, record=record)
            except:
                print(traceback.format_exc())
                message_queue.insert(0, record)
                print(message_queue)
                time.sleep(5)
                
            finally:
                print('RELEASE')
                #influx_sem.release()


t = Thread(target=influxdb_writer, args=(), daemon=True)
t.start()



sensor = py_qmc5883l.QMC5883L()
while True:
    B = sensor.get_magnet_raw()
    print(B)
    p = influxdb_client.Point("send efficient 2") \
        .tag("Name", printer_name) \
        .field("Bx", B[0]) \
        .field('By', B[1]) \
        .field('Bz', B[2])  

    write_influx(p)
    time.sleep(1)
