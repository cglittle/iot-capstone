import json
import glob
import importlib
import os
import os.path
from threading import Thread
import traceback
import time
SEC = 1
MIN = 60*SEC
HOUR = 60*MIN
DAY = 24*HOUR
WEEK = 7*DAY
MONTH = 30*DAY

MAIN_CONFIG_JSON = 'config.json'
MACHINE_INFO_JSON = 'machine_info.json'

class TrackedImage:
    def __init__(self, default_image):
        self.count = 0
        self.image = default_image
        self.timestamp = time.time()


def get_pi_info():
    with open(MACHINE_INFO_JSON) as f:
        content = f.read()
    pi_info = json.loads(content)
    return pi_info

def get_principal_config():
    with open(MAIN_CONFIG_JSON) as f:
        return json.loads(f.read())

# Returns host, port, tls
def amigos_connection_info(url):
    if url.endswith('/'):
        url = url[:-len('/')]
    tls = False
    if url.startswith('http://'):
        port = 80
        url = url[len('http://'):]
    elif url.startswith('https://'):
        tls = True
        port = 443
        url = url[len('https://'):]
    if ':' in url:
        url, port = url.split(':', 1)
        port = int(port)
    return url, port, tls

def get_pi_name():
    pi_info = get_pi_info()
    pi_name = pi_info['name']
    return pi_name
    
def is_server():
    return os.path.exists('i_am_server') # get_pi_name() is None


def modules_in_path(directory, constraint=None):
    
    modules = []

    for path in glob.glob(os.path.join(directory, '*.py')):
        print(f'Machine: {path}')
        
        module_name = path[:-len('.py')].replace('/', '.')

        module = importlib.import_module(module_name)
        # importlib.reload(module)
        
        if constraint is not None and not constraint(module):
            continue

        modules.append(module)

    return modules


ARMED = False
DETONATE = True

# Manage all the running threads for one machine
class RunningProgram:
    def __init__(self, machine_info, program_type, program_name, program_cls):
        self.machine_info = machine_info
        self.program_type = program_type
        self.program_name = program_name
        self.program_cls = program_cls
        #self.destruct = ARMED
        self.program_inst = None
        self.t = None
        self.running = False
        self.error_text = None
    def self_destruct(self):
        if not self.running:
            return
        if self.program_inst is None:
            return
        t = time.time()
        self.program_inst.destruct = DETONATE
        self.t.join(1)
        taken = time.time() - t
        if self.t.is_alive() or taken > 0.5:
            diditnot = 'not ' if self.t.is_alive() else ''
            # Programs are supposed to be designed to exit quickly after self.destruct is detonated (set to True).
            print(f'ERROR PROBLEM: Program {self.program_name} failed to self destruct within a reasonable time, after {taken}s, the program did {diditnot}quit.')
        else:
            # Do not say it isn't running when it did not self destruct properly
            self.running = False
        print(time.time() - t)
    # def run_thread(self):
    #     self.program_inst.run()
    def run_thread(self, *args, **kwargs):
        #print(f'RUN THREAD FOR {self.program_name} BEGINS!')
        self.running = True
        try:
            #print(0)
            self.program_inst = self.program_cls(*args, **kwargs)
            #print(1)
            self.program_inst.program_type = self.program_type
            #print(2)
            self.program_inst.run()
            #print(3)
        except:
            # Get the error text and store it.
            # TODO: Use the store error text to send to server at somepoint, getting program status, etc...
            self.error_text = traceback.format_exc()
            print('RUN THREAD ERROR:')
            print(self.error_text)
        #self.program_inst = None
        self.running = False
    def run(self, *args, **kwargs):
        self.t = Thread(target=self.run_thread, args=args, kwargs=kwargs, daemon=True)
        self.t.start()

MACHINE_MOD_PATH = 'machines'

# Consider using multiprocess or something? (look into the ability to communicate with the parent process)
class ProgramRunner:

    def __init__(self, named_programs=None, server=False, api_callback_args=None):
        self.server = server
        self.named_programs = named_programs
        self.api_callback_args = api_callback_args
        if self.api_callback_args is None:
            self.api_callback_args = ()
        # key: machine_name
        self.running = {}

        if self.named_programs is None:

            self.reload_machine_modules()

    def reload_machine_modules(self):
        # TODO: Kill old programs that were running...

        self.named_programs = {}

        self.raw_machine_modules = modules_in_path(MACHINE_MOD_PATH, constraint=lambda module: hasattr(module, 'machine_type'))
        #self.machine_modules_dict = 

        self.machine_modules = {machine_module.machine_type: machine_module for machine_module in self.raw_machine_modules}
        for machine_type, machine_module in self.machine_modules.items():
            #self.machine_modules[machine_module.machine_type] = machine_module

            named_programs_for_machine = machine_module.server_programs if self.server else machine_module.pi_programs

            self.named_programs[machine_module.machine_type] = named_programs_for_machine

        # Add module hooks:
        for machine_type, machine_module in self.machine_modules.items():
            if self.server:
                machine_module.pi_manager_backend_api_callback(*self.api_callback_args)
            else:
                machine_module.pi_server_api_callback(*self.api_callback_args)
        # TODO: Now start all of the programs: (for both server and pi)

    def get_programs_options(self, machine_info):
        machine_config = machine_info.get('config',{})
        if self.server:
            return machine_config.get('server_programs', {})
        else:
            return machine_config.get('pi_programs', {})

    def stop_all(self, machine_info, program_type):
        machine_name = machine_info['name']

        if machine_name not in self.running:
            return
        running_for_machine = self.running[machine_name]

        i = 0
        while i < len(running_for_machine):
            running_program = running_for_machine[i]

            if running_program.program_type != program_type:
                i += 1
                continue

            running_for_machine.pop(i)
            running_program.self_destruct()

    def stop(self, machine_info, program_name):
        machine_name = machine_info['name']

        # program_type = get_program_type(program_name)
        if machine_name not in self.running:
            return
        running_for_machine = self.running[machine_name]

        for i, running_program in enumerate(running_for_machine):
            if running_program.program_name == program_name:
                break
        else:  # No break
            return

        # Stop this program
        running_for_machine.pop(i)
        running_program.self_destruct()

    def get_running(self, machine_info):
        machine_name = machine_info['name']
        if machine_name not in self.running:
            return []
        running_for_machine = self.running[machine_name]
        # TODO: Ensure that the non running programs eventually get removed from this list of running_for_machine...
        print('RUNNING STATS:', running_for_machine)
        return [{'name': running_program.program_name, 'type': running_program.program_type} for running_program in running_for_machine if running_program.running]

    def get_runnables(self, machine_type):
        programs_for_machine = self.named_programs[machine_type]
        # Just the key values:
        return list(programs_for_machine)

    def get_program_type(self, program_name):
        if ' ' not in program_name:
            return program_name
        parts = program_name.split(' ')
        end = parts.pop(-1)
        if end.isnumeric():
            return ' '.join(parts)
        return program_name
        #if not program_name[-1].isnumeric():
        #     return program_name
        # return ' '.join(program_name.split(' ')[:-1])
    
    def start(self, machine_info, program_type, args=None, kwargs=None):  # machine_info

        machine_name = machine_info['name']

        if machine_name not in self.running:
            self.running[machine_name] = []

        machine_type = machine_info['type']

        if machine_type not in self.machine_modules:
            print(f'Restart machine: No such machine type found: {machine_type}.')
            return None

        programs_for_machine = self.named_programs[machine_type]

        if program_type not in programs_for_machine:
            print(f'No such program {repr(program_name)} for machine {machine_name}...')
            return None

        program_cls = programs_for_machine[program_type]
        
        # if options is None:
        #     options = {}

        running_for_machine = self.running[machine_name]

        # Ensure program name is unique
        program_name = program_type
        i = 1
        while True:
            for running_program in running_for_machine:
                if running_program.program_name == program_name:
                    break
            else:  # No break
                break
            i += 1
            program_name = f'{program_type} {i}'

        running_program = RunningProgram(machine_info, program_type, program_name, program_cls)

        # Begin the program!
        if args is None:
            args = []
        if kwargs is None:
            kwargs = {}
        print(f'{program_type} is being run, look:')
        running_program.run(*args, **kwargs)

        running_for_machine.append(running_program)

        '''
        machine_module = machine_modules[machine_type]

        for program_name, program_cls in machine_module.pi_programs.items():

            # Exit the previous thread for this program and machine if it is running...

            pass
        '''

        # Start with key_name, then f'{key_name}_{i}' for i=2,3,...
        return program_name

