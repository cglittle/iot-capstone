import influxdb_client
from influxdb_client.client.write_api import ASYNCHRONOUS
import makerbotapi
import time
import asyncio

print('start')
makerbot_1 = makerbotapi.Makerbot('172.20.10.9', auto_connect=True)
makerbot_2 = makerbotapi.Makerbot('', auto_connect=True)  # add IP address of printer 2
print(1)
makerbot_1.authenticate_fcgi()
makerbot_2.authenticate_fcgi()
print(2)
makerbot_1.get_access_token('jsonrpc')
makerbot_2.get_access_token('jsonrpc')
print(3)
makerbot_1.authenticate_json_rpc()
makerbot_2.authenticate_json_rpc()
print(4)

# This would have to be changed for different influxdbs
bucket = "data"
org = "Capstone"
token = "z4RcZfk0S0jGhY4U5GBSziX5MtGUSBlni0y-mOJXwPMI5swpJBOqcpajC2m6z5EASLGpJttJSkYgDYAGu110sg=="
# Store the URL of your InfluxDB instance
url = "http://localhost:8086"

client = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
)
write_api = client.write_api(write_options=ASYNCHRONOUS)


async def printer_1_data():
    # function to get all the data from printer 1
    data = makerbot_1.get_system_information()
    current_process = data['result']['current_process']
    p = influxdb_client.Point("test_makerbot_send_2") \
        .tag("Name", data["result"]["machine_name"]) \
        .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"]) \
        .field("Percentage Done", current_process['progress']) \
        .field("Step", current_process['step'])
    if current_process['step'] == 'homing':
        await asyncio.sleep(60)
    if current_process['step'] != 'homing':
        await asyncio.sleep(5)
    return p


async def printer_2_data():
    # function to get all the data from printer 2
    data = makerbot_2.get_system_information()
    current_process = data['result']['current_process']
    p = influxdb_client.Point("test_makerbot_send_2") \
        .tag("Name", data["result"]["machine_name"]) \
        .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"]) \
        .field("Percentage Done", current_process['progress']) \
        .field("Step", current_process['step'])
    if current_process['step'] == 'homing':
        await asyncio.sleep(60)
    if current_process['step'] != 'homing':
        await asyncio.sleep(5)
    return p


while True:
    printer_task_lst = []
    async def main():
        printer_task_lst.append(asyncio.create_task(printer_1_data()))
        printer_task_lst.append(asyncio.create_task(printer_2_data()))
        for coro in asyncio.as_completed(printer_task_lst):
            data = await coro
            write_api.write(bucket=bucket, org=org, record=data)
    asyncio.run(main())




