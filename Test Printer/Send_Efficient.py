import makerbotapi
import asyncio
import influxdb_client
from influxdb_client.client.write_api import ASYNCHRONOUS
from threading import Thread
SEND_TO_SERVER = True


class AsyncLoopThread(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.loop = asyncio.new_event_loop()

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.loop.run_forever()
# Create a class for makerbot printers
class MakerbotPrinter:
    def __init__(self, ip, auth_code):
        self.ip = ip
        self.auth_code = auth_code
        self.makerbot = makerbotapi.Makerbot(ip, auto_connect=True)
        #self.makerbot.authenticate_fcgi()
        self.makerbot.auth_code = auth_code
        self.makerbot.get_access_token('jsonrpc')
        self.makerbot.authenticate_json_rpc()
        self.name = self.makerbot.get_system_information()["result"]["machine_name"]

    async def send_info(self):
        await makerbot_printer_send(self.makerbot)


# This would have to be changed for different influxdbs
bucket = "Test"
org = "Capstone2023"
token = "4aFaY-CgICA4eRIzKCUuiYX-_yShjxqn6QDegWWua7Qtxw_tKF_e_JdmcrsmrsHPePdaBV-0UK2xA7inQSN8xg=="
# Store the URL of your InfluxDB instance
url = "http://10.42.1.1:8086"
if SEND_TO_SERVER:
    client = influxdb_client.InfluxDBClient(
        url=url,
        token=token,
        org=org
    )
    write_api = client.write_api(write_options=ASYNCHRONOUS)

#"initial_heating = 1"
#"homing = 2"
#"final heating = 3"
#"printing = 4"
#"cleaning_up = 5"
#"completed = 6"
STEP_NUMBERS = {'idle': 0,
                'initial_heating': 1, 'homing': 2, 'final_heating': 3, 'printing': 4, 'cleaning_up': 5, 'completed': 6}

import traceback

async def makerbot_printer_send(name):
    # function to get all the data from printer 1
    try:
        data = name.get_system_information()
        print(data["result"]["machine_name"])
        if not SEND_TO_SERVER:
            return
        current_process = data['result']['current_process']
        p = influxdb_client.Point("test_makerbot_send_2") \
            .tag("Name", data["result"]["machine_name"]) \
            .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"])
        step_name = 'idle'
        perc_done = 0
        if current_process is not None:
            step_name = current_process['step']
            perc_done = current_process['progress']
        step_num = STEP_NUMBERS[step_name]
        p = p.field("Percentage Done", perc_done) \
            .field("Step", step_num)
        if current_process is None:    
            await asyncio.sleep(30)
        else:
            await asyncio.sleep(5)
        write_api.write(bucket=bucket, org=org, record=p)
        print('sent')
        print(p)
    except Exception as e:
        print(traceback.format_exc())



async def main():
    loop_handler = AsyncLoopThread()
    loop_handler.start()
    future_dict = {}
    printer_lst = [MakerbotPrinter('192.168.4.3', 'OZnEMWIUOwVYFZHpdXJcJPteACJxbWnH'),
                   MakerbotPrinter('192.168.4.4', 'vAzoexoBEKpMjWelEKkPOQzGhFPpqOtQ')]
    printer_lst_name = [printer.name for printer in printer_lst]
    for printer_name in printer_lst_name:
        future_dict[printer_name] = None
    while True:
        for printer, printer_name in zip(printer_lst, printer_lst_name):
            if future_dict[printer_name] is not None and not future_dict[printer_name].done():
                continue
            print('Add ' + printer_name + ' to the loop')
            future = asyncio.run_coroutine_threadsafe(printer.send_info(),
                                                      loop_handler.loop)
            future_dict[printer_name] = future

asyncio.run(main())
