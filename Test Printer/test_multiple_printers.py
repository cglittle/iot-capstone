import influxdb_client
from influxdb_client.client.write_api import ASYNCHRONOUS
import makerbotapi
import time

# Create a class for makerbot printers
class MakerbotPrinter:
    def __init__(self, ip, auth_code):
        self.ip = ip
        self.auth_code = auth_code
        self.makerbot = makerbotapi.Makerbot(ip, auto_connect=True)
        #self.makerbot.authenticate_fcgi()
        self.makerbot.auth_code = auth_code
        self.makerbot.get_access_token('jsonrpc')
        self.makerbot.authenticate_json_rpc()

    def send_info(self):
        makerbot_printer_send(self.makerbot)


# This would have to be changed for different influxdbs
bucket = "Test"
org = "Capstone2023"
token = "4aFaY-CgICA4eRIzKCUuiYX-_yShjxqn6QDegWWua7Qtxw_tKF_e_JdmcrsmrsHPePdaBV-0UK2xAinQSN8xg=="
# Store the URL of your InfluxDB instance
url = "http://10.42.0.1:8086"

client = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
)
write_api = client.write_api(write_options=ASYNCHRONOUS)



def makerbot_printer_send(name):
    # function to get all the data from printer 1
    data = name.get_system_information()
    current_process = data['result']['current_process']
    p = influxdb_client.Point("test_makerbot_send_2") \
        .tag("Name", data["result"]["machine_name"]) \
        .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"])
    if current_process is not None:
        p = p.field("Percentage Done", current_process['progress']) \
            .field("Step", current_process['step'])
    write_api.write(bucket=bucket, org=org, record=p)
    print(p)
    time.sleep(5)

#"initial_heating  = 1"
#"homeing = 2"
#"final_heating = 3"
#"printing = 4"
#"cleaning_up = 5"
#"completed = 6"

def main():
    printer_lst = [MakerbotPrinter('192.168.4.5', 'OZnEMWIUOwVYFZHpdXJcJPteACJxbWnH'),
                   MakerbotPrinter('192.168.4.6', 'vAzoexoBEKpMjWelEKkPOQzGhFPpqOtQ')]
    while True:
        # gather data every 5 seconds
        for printer in printer_lst:
            printer.send_info()

if __name__ == "__main__":
    main()
