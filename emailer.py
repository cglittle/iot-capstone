import smtplib, ssl
from email.message import EmailMessage

# NOTE: This does not currently work, other email_test.py script does, but
# it works differently, and that needs to be replaced...

import configparser

printer_info = configparser.ConfigParser()
printer_info.read('email.ini')

# Source for email script part: https://leimao.github.io/blog/Python-Send-Gmail/

port = 465  # For SSL
smtp_server = printer_info['default']['server']
sender_email = printer_info['default']['username']
receiver_email = printer_info['default']['receiver']
password = printer_info['default']['password']

msg = EmailMessage()
msg.set_content("Hello!")
msg['Subject'] = 'This is a subject, subjected to email.'
msg['From'] = sender_email
msg['To'] = receiver_email

context = ssl.create_default_context()
with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
    server.login(sender_email, password)
    server.send_message(msg, from_addr=sender_email, to_addrs=receiver_email)

