'''
        Read Gyro and Accelerometer by Interfacing Raspberry Pi with MPU6050 using Python
	http://www.electronicwings.com
'''
import smbus					#import SMBus module of I2C
from time import sleep    
import time
from threading import Thread, Semaphore
import configparser
import os
import os.path
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime


print_sem = Semaphore(1)

old_print = print
def print(*args, **kw):
    print_sem.acquire()
    old_print(*args, **kw)
    print_sem.release()


CONFIG_FILE = 'iot4config.ini'

config = configparser.ConfigParser()
if os.path.exists(CONFIG_FILE):
    config.read(CONFIG_FILE)

printer_info = configparser.ConfigParser()
printer_info.read('printer_info.ini')
printer_name = printer_info['default']['printer_name']
print(printer_name)


USE_GUI = False

influxdb_instances = {}
#influxdb_infos = []
#influxdb_options
print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    instance = config[name]
    influxdb_instances[name] = instance
    use_info = instance
    bucket = use_info['bucket']
    org = use_info['org']
    token = use_info['token']
    url = use_info['url']
    print('InfluxDB:', name)
    print('Bucket:', bucket)
    print('Org:', org)
    print('Token:', token)
    print('URL:', url)



if not influxdb_instances:
    raise Exception('No influxdb database listed!')
    
    
message_queue = []
def write_influx(record):
    
    #current_time = datetime.utcnow().timestamp() * 1000
    #current_time = int(current_time)
    current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    #.strftime('%Y-%m-%dT%H:%M:%SZ')
    #current_time = current_time.encode('utf8')
    print(repr(current_time))
    #record = record.field('time', current_time)
    record = record.time(current_time)
    print(record)
    message_queue.append(record)
    
    
def influxdb_writer():
    while True:
        while not message_queue:
            time.sleep(0.01)
        record = message_queue.pop(0)           
        for name, use_info in influxdb_instances.items():

            enabled = use_info['enabled']

            print(name, 'enabled:', repr(enabled))

            if not enabled:
                continue

            bucket = use_info['bucket']
            org = use_info['org']
            token = use_info['token']
            url = use_info['url']



            print('ACQUIRE:', record)
            #influx_sem.acquire()
            try:
                client = influxdb_client.InfluxDBClient(
                    url=url,
                    token=token,
                    org=org
                )
                write_api = client.write_api(write_options=SYNCHRONOUS)
                write_api.write(bucket=bucket, record=record)
            except:
                print(traceback.format_exc())
                message_queue.insert(0, record)
                print(message_queue)
                time.sleep(5)
                
            finally:
                print('RELEASE')
                #influx_sem.release()


t = Thread(target=influxdb_writer, args=(), daemon=True)
t.start()

#some MPU6050 Registers and their Address
PWR_MGMT_1   = 0x6B
SMPLRT_DIV   = 0x19
CONFIG       = 0x1A
GYRO_CONFIG  = 0x1B
INT_ENABLE   = 0x38
ACCEL_XOUT_H = 0x3B
ACCEL_YOUT_H = 0x3D
ACCEL_ZOUT_H = 0x3F
GYRO_XOUT_H  = 0x43
GYRO_YOUT_H  = 0x45
GYRO_ZOUT_H  = 0x47


def MPU_Init():
	#write to sample rate register
	bus.write_byte_data(Device_Address, SMPLRT_DIV, 7)
	
	#Write to power management register
	bus.write_byte_data(Device_Address, PWR_MGMT_1, 1)
	
	#Write to Configuration register
	bus.write_byte_data(Device_Address, CONFIG, 0)
	
	#Write to Gyro configuration register
	bus.write_byte_data(Device_Address, GYRO_CONFIG, 24)
	
	#Write to interrupt enable register
	bus.write_byte_data(Device_Address, INT_ENABLE, 1)

def read_raw_data(addr):
	#Accelero and Gyro value are 16-bit
        high = bus.read_byte_data(Device_Address, addr)
        low = bus.read_byte_data(Device_Address, addr+1)
    
        #concatenate higher and lower value
        value = ((high << 8) | low)
        
        #to get signed value from mpu6050
        if(value > 32768):
                value = value - 65536
        return value


bus = smbus.SMBus(1) 	# or bus = smbus.SMBus(0) for older version boards
Device_Address = 0x68   # MPU6050 device address

MPU_Init()

print (" Reading Data of Gyroscope and Accelerometer")

while True:
	
    #Read Accelerometer raw value
    acc_x = read_raw_data(ACCEL_XOUT_H)
    acc_y = read_raw_data(ACCEL_YOUT_H)
    acc_z = read_raw_data(ACCEL_ZOUT_H)

    #Read Gyroscope raw value
    gyro_x = read_raw_data(GYRO_XOUT_H)
    gyro_y = read_raw_data(GYRO_YOUT_H)
    gyro_z = read_raw_data(GYRO_ZOUT_H)

    #Full scale range +/- 250 degree/C as per sensitivity scale factor
    Ax = acc_x/16384
    Ay = acc_y/16384
    Az = acc_z/16384
    A = (Ax**2 + Ay**2 + Az**2)**0.5
    
    Gx = gyro_x/131
    Gy = gyro_y/131
    Gz = gyro_z/131

    p = influxdb_client.Point("send efficient 2") \
            .tag("Name", printer_name) \
            .field("Ax", Ax) \
            .field("Ay", Ay) \
            .field("Az", Az) \
            .field("A", A)
    write_influx(p)
    


    print ("Gx=%.2f" %Gx, u'\u00b0'+ "/s", "\tGy=%.2f" %Gy, u'\u00b0'+ "/s", "\tGz=%.2f" %Gz, u'\u00b0'+ "/s", "\tAx=%.2f g" %Ax, "\tAy=%.2f g" %Ay, "\tAz=%.2f g" %Az) 	
    time.sleep(1)
	
