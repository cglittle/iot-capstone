import socket
import ssl
import json
import os
import traceback
import time
import secrets
import base64
import hashlib
import random
import string
import struct

opcodes = {'cont': 0x0, 'text': 0x1, 'binary': 0x2, 'ping': 0x9, 'pong': 0xa}

def construct_frame_header(kind='text', final=False, length=0, use_mask=True):
    frame_mask = b''
    if use_mask:
        # Construct one
        frame_mask = secrets.token_bytes(4)
    use_mask = int(use_mask)
    final = int(final)
    opcode = opcodes[kind]
    B1 = (final << 7) | opcode
    frame_extra_length = b''
    small_length = length
    if length > 2**63-1:
        raise Exception(f'Frames cannot have a payload this big, max is a 64 bit unsigned integer, and this number is: {length}')
    elif length > 2**16-1:
        frame_extra_length= struct.pack('!Q', length)
        small_length = 127
    elif length > 125:
        frame_extra_length = struct.pack('!H', length)
        small_length = 126
    B2 = (use_mask << 7) | small_length
    print('Frame constructed:')
    print(bin(B1))
    print(bin(B2))
    print()
    frame_header = struct.pack('!BB', B1, B2) + frame_extra_length + frame_mask
    return frame_header, frame_mask

def get_frame_header(data, ):
    header = None
    if len(data) < 2:
        return header, data
    B1, B2, rest_of_data = data[0], data[1], data[2:]
    finish = B1 >> 7
    opcode = B1 & 0b00001111
    use_mask = B2 >> 7
    small_length = B2 & 0b01111111
    if small_length == 126:
        if len(rest_of_data) < 2:
            return header, data
        length_data, rest_of_data = rest_of_data[:2], rest_of_data[2:]
        length = struct.unpack('!H', length_data)[0]
        length -= 2
    elif small_length == 127:
        if len(rest_of_data) < 8:
            return header, data
        length_data, rest_of_data = rest_of_data[:8], rest_of_data[8:]
        length = struct.unpack('!Q', length_data)[0]
        length -= 8
    else:
        length = small_length

    mask = None
    if use_mask:
        if len(rest_of_data) < 4:
            return header, data
        mask, rest_of_data = rest_of_data[:4], rest_of_data[4:]
        length -= 4

    header = {'length': length, 'finish': finish, 'opcode': opcode, 'mask': mask}

    frame_head = data[-len(rest_of_data):]
    #print('GOT HEADER:', frame_head)
    #print(header)

    return header, rest_of_data

# About 3GB:
FRAME_LENGTH = (2**34 // 5)  # 2**63-1

DEBUG_AMIGOS = False  # True

class AmigosConnectionClosed(Exception):
    pass


MB = 1024**2

class AmigosRPC:
    
    def __init__(self, server=False, sock = None, server_address = None, tls = False, host = None, port = None):
        # Create a TCP/IP socket
        self.server = server
        self.sock = sock
        self.original_sock = self.sock
        self.data = b''
        if sock is None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            if not server:
                host = server_address[0]
                self.sock.connect(server_address)
                if tls:
                    print(f'Connecting to {server_address} with TLS...')
                    context = ssl.create_default_context()
                    self.sock = context.wrap_socket(self.sock, server_hostname=host)
                self.pretend_request_websocket(host)
            else:
                self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.sock.bind((host, port))
                self.sock.listen(10)

    def _recieve_http_header(self):
        while b'\r\n\r\n' not in self.data:
            chunk = self.sock.recv(4096)
            if not chunk:
                raise BrokenPipeError('HTTP recv failed!')
            self.data += chunk
            #print(chunk)
        head, self.data = self.data.split(b'\r\n\r\n', 1)
        #print('HEAD RESPONSE:')
        #print(head)
        return head

    def _recieve_websock_header(self):
        header = None
        while True:
            header, self.data = get_frame_header(self.data)
            if header is not None:
                break
            chunk = self.sock.recv(1024)
            if not chunk:
                raise BrokenPipeError('HTTP recv failed!')
            self.data += chunk

    # These two functions allow arbitrary protocols over cloudflare by createing a websocket, which cloudflare does not proceed to interfere with. (We do not actually follow any websocket protocol specs)
    def pretend_request_websocket(self, host):
        client_websock_key = base64.b64encode(secrets.token_bytes(16)).decode('utf8')
        #client_websock_key = 'dGhlIHNhbXBsZSBub25jZQ=='  # This should be random according to wikipedia: https://en.wikipedia.org/wiki/WebSocket
        more_random = ''.join(random.choice(string.ascii_lowercase) for i in range(16))
        send_this = f'GET /{more_random}/talk HTTP/1.1\r\nHost: {host}\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Key: {client_websock_key}\r\nOrigin: https://{host}\r\nSec-WebSocket-Protocol: chat, superchat\r\nSec-WebSocket-Version: 13\r\n\r\n'
        #print('SENDING INITIAL:')
        #print(send_this)
        send_this = send_this.encode('utf8')
        self.sock.send(send_this)
        # Pretent this is pretty much an infinitely long frame (amigos rpc)
        head = self._recieve_http_header()

        frame_header, mask = construct_frame_header(kind='binary', final=True, length=FRAME_LENGTH)
        self.sock.send(frame_header)
        self._recieve_websock_header()

    def pretend_websocket_upgrade(self):
        head = self._recieve_http_header().decode('utf8')
        lines = head.split('\r\n')
        for line in lines[1:]:
            name, value = line.split(':', 1)
            name = name.strip().lower()
            value = value.strip()
            if name == 'sec-websocket-key':
                break
        else:  # No break
            print('Bad response:')
            print(head)
            raise Exception('Bad response! Did not find websocket security key!')
        x = value.encode('utf8') + b'258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
        y = hashlib.sha1(x).digest()
        server_websocket_key = base64.b64encode(y).decode('utf8')
        # server_websocket_key = 's3pPLMBiTxaQ9kYGzzhZRbK+xOo='
        send_this = f'HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: {server_websocket_key}\r\nSec-WebSocket-Protocol: chat\r\n\r\n'
        send_this = send_this.encode('utf8')
        self.sock.send(send_this)

        self._recieve_websock_header()
        frame_header, mask = construct_frame_header(kind='binary', final=True, length=FRAME_LENGTH)
        self.sock.send(frame_header)
    
    def accept(self):
        client, client_addr = self.sock.accept()
        amigos_client = AmigosRPC(sock = client)
        amigos_client.pretend_websocket_upgrade()
        return client_addr, amigos_client

    def check_open(self):

        # https://stackoverflow.com/questions/23383615/switching-between-blocking-and-nonblocking-on-a-python-socket
        # "... You do this [setblocking(0)] after creating the socket, but before using it. (Actually, if you're nuts, you can switch back and forth.)"

        self.sock.setblocking(False)

        still_open = True

        try:
            chunk = self.sock.recv(4*MB)
            if not chunk:
                still_open = False
            else:
                self.data += chunk
        except BlockingIOError:
            pass

        self.sock.setblocking(True)

        return still_open

    def check_open_and_command(self):
        self.sock.setblocking(False)

        still_open = True
        have_command = False

        while not have_command:
            try:
                chunk = self.sock.recv(4*MB)
                if not chunk:
                    still_open = False
                    break
                else:
                    self.data += chunk
            except BlockingIOError:
                break
            # If we have a command, break:
            have_command = b'\n' in self.data

        if still_open:
            pass  # TODO: Figure out what this is.!

        self.sock.setblocking(True)

        return still_open, have_command
        
    def _recieve(self, newline = True, maxlen = None):
        while True:
            # Check before getting a new chunk since the whole new line may be sitting here when this function is called.
            if newline:
                if b'\n' in self.data:
                    message, self.data = self.data.split(b'\n',1)
                    return message
            if not ((maxlen is None) or (len(self.data) < maxlen)):
                break
            chunk = self.sock.recv(4*MB)
            if not chunk:
                # Connection error!
                raise BrokenPipeError('Recv failed!')
            self.data += chunk
        message = self.data[:maxlen]
        self.data = self.data[maxlen:]
        return message
        
    def _recieve_stream(self, yet2get):
        while len(self.data) < yet2get:
            if self.data:
                yield self.data
                yet2get -= len(self.data)
                self.data = b''
            chunk = self.sock.recv(4*MB)
            if not chunk:
                # Connection has been closed...
                #raise AmigosConnectionClosed('During receive stream.')
                raise BrokenPipeError('During receive stream.')
            self.data += chunk
        if self.data:
            yield self.data[:yet2get]
            self.data = self.data[yet2get:]

    
    def send(self, command):
        clean_command = {'data': command.get('data'), 'amigos_version': 1, 'method': command['method']}
        if 'url' in command:
            clean_command['url'] = command['url']
        if 'host' in command:
            clean_command['host'] = command['host']
        get_file = 'file_name' in command
        if get_file:
            file_content = command.pop('file_content', None)
            file_length = None
            if file_content is not None:
                file_length = len(file_content)
            file_stream = command.pop('file_stream', None)
            if file_stream is not None:
                # file_stream.seek(0, os.SEEK_END)
                # file_length = file_stream.tell()
                file_length = os.fstat(file_stream.fileno()).st_size
            clean_command['file_name'] = command['file_name']
            clean_command['file_length'] = file_length
            #clean_c
        if DEBUG_AMIGOS:
            print('AMIGOS 1!')
        self.sock.send(json.dumps(clean_command).encode('utf8')+ b'\n')
        if DEBUG_AMIGOS:
            print('AMIGOS 2!')
        if get_file:
            if DEBUG_AMIGOS:
                print('AMIGOS 3!', len(file_content) if file_content is not None else 0)
            if file_content is not None:
                # Do something
                if not DEBUG_AMIGOS:
                    self.sock.send(file_content)
                else:
                    to_send = file_content
                    CHUNK = 1024
                    sent_bytes = 0
                    while to_send:
                        print('AMIGOS 3.5 sent:', sent_bytes)
                        chunk, to_send = to_send[:CHUNK], to_send[CHUNK:]
                        self.sock.send(chunk)
                        sent_bytes += len(chunk)
                        time.sleep(0.001)
            if DEBUG_AMIGOS:
                print('AMIGOS 4!')
            if file_stream is not None:
                if DEBUG_AMIGOS:
                    print('AMIGOS 5!')
                while True:
                    if DEBUG_AMIGOS:
                        print('AMIGOS 6!')
                    chunk = file_stream.read(4*MB)
                    if not chunk:
                        break
                    # Do something
                    if DEBUG_AMIGOS:
                        print('AMIGOS 7!')
                    self.sock.send(chunk)
                    if DEBUG_AMIGOS:
                        print('AMIGOS 8!')
                file_stream.close()
        if DEBUG_AMIGOS:
            print('AMIGOS 9!')
    
    def recieve(self):
        stuff_recv = self._recieve().decode('utf8')
        try:
            command = json.loads(stuff_recv)
        except Exception as e:
            print('GOT THIS:')
            print(stuff_recv)
            raise e
        if 'file_name' in command:
            file_length = command['file_length']
            stream = self._recieve_stream(file_length)
            command['file_stream'] = stream
            
        return command
        
        
class ThreeAmigosAPI:
    def __init__(self):
        self.get_function_mapping = {}
        self.post_function_mapping = {}
        
    def get(self, url):
        def actual_decorator(func):
            self.get_function_mapping[url] = func
            return func
        return actual_decorator

    def post(self, url):
        def actual_decorator(func):
            self.post_function_mapping[url] = func
            return func
        return actual_decorator
        
    def run(self, host, port, tls=False):
        server_address = (host, port)
        amigos = None
        while True:
            print('Connecting Amigos RPC...')
            try: 
                amigos = AmigosRPC(server=False, server_address=server_address, tls=tls)
                print('Connected Amigos RPC!')
                while True:
                    command = amigos.recieve()
                    if command['method'] == 'get':
                        response = self.get_function_mapping[command['url']](command)
                        amigos.send({'data': response, 'amigos_version': 1, 'method': 'response'})
                    elif command['method'] == 'post':
                        response = self.post_function_mapping[command['url']](command)
                        amigos.send({'data': response, 'amigos_version': 1, 'method': 'response'})
                    elif command['method'] == 'keepalive':
                        amigos.send({'data': None, 'amigos_version': 1, 'method': 'response'})
           
            except Exception as e:
                print(traceback.format_exc())
            finally:
                print('Closing socket')
                if amigos is not None:
                    try:
                        amigos.sock.close()
                    except Exception as e:
                        print('While trying to close socket:')
                        print(traceback.format_exc())
            time.sleep(10)
            
        
class ThreeAmigosAPIServer:
    def __init__(self, host, port):
        self.amigos_server = AmigosRPC(server = True, host = host, port = port)
    
    def run(self, callback):
        while True:
            client_addr, amigos_client = self.amigos_server.accept()
            callback(client_addr, amigos_client)
        
