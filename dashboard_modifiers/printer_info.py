
# Files in the same directory are loaded and called to modify the home dashboard in customisable ways

# TODO: Replace dash_name referenced here elsewhere in the code...
# dash_name = 'Test_Homepage'
general_type = "printer_info"
# apply_to = [dash_name]

# TODO: Ensure there are many useful helper functions available

# {panel type: {panel title: rule}}
# More generally, use a match function?
# rule format: ...
#def printer_steps_rule(md, mm, targets, machines):
def printer_steps_rule(md, mm, panel, machines):
    #targets = panel['targets']
    #print('IN THE RULE!')
    #input('Press enter to continue...')
    new_targets = []
    for machine in machines:
        machine_name = machine['name']
        dash_name = machine['dash_name']
        # NOTE: The query buck, measuremnt name, etc. all get replaced below:
        query = 'from(bucket: "Test")\n  |> range(start: -24h)\n  |> filter(fn: (r) =>\n    r._measurement == "send efficient 2" and\n    r._field == "Step" and\n    r.Name == "Printer 001"\n  )'
        query = md.query_machine_transplant(query, machine_name)
        target = md.influxdb_target(query, ref_id=dash_name)
        new_targets.append(target)
    panel['targets'] = new_targets
    #return new_targets

# panel['fieldConfig']['defaults']['displayName'] is required for some, as not all use 'title'
# This means we need functions to call like match_title, etc... Though, they need to be imported
# or something... Importing seems like a lot of work. Then we need a callback function here
# where it is constructed... Or just do it in the below function!

# mm is machine manager
# md is manachine dashboards
def modify_home_dashboard(dashboard, md, mm, general_info):

    #print('Making modifications...')
    #input('Press enter to continue...')

    general_config = general_info.get('config', {})

    the_tag = general_config.get('list_tag', 'printer')
    the_folder = general_config.get('list_folder', 'Machine Info')

    machines = mm.filter_machines_by_tag(the_tag)

    panel_replace_rules = [(['type/stat', 'title/Current Status'], printer_steps_rule),
                           (['type/stat', 'title/'], md.sum_for_machines_rule),
                           (['type/state-timeline', 'title/Status Timeline'], printer_steps_rule),
                           (['type/dashlist'], md.dashboard_list_rule, {'folder': the_folder}),
                           ]

    md.panel_replacements(mm, dashboard, panel_replace_rules, machines, general_info)

    '''
    for panel in dashboard['panels']:
        if panel['options'] == None:
            pass
    '''

    #return dasboard

