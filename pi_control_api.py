# NOTE: This file is deprecated

import requests
import socket
from threading import Thread, Semaphore
import traceback

port = 10000
class PiAPI:
    def __init__(self):
        #self.session = requests.Session()
        pass
        
    def run_program(self, program, ip):
        self.server = f'http://{ip}:{port}'
        r = requests.post(f'{self.server}/run/program', json={'command': program})
    def kill_program(self, program, ip):
        self.server = f'http://{ip}:{port}'
        try:
            r = requests.post(f'{self.server}/kill/program', json={'command': program})
        except requests.exceptions.ConnectionError:
            pass  # print(traceback.format_exc())
            
    def upload_program(self, fname, ip):
        self.server = f'http://{ip}:{port}'
        files = {'file': open(fname, 'rb')}
        r=requests.post(f'{self.server}/upload/program', files = files)

    def admin(self, program, ip):
        self.server = f'http://{ip}:{port}'
        r = requests.post(f'{self.server}/admin', json={'command': program})
        
    def add_machine(self, machine_info, ip):
        self.server = f'http://{ip}:{port}'
        r = requests.post(f'{self.server}/machines/add', json=machine_info)

    def edit_machine(self, prev_machine_name, machine_info, ip):
        self.server = f'http://{ip}:{port}'
        r = requests.post(f'{self.server}/machines/edit', json={'prev_name': prev_machine_name, 'info': machine_info})

    def delete_machine(self, machine_name, ip):
        self.server = f'http://{ip}:{port}'
        r = requests.post(f'{self.server}/machines/delete', json={'name': machine_name})







if __name__ == "__main__":
    ip = '192.168.0.66'
    #t = Thread(target = connect_to_server, args = (), daemon = True)
    #t.start()
    rpi_api = PiAPI()
    x = input('What would you like to do? ')
    rpi_api.admin(x, ip)
