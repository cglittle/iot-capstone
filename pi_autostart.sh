nohup screen -S livestream -dm python3 livestream.py &
nohup screen -S c_d_new -dm python3 c_d_new.py &
nohup screen -S pi_data_collection -dm python3 pi_data_collection.py &
sudo systemctl restart pi_client.service

#To exit screen without killing type ctrl + a  d 
#To scroll ctrl + a  esc to stop scrolling esc
#To view a screen screen -r name
#To kill a specific screem screen -X -S <session_name> quit
#To kill all screen pkill screen
 
