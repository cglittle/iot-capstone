'''
https://github.com/gradio-app/gradio/issues/1637
https://github.com/VolkovLabs/volkovlabs-image-panel/issues/6
'''


#import makerbotapi
import os.path
import configparser
import asyncio
import traceback
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
import newapi
from threading import Thread, Semaphore
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime



print_sem = Semaphore(1)

old_print = print
def print(*args, **kw):
    print_sem.acquire()
    old_print(*args, **kw)
    print_sem.release()


# import pymjpeg
# pymjpeg:
# https://github.com/damiencorpataux/pymjpeg
import os, time

pymjpeg_boundary = '--boundarydonotcross'

def pymjpeg_request_headers():
    return {
        'Cache-Control': 'no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0',
        'Connection': 'close',
        'Content-Type': 'multipart/x-mixed-replace;boundary=%s' % pymjpeg_boundary,
        'Expires': 'Mon, 1 Jan 2030 00:00:00 GMT',
        'Pragma': 'no-cache',
'Access-Control-Allow-Origin': '*' # CORS
    }

def pymjpeg_image_headers(filename):
    return {
        'X-Timestamp': time.time(),
        'Content-Length': os.path.getsize(filename),
        'Content-Type': 'image/jpeg',
    }
    
def pymjpeg_image_data_headers(image_data):
    return {
        'X-Timestamp': time.time(),
        'Content-Length': len(image_data),
        'Content-Type': 'image/jpeg',
    }

# FIXME: should take a binary stream
def pymjpeg_image(filename):
    with open(filename, "rb") as f:
        # for byte in f.read(1) while/if byte ?
        byte = f.read(1)
        while byte:
            yield byte
            # Next byte
            byte = f.read(1)

def pymjpeg_data_image(image_data):
    
    with open(filename, 'rb') as f:
        # for byte in f.read(1) while/if byte ?
        byte = f.read(1)
        while byte:
            yield byte
            # Next byte
            byte = f.read(1)




# https://stackoverflow.com/questions/46210672/python-2-7-streaming-http-server-supporting-multiple-connections-on-one-port
import time, threading, socket
from http.server import BaseHTTPRequestHandler, HTTPServer
import logging

class Handler(BaseHTTPRequestHandler):

    def do_GET(self):

        # https://localhost:8001/Printer 001  'Printer 001'
        printer_name = self.path[len('/'):].replace('%20', ' ')

        print('Printer name:', repr(printer_name))
        print('Latest images:', list(latest_images))
        
        if printer_name not in latest_images:
            self.send_error(404, 'No such printer!')
            return

        logging.debug('GET response code: 200')
        self.send_response(200)
        # Response headers (multipart)
        for k, v in pymjpeg_request_headers().items():
            self.send_header(k, v)
            logging.debug('GET response header: ' + k + '=' + v)
        # Multipart content
        next_image_num = 0
        try:
            while True:
                # filename
                image_data, image_num = latest_images[printer_name]
                if image_num < next_image_num:
                    time.sleep(0.05)
                    continue
                next_image_num = image_num + 1
            
                logging.debug('GET response image: ' + str(image_num))
                # Part boundary string
                self.end_headers()
                self.wfile.write(bytes(pymjpeg_boundary, 'utf-8'))
                self.end_headers()
                # Part headers
                for k, v in pymjpeg_image_data_headers(image_data).items():
                    self.send_header(k, v)
                    # logging.debug('GET response header: ' + k + '=' + v)
                self.end_headers()
                # Part binary
                # logging.debug('GET response image: ' + filename)
                self.wfile.write(image_data)
                '''
                for chunk in pymjpeg_data_image(image_data):
                    self.wfile.write(chunk)
                '''
        except BrokenPipeError:
            pass
            
# Create ONE socket.
addr = ('', 8001)
sock = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(addr)
sock.listen(5)

latest_images = {}

# Launch 100 listener threads.
class Thread(threading.Thread):
    def __init__(self, i):
        threading.Thread.__init__(self)
        self.i = i
        self.daemon = True
        self.start()
    def run(self):
        httpd = HTTPServer(addr, Handler, False)

        # Prevent the HTTP server from re-binding every handler.
        # https://stackoverflow.com/questions/46210672/
        httpd.socket = sock
        httpd.server_bind = self.server_close = lambda self: None

        httpd.serve_forever()

print('Starting threads for image server...')
[Thread(i) for i in range(100)]
print('Image server running!')



# Control if we are sending data to the server
SEND_TO_SERVER = True

from threading import Thread

class AsyncLoopThread(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.loop = asyncio.new_event_loop()

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.loop.run_forever()


# Create a class for makerbot printers
class MakerbotPrinter:
    def __init__(self, ip, auth_code):
        self.ip = ip
        self.auth_code = auth_code
        self.ready = False

    def connect(self):
        self.makerbot = newapi.MakerbotAPI(self.ip, self.auth_code)
        # self.makerbot.auth_code = auth_code
        print('Connecting...', self.ip)
        self.makerbot.connect()
        # self.makerbot.get_access_token('jsonrpc')
        print('Authenticating...')
        self.makerbot.authenticate_json_rpc()
        print('Getting system info...')
        self.name = self.makerbot.get_system_information()["result"]["machine_name"]
        self.ready = True

    # send the info of the Makerbot printer
    def send_info(self, current_printers):
        try:
            print('About to connect!')
            self.connect()
            print('Loop.')
            makerbot_printer_send(self.name, self.makerbot, current_printers)
        except Exception as e:
            print(traceback.format_exc())
    def send_camera_stream(self):
        image_num = 0
        while not self.ready:
            pass
        while True:
            # Get latest image
            _, width, height, _, yuv_image = self.makerbot.get_camera_image()
            latest_images[self.name] = (yuv_image, image_num)
            image_num += 1
            #time.sleep(0.5)



'''
def influxdb_info(bucket, org, token, url):
    return {'bucket': bucket,
            'org': org,
            'token': token,
            'url': url}
'''

#USE_CONFIG_FILE = False

CONFIG_FILE = 'iot4config.ini'

config = configparser.ConfigParser()
if os.path.exists(CONFIG_FILE):
    config.read(CONFIG_FILE)
'''
if 'local' not in config:
    pass
'''
influxdb_instances = {}
#influxdb_infos = []
#influxdb_options
print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    influxdb_instances[name] = config[name]
    '''
    if 'local' in config:
        influxdb_infos['local'] = config['local']
    if 'cloud' in config:
        influxdb_infos['cloud'] = config['cloud']
    '''

if not influxdb_instances:
    raise Exception('No influxdb database listed!')

#influxdb_instances = {}

# Always send to local influxdb.


for name, use_info in influxdb_instances.items():
    bucket = use_info['bucket']
    org = use_info['org']
    token = use_info['token']
    url = use_info['url']
    print()
    print('InfluxDB info:')
    print(repr(name))
    print('Using Bucket:', repr(bucket))
    print('Using Org:', repr(org))
    print('Using Token:', repr(token))
    print('Using url:', repr(url))
    print()


authentication_dict = {"Printer 001" : 'OZnEMWIUOwVYFZHpdXJcJPteACJxbWnH',
                       "Printer 002" : 'vAzoexoBEKpMjWelEKkPOQzGhFPpqOtQ'}

# Step Encodings
STEP_NUMBERS = {'idle': 0,
                'clear_build_plate': 7,
                'initial_heating': 1,
                'homing': 2,
                'transfer': 10,
                'final_heating': 3,
                'printing': 4,
                'cleaning_up': 5,
                'completed': 6,
                'cooling': 8,
                'cancelling': 9,
                'verify_firmware': 11,
                'writing': 12
                }
# extrusion, 


influx_sem = Semaphore(1)
    
def makerbot_printer_send(name, machine, current_printers):
    try:
        #image_num = 0
        prev_step = 0
        while True:
            print('Getting system info...')
            # Retrieve data from system
            data = machine.get_system_information()
            
            print("Got system info")
            print(data["result"])#["machine_name"])
            if not SEND_TO_SERVER:
                time.sleep(30)
                continue
            current_process = data['result']['current_process']
            # Send the name of the device and the extruder temperature to influxdb
            p = influxdb_client.Point("send efficient 2") \
                .tag("Name", data["result"]["machine_name"]) \
                .field("Target Temperature", data["result"]["toolheads"]["extruder"][0]["target_temperature"]) \
                .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"])

            # Set the step to be idle and percentage done to be 0 as default

            step_name = 'idle'
            perc_done = 0

            # if the printer is printing get the step and the percentage done of the job
            if current_process is not None:
                step_name = current_process['step']
                perc_done = current_process.get('progress', 0)
            step_num = STEP_NUMBERS[step_name]
            p = p.field("Percentage Done", perc_done) \
                .field("Step", step_num)
            
            # Get latest image
            #_, width, height, _, yuv_image = machine.get_camera_image()
            #latest_images[name] = (yuv_image, image_num)
            #image_num += 1

            did_send_event = False
                       
            if step_num == STEP_NUMBERS['idle']:
                tmp = 0
            if step_num == STEP_NUMBERS['printing'] and prev_step != step_num:
                p = p.field("Print Event", 1)
                did_send_event = True
            
            # change STEP_NUMBERS TO "completed" 	
            if step_num == STEP_NUMBERS["completed"] and prev_step != step_num: 
                # machine.save_camera_jpg(f"test_{name}.jpg")
                p = p.field("Print Event", 3)
                did_send_event = True
            
            if step_num == STEP_NUMBERS['idle'] and prev_step == 5:
                p = p.field('Print Event', 2)
                did_send_event = True
           
            if not did_send_event:
                p = p.field('Print Event', 0)


            # if the printer is not printing send data every 30 seconds
            if current_process is None:
                #await asyncio.sleep(30)
                time.sleep(30)

            # if the printer is printing send data every 5 seconds
            else:
                #await asyncio.sleep(5)
                time.sleep(5)

            print('Waiting to send to influxdb...')
            # write the data into influxdb

            write_influx(p)
            '''
            print('ACQUIRE 3')
            influx_sem.acquire()
            print('Sending to influxdb...')
            try:
                client = influxdb_client.InfluxDBClient(
        url=url,
        token=token,
        org=org
    )
                write_api = client.write_api(write_options=SYNCHRONOUS)
                write_api.write(bucket=bucket, record=p)
            finally:
                
                #with InfluxDBClientSync(url=url, token=token, org=org) as client:
                #    client.write_api().write(bucket=bucket, record=p)
                print('RELEASE 3')
                influx_sem.release()
            '''
            print('sent')
            print(p)
            prev_step = step_num

    except Exception as e:
        print(traceback.format_exc())
    try:
        print(name, 'is being turned off!')
        del current_printers[name]
        print(name, "has turned off!")
        machine.finish()
        print(name, "is finished!")
        p = p.field("Step", -1)
        write_influx(p)
        '''
        print('ACQUIRE 4')
        influx_sem.acquire()
        try:
            client = influxdb_client.InfluxDBClient(
        url=url,
        token=token,
        org=org
    )
            client.write_api(write_options=SYNCHRONOUS).write(bucket=bucket, record=p)
        finally:
        
            #with InfluxDBClientSync(url=url, token=token, org=org) as client:
            #    client.write_api().write(bucket=bucket, record=p)
            print('RELEASE 4')
            influx_sem.release()
        '''
    except Exception as e:
        print(traceback.format_exc())
            
message_queue = []
def write_influx(record):
    
    #current_time = datetime.utcnow().timestamp() * 1000
    #current_time = int(current_time)
    current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    #.strftime('%Y-%m-%dT%H:%M:%SZ')
    #current_time = current_time.encode('utf8')
    print(repr(current_time))
    print(record)
    #record = record.field('time', current_time)
    record = record.time(current_time)
    print(record)
    message_queue.append((record,[]))
    
    
def influxdb_writer():
    while True:
        while not message_queue:
            time.sleep(0.01)
        message = message_queue.pop(0)
        record = message[0]
        submitted_to = message[1]           
        for name, use_info in influxdb_instances.items():
            successfully_submitted = True
            if name in submitted_to:
                continue
                
            enabled = use_info['enabled']

            print(name, 'enabled:', repr(enabled))

            if not enabled:
                continue

            bucket = use_info['bucket']
            org = use_info['org']
            token = use_info['token']
            url = use_info['url']



            print('ACQUIRE')
            influx_sem.acquire()
            try:
                client = influxdb_client.InfluxDBClient(
                    url=url,
                    token=token,
                    org=org
                )
                write_api = client.write_api(write_options=SYNCHRONOUS)
                write_api.write(bucket=bucket, record=record)
            except:
                print(traceback.format_exc())
                successfully_submitted = False
                time.sleep(5)
            
            finally:
                if successfully_submitted:
                    submitted_to.append(name)
                print('RELEASE')
                influx_sem.release()
        if len(submitted_to) < len(influxdb_instances):
            message_queue.insert(0, message)

SEC = 1
MIN = 60*SEC
HOUR = 60*MIN

def printers_that_are_off(current_printers):

    # Figure out whether there is a thread running for each printer.
    while True:
        # We will use authentication_dict as a list of printers for now,
        # later we should write the printers to an external file so that
        # they can dynamically change.
        print('Thread checking...')

        for name in authentication_dict:
            if name in current_printers:
                print('Skipped printer', name)
                continue


            print('Off printer:', name, 'has had print event 0.')
            p = influxdb_client.Point("send efficient 2") \
                    .tag("Name", name) \
                    .field("Print Event", 0) \
                    .field("Step", -1)

            # Send that the printer
            write_influx(p)
            '''
            print('ACQUIRE 1')
            influx_sem.acquire()
            try:
                print(1)
                client = influxdb_client.InfluxDBClient(
            url=url,
            token=token,
            org=org
        )
                print(2)
                client.write_api(write_options=SYNCHRONOUS).write(bucket=bucket, record=p)
                print(3)
                
                #with InfluxDBClientSync(url=url, token=token, org=org) as client:
                #    client.write_api().write(bucket=bucket, record=p)
            finally:
                print('RELEASE 1')
                influx_sem.release()
            '''

        time.sleep(12*HOUR)


def main():
    t = Thread(target=influxdb_writer, args=(), daemon=True)
    t.start()
    print('Sending offs...')
    for name in authentication_dict:
        p = influxdb_client.Point("send efficient 2") \
                .tag("Name", name) \
                .field("Extruder Temperature", 0) \
                .field("Target Temperature", 0) \
                .field("Percentage Done", 0) \
                .field("Step", -1)

        write_influx(p)
        '''
        print('ACQUIRE 2')
        influx_sem.acquire()
        try:
            client = influxdb_client.InfluxDBClient(
        url=url,
        token=token,
        org=org
    )
            client.write_api(write_options=SYNCHRONOUS).write(bucket=bucket, record=p)
        finally:
        
            #with InfluxDBClientSync(url=url, token=token, org=org) as client:
            #    client.write_api().write(bucket=bucket, record=p)
            print('RELEASE 2')
            influx_sem.release()
        '''
    print('We have declared that the printers are off!')
    loop_handler = AsyncLoopThread()
    loop_handler.start()

    current_printers = {}

    t = Thread(target=printers_that_are_off, args=(current_printers,), daemon=True)
    t.start()


    discovery = newapi.MakerbotDiscovery()
    print("start")
    while True:
        found = discovery.discover()
        print('Found:', found)
        for ip, name, identity in found:
            if name not in current_printers:
                print("found new printer:", name)
                printer = MakerbotPrinter(ip, authentication_dict[name])
                t = Thread(target=printer.send_info, args=(current_printers,), daemon=True)
                t.start()
                current_printers[name] = t
                #asyncio.run_coroutine_threadsafe(printer.send_info(current_printers), loop_handler.loop)
                t = Thread(target=printer.send_camera_stream, daemon=True)
                t.start()
                #asyncio.run_coroutine_threadsafe(printer.send_camera_stream(), loop_handler.loop)
                print("Added Printer")


if __name__ == '__main__':
    main()
#asyncio.run(main())
