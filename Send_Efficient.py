#import makerbotapi
import asyncio
import traceback
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
import newapi

# Control if we are sending data to the server
SEND_TO_SERVER = True

from threading import Thread

class AsyncLoopThread(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.loop = asyncio.new_event_loop()

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.loop.run_forever()


# Create a class for makerbot printers
class MakerbotPrinter:
    def __init__(self, ip, auth_code):
        self.ip = ip
        self.auth_code = auth_code

    def connect(self):
        self.makerbot = newapi.MakerbotAPI(self.ip, self.auth_code)
        # self.makerbot.auth_code = auth_code
        print('Connecting...', self.ip)
        self.makerbot.connect()
        # self.makerbot.get_access_token('jsonrpc')
        print('Authenticating...')
        self.makerbot.authenticate_json_rpc()
        print('Getting system info...')
        self.name = self.makerbot.get_system_information()["result"]["machine_name"]

    # send the info of the Makerbot printer
    async def send_info(self):
        print('About to connect!')
        self.connect()
        print('Loop.')
        await makerbot_printer_send(self.makerbot)


# Information for Influxdb
#bucket = "Test"
#org = "Capstone2023"
#token = "4aFaY-CgICA4eRIzKCUuiYX-_yShjxqn6QDegWWua7Qtxw_tKF_e_JdmcrsmrsHPePdaBV-0UK2xA7inQSN8xg=="
# Store the URL of your InfluxDB instance
#url = "http://10.42.1.1:8086"

# This would have to be changed for different influxdbs
bucket = "Test"  # "data"
org = "hi"  # "Capstone"
token = "mBjdj3dNyvjlPdA-ITljUZMLiZwdFA3H2cg1K0k69R3ehY9pJIRY8wrvW-4VOr_PsW0QFSKH7B_CgDxMg3LKIg=="  # "z4RcZfk0S0jGhY4U5GBSziX5MtGUSBlni0y-mOJXwPMI5swpJBOqcpajC2m6z5EASLGpJttJSkYgDYAGu110sg=="
# Store the URL of your InfluxDB instance
url = "http://localhost:8086"

# "initial_heating = 1"
# "homing = 2"
# "final heating = 3"
# "printing = 4"
# "cleaning_up = 5"
# "completed = 6"

authentication_dict = {"Printer 001" : 'OZnEMWIUOwVYFZHpdXJcJPteACJxbWnH',
                       "Printer 002" : 'vAzoexoBEKpMjWelEKkPOQzGhFPpqOtQ'}

# Step Encodings
STEP_NUMBERS = {'idle': 0,
                'initial_heating': 1,
                'homing': 2,
                'final_heating': 3,
                'printing': 4,
                'cleaning_up': 5,
                'completed': 6}


async def makerbot_printer_send(name):
    while True:
        try:
            print('Getting system info...')
            # Retrieve data from system
            data = name.get_system_information()
            print(data["result"])#["machine_name"])
            if not SEND_TO_SERVER:
                await asyncio.sleep(30)
                continue
            current_process = data['result']['current_process']
            # Send the name of the device and the extruder temperature to influxdb
            p = influxdb_client.Point("send efficient 2") \
                .tag("Name", data["result"]["machine_name"]) \
                .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"])

            # Set the step to be idle and percentage done to be 0 as default

            step_name = 'idle'
            perc_done = 0

            # if the printer is printing get the step and the percentage done of the job
            if current_process is not None:
                step_name = current_process['step']
                perc_done = current_process['progress']
            step_num = STEP_NUMBERS[step_name]
            p = p.field("Percentage Done", perc_done) \
                .field("Step", step_num)

            # if the printer is not printing send data every 30 seconds
            if current_process is None:
                await asyncio.sleep(2)

            # if the printer is printing send data every 5 seconds
            else:
                await asyncio.sleep(5)

            # write the data into influxdb
            async with InfluxDBClientAsync(url=url, token=token, org=org) as client:
                await client.write_api().write(bucket=bucket, record=p)
            print('sent')
            print(p)

        except Exception as e:
            print(traceback.format_exc())
            
async def main():
	loop_handler = AsyncLoopThread()
	loop_handler.start()
	discovery = newapi.MakerbotDiscovery()
	current_printers = {}
	while True:
		for ip, name, id in discovery.discover():
			#print('Discovered:', ip, name)
			if name not in current_printers:
			    print("found new printer:", name)
			    current_printers[name] = asyncio.run_coroutine_threadsafe(MakerbotPrinter(ip, authentication_dict[name]).send_info(), loop_handler.loop)
			    #current_printers[name] = MakerbotPrinter(ip, authentication_dict[name]).send_info()
			    #asyncio.create_task(current_printers[name])
			    print("Added Printer")




'''
async def main()
	printer_lst = []
	while True:
		discovered_lst = [ip for ip, namne, id in discovery.discover()]
		for ip in discovered_lst:
    		printer_lst = [MakerbotPrinter('192.168.4.4', 'OZnEMWIUOwVYFZHpdXJcJPteACJxbWnH'),
                   	MakerbotPrinter('192.168.4.5', 'vAzoexoBEKpMjWelEKkPOQzGhFPpqOtQ')]
    		await asyncio.gather(*[printer_lst[0].send_info(),
                         printer_lst[1].send_info()])
    #loop_handler = AsyncLoopThread()
    #loop_handler.start()
'''


asyncio.run(main())
