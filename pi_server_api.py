# The "beacon"

from typing import Union

import argparse
import requests
import json
import os
import os.path
import socket
import sys
import time
import traceback
from threading import Thread
from threeamigosrpc import ThreeAmigosAPI, AmigosRPC
import uuid
from misc import TrackedImage, modules_in_path

from iot_influxdb import InfluxDBWriter
from misc import ProgramRunner, RunningProgram, MACHINE_INFO_JSON, MAIN_CONFIG_JSON
from misc import amigos_connection_info


this_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(this_path)  #('/home/capstone2023/Desktop')

#nohup screen -S livestream -dm python3 livestream.py &
#creem screen -X -S <session_name> 

HOME_TEST = os.path.exists('home_test')

# This is also loaded from the config file
# host = '192.168.0.219'  # host = '0.0.0.0'

port = 10000
MB = 1024**2

        
app = ThreeAmigosAPI()

def start(program):
    if not HOME_TEST:
        os.system(f'screen -X -S {program} quit')
        print(f'start {repr(program)}')
        #os.system(f'nohup screen -S {program} -dm python3 {program}.py &')
        os.system(f'screen -S {program} -dm python3 {program}.py')

def kill(program):
    if not HOME_TEST:
        os.system(f'screen -X -S {program} quit')

def admin(program):
    if not HOME_TEST:
        os.system(f'{program}')


@app.post("/run/program")
def run_program(params):
    params = params['data']
    start(params['command'])

@app.post("/kill/program")
def kill_program(params):
    params = params['data']
    kill(params['command'])
    
@app.post("/upload/program")
def upload(params):
    fname = params['file_name']
    try:
        if HOME_TEST:
            for chunk in params['file_stream']:
                pass
            return {"message": f"Successfully uploaded {fname}"}
        cur_path = '.'
        for folder_name in fname.split('/')[:-1]:
            cur_path += '/' + folder_name
            if not os.path.exists(cur_path):
                os.mkdir(cur_path)
        with open(fname, 'wb') as f:
            for chunk in params['file_stream']:
                f.write(chunk)
    except Exception:
        error_text = traceback.format_exc()
        print(error_text)
        return {'message': 'There was an error uploading the file', 'error_text': error_text}

    return {"message": f"Successfully uploaded {fname}"}

@app.post("/admin")
def administrator(params):
    params = params['data']
    admin(params['command'])


@app.get("/machines")
def machines(params):
    params = params['data']
    return machines_info


@app.post("/machines/add")
def add_machine(params):
    params = params['data']
    machine_info = params
    machines = machines_info['machines']
    machines.append(machine_info)

    # TODO: Actually initialise the machine? Start programs or threads?

    # TODO: Make another machine_info field for a machine to store authentication codes and other things too, though the server has no control over that part in editing..

    update_machines_file()

def find_machine_index(machine_name):
    for i, a_machine_info in enumerate(machines_info['machines']):
        if a_machine_info['name'] == machine_name:
            break
    else:  # No break
        print('Problem: Did not find machine with name:', prev_machine_name)
        return
    return i

@app.post("/machines/edit")
def edit_pi(params):
    params = params['data']
    machines_info['name'] = params['new_name']

    update_machines_file()

@app.post("/machines/edit")
def edit_machine(params):
    params = params['data']
    prev_machine_name = params['prev_name']
    machine_info = params['info']

    i = find_machine_index(prev_machine_name)
    if i is None:
        return
    
    machines_info['machines'][i] = machine_info

    # TODO: Use a uid? That way prev_machine_name is unnecessary...
    #       e.g. str(uuid.uuid4())

    # TODO: Rebuild entire machines_info after updating based on uid...
    #       Use separate classes to handle to machine threads and each machine_info used in this construction process.

    update_machines_file()

@app.post("/machines/delete")
def delete_machine(params):
    params = params['data']
    machine_name = params['name']

    i = find_machine_index(prev_machine_name)
    if i is None:
        return

    # TODO: Same things as for edit_machine...
    
    machines_info['machines'].pop(i)

    update_machines_file()

#        r = self.post(pi_name, '/machines/replace_config', json={'name': machine_name, 'config': config})
@app.post("/machines/replace_config")
def replace_config(params):
    params = params['data']
    machine_name = params['name']
    config = params['config']
    print('Replacing config!', machine_name, config)
    machines = machines_info['machines']
    i = find_machine_index(machine_name)
    if i is None:
        print('No such machine found:', machine_name)
        return
    machine_info = machines[i]

    machine_info['config'] = config

    update_machines_file()
    

@app.post('/program/available')
def available_programs(params):
    params = params['data']
    # Return binned by machine_type
    # Loop through the modules...
    #for machine_type, machine_module in pr.machine_modules.items():
    available = {}
    for machine_type, program_dict in pr.named_programs.items():
        available[machine_type] = list(program_dict)
    return available
@app.post('/program/running')
def running_programs(params):
    params = params['data']
    machine_name = params['name']
    # For a single machine name
    # for machine_info in machines_info['machines']:
    #     machine_name = machines_info['name']
    i = find_machine_index(machine_name)
    if i is None:
        error_msg = f'Did not find requested name: {machine_name}...'
        print(error_msg)
        return {'error': error_msg}
    machines = machines_info['machines']
    machine_info = machines[i]
    running_lst = pr.get_running(machine_info)
    # Each item in the result is {'name': ..., 'type': ...}
    return {'running': running_lst}
@app.post('/program/restart')
def restart_program(params):
    params = params['data']
    machine_name = params['name']
    i = find_machine_index(machine_name)
    if i is None:
        error_msg = f'Did not find requested name: {machine_name}...'
        print(error_msg)
        return {'error': error_msg}
    machines = machines_info['machines']
    machine_info = machines[i]
    program_type = params['program_type']
    print(f'Requested killing program {program_type} for machine {machine_name}:')
    program_lst = pr.get_running(machine_info)
    for this_program_info in program_lst:
        this_program_name = this_program_info['name']
        this_program_type = this_program_info['type']
        if program_type == this_program_type:
            print(f'Killing program: {this_program_name}...')
            pr.stop(machine_info, this_program_name)
    print(f'Starting program: {program_type}...')
    # TODO: Make a function for the below three calls, taking machine_info and program_type...
    programs_options = pr.get_programs_options(machine_info)
    options = programs_options.get(program_type, {})
    pr.start(machine_info, program_type, args=(influx_writer, camera_man, machine_info, options))
    print('The restarting is over.')
@app.post('/program/stop_all')
def stop_all_programs(params):
    params = params['data']
    machine_name = params['name']
    i = find_machine_index(machine_name)
    if i is None:
        error_msg = f'Did not find requested name: {machine_name}...'
        print(error_msg)
        return {'error': error_msg}
    machines = machines_info['machines']
    machine_info = machines[i]
    print(f'Killing all programs for machine {machine_name}:')
    program_lst = pr.get_running(machine_name)
    for program_info in program_lst:
        program_name = program_info['name']
        print(f'Killing program: {program_name}...')
        pr.stop(machine_info, program_name)
    print('Killing over.')
    #pr.stop_all(machine_info, )
@app.post('/program/stop')
def stop_program(params):
    params = params['data']
    machine_name = params['name']
    i = find_machine_index(machine_name)
    if i is None:
        error_msg = f'Did not find requested name: {machine_name}...'
        print(error_msg)
        return {'error': error_msg}
    machines = machines_info['machines']
    machine_info = machines[i]
    program_type = params['program_type']
    print(f'Killing program {program_type} for machine {machine_name}...')
    program_lst = pr.get_running(machine_info)
    for this_program_info in program_lst:
        this_program_name = this_program_info['name']
        this_program_type = this_program_info['type']
        if program_type == this_program_type:
            print(f'Killing program: {this_program_name}...')
            pr.stop(machine_info, this_program_name)
    # pr.stop(machine_info, program_name)
    print('The killing is over.')
# List all programs, running programs, etc, restart, stop (store the error cause and return it here)
# TODO: Implement the above in pi_manager

def load_principal_config():

    global principal_config

    with open(MAIN_CONFIG_JSON) as f:
        principal_config = json.loads(f.read())
    # FROM config INSTEAD!
    changes = False 
    # host = machines_info['hostname']
    '''
    if changes:
        with open(MAIN_CONFIG_FILE, 'w') as f:
            f.write(json.dumps(config, indent=2))
    '''


def load_machines_file():
    with open(machine_file) as f:
        content = f.read()
        machines_info = json.loads(content)

    if 'machines' not in machines_info:
        machine_name = machines_info['name']
        machine_type = machines_info['type']
        machines_info = {'name': f'Pi {machine_name}', 'machines': [machines_info]}
        if store_machine_info:
            with open(machine_file, 'w') as f:
                f.write(json.dumps(machines_info, indent=4))
            print('Wrote new machine info!')

    return machines_info


@app.post("/replace_pi_config")
def replace_pi_config(params):
    params = params['data']
    config = params['config']

    machines_info['config'] = config

    update_machines_file()

def update_machines_file():
    if store_machine_info:
        with open(machine_file, 'w') as f:
            f.write(json.dumps(machines_info, indent=2))

# Connect the socket to the port where the server is listening
CONFIG_FILE = 'pi_server_config.json'
server_address = ('192.168.0.219', 10000)
config = {'server_address': server_address}
if not os.path.exists(CONFIG_FILE):
    with open(CONFIG_FILE, 'w') as f:
        f.write(json.dumps(config))
else:
    with open(CONFIG_FILE) as f:
        config = json.loads(f.read())
server_address = config['server_address']

machine_file = MACHINE_INFO_JSON#'machine_info.json'


# cameras = {}
# CAMERA_FILE = 'cameras.json'
# if os.path.exists(CAMERA_FILE):
#     with open(CAMERA_FILE) as f:
#         cameras = json.loads(f.read())
#cameras = {}

CAMERA_MOD_PATH = 'camera_types'

class CameraReader:
    def __init__(self, tracked_image):
        self.tracked_image = tracked_image
        self.img_c = -1

    def new_and_latest_image(self, callback=None):
        while self.tracked_image.count <= self.img_c:
            if callback is not None:
                callback()
            time.sleep(0.01)
        while self.tracked_image.image is None:
            if callback is not None:
                callback()
            time.sleep(0.01)
        image = self.tracked_image.image 
        self.img_c = self.tracked_image.count
        return image

class CameraMan:

    def __init__(self):

        self.cameras = {}

        # Load camera modules
        raw_camera_modules = modules_in_path(CAMERA_MOD_PATH)
        print('Loading camera modules from', CAMERA_MOD_PATH)
        print(raw_camera_modules)

        self.camera_modules = {}
        for camera_module in raw_camera_modules:
            self.camera_modules[camera_module.camera_type] = camera_module

        self.camera_threads = {}
        self.camera_handlers = {}

    def get_camera_reader(self, camera_name):
        if camera_name not in self.cameras:
            return None
        return CameraReader(self.cameras[camera_name].tracked_image)

    def get_tracked_image(self, camera_name):
        camera = self.cameras.get(camera_name)
        if camera is None:
            return None
        return camera.tracked_image

    def kill_camera(self, camera_name):
        if camera_name not in self.cameras:
            return
        camera = self.cameras.pop(camera_name)
        camera.self_destruct()

    def add_camera(self, camera_name, camera_cls, camera_options, associated_machines):
        if camera_name in self.camera_handlers:
            print(f'Failed to start camera {camera_name}, since it is already running...')
            return
        '''
        try:
            camera_handler = camera_module.Camera(camera_options)
        except:
            # Get the error text and store it.
            # TODO: Use the store error text to send to server at somepoint, getting program status, etc...
            self.error_text = traceback.format_exc()
            print(f'Failed to create camera stream for camera: {camera_name}...')
            print(self.error_text)
            return
        '''
        # This stores the latest camera frames:
        # Use CameraFrame class from misc also used by image_streaming_wrangler
        # NOTE: These are accessible by the single file to rule them all classes/threads for each machine!

        camera_thread = CameraThread(camera_name, camera_cls, associated_machines)  # camera_type, 

        camera_thread.run(camera_options)

        self.cameras[camera_name] = camera_thread

        #t = Thread(target=camera_thread, args=(camera_name, camera_type, camera_handler, tracked_image), daemon=True)

        #self.cameras[camera_name] = tracked_image
        #self.camera_handlers[camera_name] = camera_handler
        #self.camera_threads[camera_name] = t
        

    #def camera_thread(self, camera_name, camera_type, camera_handler, tracked_image):

class CameraThread:
    def __init__(self, camera_name, camera_cls, associated_machines):  # camera_type, 
        self.tracked_image = TrackedImage(None)
        self.camera_name = camera_name
        #self.camera_type = camera_type
        self.camera_handler = None
        self.t = None
        self.destruct = ARMED
        self.running = False
        self.error_text = None
        #self.camera_module = camera_module
        self.camera_cls = camera_cls
        self.associated_machines = associated_machines
    def self_destruct(self):
        if not self.running:
            return
        self.destruct = DETONATE
        self.t.join()
        self.running = False
    def run_thread(self, camera_options):
        self.running = True
        amigos = None
        try:
            #self.camera_handler = self.camera_module.Camera(camera_options)
            self.camera_handler = self.camera_cls(camera_options)

            # Reconnect when connection lost!
            while True:
                print(f'Camera {self.camera_name} is connecting...')
                try:

                    # Create a connection to the server with armigos rpc, and the pi name and camera name
                    host, port, tls = amigos_connection_info(principal_config['amigos_images'])
                    amigos = AmigosRPC(server=False, server_address=(host, port), tls=tls)
                    # = amigos.recieve()
                    pi_name = machines_info['name']
                    response = {'pi_name': pi_name, 'camera_name': self.camera_name, 'associated_machines':self.associated_machines}
                    amigos.send({'data': response, 'amigos_version': 1, 'method': 'response'})
                    MAX_PRINT_N = 2
                    is_to_print = lambda: self.tracked_image.count < MAX_PRINT_N
                    do_print = lambda *args, **kw_args: print(f'[{self.camera_name}]', *args, **kw_args) if is_to_print() else None
                    while not self.destruct:
                        
                        do_print('Getting frame...')
                        jpg_frame = self.camera_handler.get_frame()
                        do_print('Got frame.')

                        #cameras[camera_name] = TrackedImage()
                        self.tracked_image.image = jpg_frame
                        self.tracked_image.timestamp = time.time()
                        self.tracked_image.count += 1
                        #print(f'For {self.camera_name}, got frame {self.tracked_image.count}...')

                        # Send it to the server:
                        response = {}  # NOTE: Could include timestamp, count, etc...
                        do_print('Sending...')
                        amigos.send({'data': response, 'amigos_version': 1, 'method': 'response', 'file_name': 'image.jpg', 'file_content': jpg_frame})
                        do_print('Sent.')

                        #time.sleep(1/5)  # (let this be chosen by the camera type?)
                    print('Out of loop when sending image frames.')

                except Exception as e:
                    print(traceback.format_exc())
                finally:
                    print('Closing socket')
                    if amigos is not None:
                        try:
                            amigos.sock.close()
                        except Exception as e:
                            print('While trying to close socket:')
                            print(traceback.format_exc())
                time.sleep(10)

        except:
            self.error_text = traceback.format_exc()
            print(self.error_text)
        self.running = False
    def run(self, camera_options):
        self.t = Thread(target=self.run_thread, args=(camera_options,), daemon=True)
        self.t.start()

def initialise_cameras():

    global camera_man

    # This is like a "pi config", shoudl put it in machines.json...
    if 'config' not in machines_info:
        machines_info['config'] = {'cameras': {}}
        # Each camera has the camera_info form of: {'type': 'picam', 'options': {}, ...} with key value of 'name'
        # {'cameras': {'pi_cam1': {'type': 'picam', 'options': {}, ...}}}
    pi_config = machines_info['config']
    if 'cameras' not in pi_config:
        pi_config['cameras'] = {}

    update_machines_file()

    camera_man = CameraMan()

    for camera_name, camera_info in pi_config['cameras'].items():
        camera_type = camera_info['type']
        options = camera_info['options']

        if camera_type not in camera_man.camera_modules:
            print(camera_man.camera_modules)
            print(f'Error: Unrecognised camera type: {camera_type}.')
            continue
        camera_module = camera_man.camera_modules[camera_type]
        camera_cls = camera_module.Camera
        camera_man.add_camera(camera_name, camera_cls, options, None)


MACHINE_MOD_PATH = 'machines'

def initialise_machines():

    global pr

    #raw_machine_modules = modules_in_path(MACHINE_MOD_PATH)
    #named_programs = {machine_module.machine_type: machine_module for machine_module in machine_types_modules}
    psapi = __import__(__name__)
    pr = ProgramRunner(api_callback_args=(app,psapi))


    # Loop through machines:
    for machine_info in machines_info['machines']:

        machine_name = machine_info['name']
        machine_type = machine_info['type']

        # machine_info

        # Each program has a name, the way to start the program with this name, what params to give it,e tc need to be specified somewhere also...
        # NOTE: The params format will change ebtween the ProgramRunner on the pi and on the server, so we need to account for this.
        # ...
        # Machine_name is a param... We need to tell the name of the program...
        program_types = pr.named_programs.get(machine_type, {})
        programs_options = pr.get_programs_options(machine_info)
        for program_type in program_types:
            # mcahine_info, program_type, options
            print(f'Starting {machine_name}/{program_type}...')
            options = programs_options.get(program_type, {})
            pr.start(machine_info, program_type, args=(influx_writer, camera_man, machine_info, options))
            print(f'Started {machine_name}/{program_type}!')

    print('Machines initialised!')


# TODO: Move this class to misc or soemthing...

ARMED = False
DETONATE = True

# running_machines = {}

if __name__ == "__main__":

    # Check for command line args:
    parser = argparse.ArgumentParser(prog=sys.argv[0], description='Connect to IoT Capstone monitoring system as a monitoring device to collect data from machines.')
    parser.add_argument('-f', '--machine_file', dest='machine_file', help='Location of the machine info file to use.', default=MACHINE_INFO_JSON)
    #parser.add_argument('--do_no_modify_machine_info', dest='store_machine_info', action='store_false')
    parser.set_defaults(store_machine_info=False)

    args = parser.parse_args()
    store_machine_info = args.store_machine_info
    machine_file = args.machine_file

    machines_info = load_machines_file()

    load_principal_config()

    # Load the influxdb_writer
    #influxdb_writer = ...
    influx_writer = InfluxDBWriter(machine_info_file=machine_file)

    # Read cameras from config file... Do not start the machine sourced ones:
    # List of cameras somewhere... Input stream can be custom, usb camera, pi camera, etc...
    # One thread for each camera, and each auto reconnects to server on port 10001, sending images.
    # TODO: The cameras can be added during runtime when initaliing a new machine,e tc...
    # NOTE: Each one is given a unique name, including those started at machine init... (the cameras associated with a machine can be stored in the machine_info as a dictionary (e.g. keys 'main_cam' and 'pi_cam', pointing to the respective values 'runtime_cam_1', 'pi_camera_1'), though that is handled in pi backend manager, and multiple machines can use the same camera source...)
    # (all of the sensors should work like this...)
    # Better: configure machines, so we can specify which sensor to use if there are many of the same type, etc...
    #         And the same config place says the names of cameras to use, e.g. for 'main_cam' to use 'runtime_cam_1', etc... The server gets sent this config too, so the main_cam, etc, can be used in the dashboards...
    #         Also, the name "runtime_cam_1" can be user chosen for this speicifc machines built in camera.
    initialise_cameras()
    # We have a camera types folder on the server with module files, that get copied here and used.

    initialise_machines()

    # This is the new pi_client.py:
    host, port, tls = amigos_connection_info(principal_config['amigos_control'])
    app.run(host=host, port=port, tls=tls)
