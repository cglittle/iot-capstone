import influxdb_client
from influxdb_client.client.write_api import ASYNCHRONOUS
import random
import time
import numpy as np
import configparser
import os
import threading

CONFIG_FILE = 'iot4config.ini'

SIMULATION_HOURS = 12

config = configparser.ConfigParser()
if os.path.exists(CONFIG_FILE):
    config.read(CONFIG_FILE)

influxdb_instances = {}

print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    influxdb_instances[name] = config[name]

use_info = influxdb_instances['local']
bucket = use_info['bucket']
org = use_info['org']
token = use_info['token']
url = use_info['url']

client = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
)
# create a list of all the printer names
write_api = client.write_api(write_options=ASYNCHRONOUS)

closing_time = SIMULATION_HOURS * 60 * 60


def off_status(printer_name, print_num, current_time):
    p = influxdb_client.Point("fake_machine") \
        .tag("Name", printer_name) \
        .field("Fall", 0)
    write_api.write(bucket=bucket, org=org, record=p)
    if (closing_time - current_time) > (120+60)*60:
        wait_time = random.randint(0 * 60, 120 * 60)
    elif (closing_time - current_time) > 70*60:
        wait_time = random.randint(0 * 60, (closing_time - current_time) - 60*60)
    else:
        wait_time = 120*60
    step = -1
    p = influxdb_client.Point("fake_machine") \
        .tag("Name", printer_name) \
        .field("Step", step) \
        .field("Print Event", 0)
    write_api.write(bucket=bucket, org=org, record=p)
    # print(wait_time)
    time_counter = 0
    while time_counter < wait_time :
        time_counter += 5
        current_time += 5
        time.sleep(5)
    # print("moving to idle")
    if (closing_time - current_time) > 40*60:
        idle_status(printer_name, True, 21, print_num, current_time)
    else:
        off_status(printer_name, print_num, current_time)


def idle_status(printer_name, was_off, current_temp, print_num, current_time):
    p = influxdb_client.Point("fake_machine") \
        .tag("Name", printer_name) \
        .field("Fall", 0)
    write_api.write(bucket=bucket, org=org, record=p)
    if was_off:
        wait_time = random.randint(1 * 60, min(20 * 60, (closing_time - current_time)))
    else:
        wait_time = random.randint(1 * 60, min(40 * 60, (closing_time - current_time)))
    # print(wait_time)
    time_counter = 0
    while time_counter < wait_time:
        step = 0
        if current_temp > 21:
            current_temp -= round(max(1, (current_temp - 21) / 16) + random.normalvariate(0, 1))

        p = influxdb_client.Point("fake_machine") \
            .tag("Name", printer_name) \
            .field("Step", step) \
            .field("Extruder Temperature", current_temp)
        write_api.write(bucket=bucket, org=org, record=p)
        time_counter += 5
        current_time += 5
        time.sleep(5)
    next_state = random.randint(1, 100)
    if (next_state < 40 and not was_off) or (closing_time - current_time) <= 40*60:
        # print("moving to off")
        off_status(printer_name, print_num, current_time)
    else:
        # print("moving to printing")
        printing_status(printer_name, current_temp, print_num, current_time)


def printing_status(printer_name, current_temp, print_num, current_time):
    print_num += 1

    print_name = printer_name + "_print_" + str(print_num)

    p = influxdb_client.Point("fake_machine") \
        .tag("Name", printer_name) \
        .field("Print Event", 1)
    write_api.write(bucket=bucket, org=org, record=p)
    print_time = random.randint(20 * 60, min(180 * 60, (closing_time - current_time) - 10*60))
    # print(print_time)
    time_counter = 0
    fall_value = random.randint(1, 100)
    fall_time = random.randint(20, 85)
    if fall_value <= 25:
        cancellation_value = 100
    else:
        cancellation_value = random.randint(1, 100)
    cancellation_time = random.randint(20, 90)
    while current_temp < 180:
        current_temp += round(7 + random.normalvariate(0, 1))
        step = 1
        p = influxdb_client.Point("fake_machine") \
            .tag("Name", printer_name) \
            .field("Step", step) \
            .field("Extruder Temperature", current_temp) \
            .field("Print Job Name", print_name)
        write_api.write(bucket=bucket, org=org, record=p)
        current_time += 5
        time.sleep(5)
    while time_counter < 30:
        step = 2
        current_temp += round(random.normalvariate(0, 1))
        p = influxdb_client.Point("fake_machine") \
            .tag("Name", printer_name) \
            .field("Step", step) \
            .field("Extruder Temperature", current_temp) \
            .field("Print Job Name", print_name)
        write_api.write(bucket=bucket, org=org, record=p)
        time_counter += 5
        current_time += 5
        time.sleep(5)
    time_counter = 0
    while current_temp < 215:
        current_temp += round(5 + random.normalvariate(0, 1))
        step = 3
        p = influxdb_client.Point("fake_machine") \
            .tag("Name", printer_name) \
            .field("Step", step) \
            .field("Extruder Temperature", current_temp) \
            .field("Print Job Name", print_name)
        write_api.write(bucket=bucket, org=org, record=p)
        current_time += 5
        time.sleep(5)
    while time_counter < print_time:
        step = 4
        current_temp += round(random.normalvariate(0, 2) - (current_temp - 215) / 40)
        p = influxdb_client.Point("fake_machine") \
            .tag("Name", printer_name) \
            .field("Step", step) \
            .field("Extruder Temperature", current_temp) \
            .field("Print Job Name", print_name) \
            .field("Elapsed Time", time_counter) \
            .field("Time Remaining", print_time - time_counter) \
            .field("Percentage Done", round(time_counter * 100 / print_time))
        write_api.write(bucket=bucket, org=org, record=p)
        time_counter += 5
        current_time += 5
        time.sleep(5)
        if cancellation_value <= 0 and round(time_counter * 100 / print_time) >= cancellation_time:
            p = influxdb_client.Point("fake_machine") \
                .tag("Name", printer_name) \
                .field("Print Event", 2)
            write_api.write(bucket=bucket, org=org, record=p)
            idle_status(printer_name, False, current_temp, print_num, current_time)
            return
        if fall_value <= 25 and round(time_counter * 100 / print_time) >= fall_time:
            p = influxdb_client.Point("fake_machine") \
                .tag("Name", printer_name) \
                .field("Fall", 1)
            write_api.write(bucket=bucket, org=org, record=p)
            if round(time_counter * 100 / print_time) >= (fall_time + 10):
                p = influxdb_client.Point("fake_machine") \
                    .tag("Name", printer_name) \
                    .field("Print Event", 2)
                write_api.write(bucket=bucket, org=org, record=p)
                idle_status(printer_name, False, current_temp, print_num, current_time)
                return
    p = influxdb_client.Point("fake_machine") \
        .tag("Name", printer_name) \
        .field("Print Event", 3)
    write_api.write(bucket=bucket, org=org, record=p)
    time_counter = 0
    completion_time = random.randint(1 * 60, min(20 * 60, (closing_time - current_time) - 5*60))
    while time_counter < completion_time:
        step = 6
        if current_temp > 21:
            current_temp -= round(max(1, (current_temp - 21) / 16) + random.normalvariate(0, 1))
        p = influxdb_client.Point("fake_machine") \
            .tag("Name", printer_name) \
            .field("Step", step) \
            .field("Extruder Temperature", current_temp) \
            .field("Print Job Name", print_name)
        write_api.write(bucket=bucket, org=org, record=p)
        time_counter += 5
        current_time += 5
        time.sleep(5)
    # print("moving to idle")
    write_api.write(bucket=bucket, org=org, record=p)
    idle_status(printer_name, False, current_temp, print_num, current_time)


thread_dict = {}
for i in range(1, 51):
    thread_dict[f"t{i}"] = threading.Thread(target=off_status, args=(f"Printer {i:03d}", 0, 0))

# starting thread 1
for i in range(1, 51):
    thread_dict[f"t{i}"].start()
