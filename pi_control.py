import paramiko
import configparser
from tempfile import NamedTemporaryFile 
import os
import os.path


MAIN_CONFIG_FILE = 'pi_setup.ini'
config = configparser.ConfigParser()
if os.path.exists(MAIN_CONFIG_FILE):
    config.read(MAIN_CONFIG_FILE)

# TODO: Move the authentication tokens entirely onto the pi... (a set machine up callback on the pi? Initiated from the management interface...)
#       (Use pi client socket to receive commands. Have pi client do everything like livestream and pi data connection too?)
# What about image_streaming_wrangler, put that (including streams, and finished print pictures) into pi_manager_backend...
# (have the image streams universal, and then the print pictures as a part of the printer_info.py plugin, with access to the image streams...)
# pi_control should also grab the necessary file names, etc, to upload from the printer_info.py? NO: It should all be in that python file!
SEND_TO_SERVER = config['DEFAULT']['send_to_server']
BROADCAST_INTERFACE = config['DEFAULT'].get('broadcast_interface')
if BROADCAST_INTERFACE is not None:
    BROADCAST_INTERFACE = BROADCAST_INTERFACE == 'True'
authentication_dict = {}
print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    print(name)
    printer_info = config[name]
    print(printer_info)
    
    authentication_dict[name] = printer_info['auth']     


def update_pi(machine_name, host, username, password):   
    key = machine_name
    print(host)
    print("Uploading to ", key)
    # create ssh client 
    ssh_client = paramiko.SSHClient()

    # remote server credentials
    port = 22
    
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh_client.connect(hostname=host,port=port,username=username,password=password)
    except:
        print('Did not work')
        return
    machine_kind = key.split(' ')[0]
    print(key)
    print(machine_kind)
    if machine_kind == 'Printer':
        f = NamedTemporaryFile(delete=False)
        f.write(('''\
[DEFAULT] 
send_to_server = True


[%s]
auth = %s
enable = True
'''%(key, authentication_dict[key])).encode('utf8'))
        f.close()
        
    
    f1 = NamedTemporaryFile(delete=False)
    
    machine_files = {'Printer' : ['livestream', 'c_d_new', 'pi_data_collection'], 'CNC' : ['livestream', 'accelerometer', 'magnetometer']}
    for script_name in machine_files[machine_kind]: 
        
        f1.write(f'''\
nohup screen -S {script_name} -dm python3 {script_name}.py &
'''.encode('utf8'))
    f1.close()


    f2 = NamedTemporaryFile(delete=False)
    f2.write(f'''\
[default]
printer_name = {key}
'''.encode('utf8'))
    f2.close()  
    
          



    #To exit screen without killing type ctrl + a  d 
    #To scroll ctrl + a  esc to stop scrolling esc
    #To view a screen screen -r name
    #To kill a specific screem screen -X -S <session_name> quit
    #To kill all screen pkill screen

    # create an SFTP client object
    ftp = ssh_client.open_sftp()
    for script_name in machine_files[machine_kind]: 
        ftp.put(f'{script_name}.py',f'Desktop/{script_name}.py')
        
        
        
        
        
        
    # download a file from the remote server
    files = ftp.put("iot4config_no_cloud.ini","Desktop/iot4config.ini")
    #files = ftp.put("printer_info.ini","Desktop/printer_info.ini")
    files = ftp.put(f1.name,"Desktop/pi_autostart.sh")
    files = ftp.put(f2.name,"Desktop/printer_info.ini")
    files = ftp.put('pi_client.py', "Desktop/pi_client.py")
    if machine_kind == 'Printer':
        files = ftp.put(f.name,"Desktop/pi_setup.ini")
        files = ftp.put("newapi.py","Desktop/newapi.py")

    # close the connection
    ftp.close()
    if machine_kind == 'Printer':
        os.unlink(f.name)
    os.unlink(f1.name)

    #command = "cd
    #stdin, stdout, stderr = s.exec_command(command)
    #for line in stdout.readlines():
    #    print(line)


    channel = ssh_client.invoke_shell()
    stdin = channel.makefile('wb')
    stdout = channel.makefile('rb')

    stdin.write('''
    killall python3
    cd Desktop
    chmod +x pi_autostart.sh
    sh pi_autostart.sh
    exit
    ''')
    print(stdout.read())


    print('Success!')

    ssh_client.close()
    
    
if __name__ == '__main__':
    from pi_manager_backend import MachinesBackend

    mb = MachinesBackend()

    machine_ips = mb.list_machines()

    '''
    machine_ips = {}
    for key in printer_info:
        if key == 'DEFAULT':
            continue
        host = printer_info[key]['host'] 
        machine_ips[key] = host
    '''
    

    #username, password = md.get_

    for machine_name, host in machine_ips.items():
        username, password = mb.pi_login(machine_name)
        update_pi(machine_name, host, username, password)





