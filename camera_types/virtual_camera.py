
import os.path
import time

camera_type = 'virtcam'

# TODO: inherit an appropriate template from misc
class Camera:
    def __init__(self, options):
        self.frame_i = 1

        self.fname_format = options['fname']

    def get_frame(self):

        prev_fname = None
        
        while True:
            fname = self.fname_format % self.frame_i  # '../test_algo/trial_10_{self.frame_i}.jpg'

            if os.path.exists(fname):
                break

            if prev_fname == fname:
                raise Exception(f'DID NOT FIND IMAGE: {fname}')

            self.frame_i = 1
            prev_fname = fname

        time.sleep(1/5)
        # print('Sending virt pi cam image:', fname)

        if fname is not None:
            self.frame_i += 1
            with open(fname, 'rb') as f:
                return f.read()

