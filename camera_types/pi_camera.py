from threading import Condition
import io
import time

camera_type = 'picam'

# TODO: Replace this code with something simpler, this is taken from part of the livestream.py code that was not written by us...
class StreamingOutput(object):
    def __init__(self):
        self.frame = None
        self.buffer = io.BytesIO()
        self.condition = Condition()

    def write(self, buf):
        if buf.startswith(b'\xff\xd8'):
            # New frame, copy the existing buffer's content and notify all
            # clients it's available
            self.buffer.truncate()
            with self.condition:
                self.frame = self.buffer.getvalue()
                self.condition.notify_all()
            self.buffer.seek(0)
        return self.buffer.write(buf)


# TODO: inherit an appropriate template from misc
class Camera:
    def __init__(self, options):
        # NOTE: We could use options to control things like resolution of this per machine, etc...
        import picamera
        self.capture = picamera.PiCamera(resolution='640x480', framerate=5)
        self.output = StreamingOutput()
        self.capture.start_recording(self.output, format='mjpeg')

    def get_frame(self):
        #time.sleep(1/5
        with self.output.condition:
            self.output.condition.wait()
            return self.output.frame
