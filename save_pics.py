
import cv2 as cv

import time
from threading import Thread, Semaphore
import os.path


#cap = cv.VideoCapture("https://camera.iotcapstone.space/pi_cam/Printer%20001")
cap = cv.VideoCapture("http://192.168.0.66:8000/stream.mjpg")
cap.set(3, 640)
cap.set(4, 420)
cap.set(cv.CAP_PROP_BUFFERSIZE, 0)
cap.set(cv.CAP_PROP_FPS, 2)

sem = Semaphore(1)
lastest_image = None

def get_images():
    global latest_image
    while True:
        success, img = cap.read()
        sem.acquire()
        latest_image = img
        sem.release()


def grab_an_image():
    global latest_image
    sem.acquire()
    latest_image = None
    sem.release()
    while latest_image is None:
        time.sleep(0.001)
    return latest_image

trial_num = 1

while os.path.exists(f'test_algo/trial_{trial_num}_1.jpg'):
    trial_num += 1

t = Thread(target=get_images, daemon=True)
t.start()
i = 0
while True:
    print("yes")
    i += 1
    img = grab_an_image()
    print("b")
    name = f'test_algo/trial_{trial_num}_{i}.jpg'
    cv.imwrite(name, img)
    time.sleep(30)
