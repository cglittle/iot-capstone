mkdir -p stls
openscad -o stls/printer_wedge.stl -D WHICH=5 mount.scad
openscad -o stls/base_slider.stl -D WHICH=6 mount.scad
openscad -o stls/base_mount.stl -D WHICH=2 mount.scad
openscad -o stls/hinge_slider.stl -D WHICH=4 mount.scad
openscad -o stls/camera_holder.stl -D WHICH=3 mount.scad
