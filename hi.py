
from fastapi import FastAPI
host = 'localhost'
port = 10002
server = f'http://{host}:{port}'
import uvicorn


app = FastAPI()

@app.get("/")
def root():
    return {"message": "Hi World!!!!"}

if __name__ == "__main__":
    uvicorn.run(app, host=host, port=port)

'''
import time
i = 0
while i < 10:
    print('hi')
    i+=1
'''