import configparser
import uuid
import json
import requests
from os import getenv
import os
import os.path
import random
import string
from bs4 import BeautifulSoup
import sys
import argparse
import importlib
import glob
import copy
from misc import modules_in_path, get_principal_config

def ensure_path(path):
    if not os.path.exists(path):
        os.mkdir(path)


principal_config = get_principal_config()

CAMERA_URL = principal_config['dash_camera_url']

EXTRA_PRINTS = False

api_key = principal_config['grafana_api_key']

HOMEPAGE_SCRIPTS_DIRECTORY = 'dashboard_modifiers'

for path in [HOMEPAGE_SCRIPTS_DIRECTORY]:
    ensure_path(path)

print('Loading dashboard modifiers...')
dashboard_modifiers = modules_in_path(HOMEPAGE_SCRIPTS_DIRECTORY)

#exit()

DEFAULT_DASHBOARD_DIRECTORY = 'dashboards'
DEFAULT_ALERT_DIRECTORY = 'alerts'

grafana_server = principal_config['grafana']

# grafana_server = 'http://localhost:3000'
# if HOME_TEST:
#     grafana_server = 'https://grafana.iotcapstone.space'

#ADD_TO_GIT = True

MAX_DATA_POINTS = 100000


bucket = 'Test'
measurement_name = 'send efficient 2'

def get_uid(uids):
    result = None
    for attempt in range(10):
        uid = ''.join(random.choices(string.ascii_lowercase, k=10))
        if uid not in uids:
            result = uid
            break

    if result is None:
        raise Exception('Too many attempts failed to find unique dashboard ID, the code up above must be changed, try increasing the number of random characters in the uid!')

    return result


class DashboardNotFound(Exception):
    pass


class AlertNotFound(Exception):
    pass



class MachineDashboards:

    def __init__(self, dashboard_directory=DEFAULT_DASHBOARD_DIRECTORY, alert_directory=DEFAULT_ALERT_DIRECTORY, dashboard_modifiers=dashboard_modifiers, bucket=bucket, measurement_name=measurement_name, grafana_server=grafana_server, grafana_server_url=None):
        self.headers = {'Authorization': f'Bearer {api_key}', 'Content-Type': 'application/json'}

        self.grafana_server = grafana_server

        self.grafana_server_url = grafana_server_url
        if self.grafana_server_url is None:
            self.grafana_server_url = self.grafana_server

        self.bucket = bucket
        self.measurement_name = measurement_name

        self.query_replacements = {'bucket: ': bucket, '_measurement == ': measurement_name} # , 'Name == ': 'Printer 001'}

        #self.home_dashboard_name = home_dashboard_name
        self.dashboard_modifiers = dashboard_modifiers

        self.dashboard_directory = dashboard_directory
        if not os.path.exists(self.dashboard_directory):
            os.mkdir(self.dashboard_directory)

        self.alert_directory = alert_directory
        if not os.path.exists(self.alert_directory):
            os.mkdir(self.alert_directory)

        self.session = requests.Session()
        self.session.headers = self.headers
            
        self.datas = self.data_sources()
            
        self.relist_all()

        self.data_source = 'InfluxDB'
        self.data_uid = self.datas[self.data_source]

    def relist_all(self):
        self.dashboards = self.list_dashboards()
        self.alerts = self.list_alerts()
        self.folders, self.full_folders = self.list_folders()

    def dashboard_links(self, mm):
        r = self.session.get(f'{self.grafana_server}/api/search?query=%')
        search_results = json.loads(r.content)
        dashboard_links = {}
        for search_result in search_results:
            uid = search_result['uid']
            url = search_result['url']
            name = search_result['title']
            machine_name = machine = mm.get_machine_name_for_dash(name)
            dashboard_links[name] = (machine_name, f'{self.grafana_server_url}{url}')
        return dashboard_links

    # Stage 1: Grab and print dashboards
    def list_dashboards(self):
        #r = requests.get(f'{self.grafana_server}/api/search?query=%', headers=self.headers, verify=True)
        r = self.session.get(f'{self.grafana_server}/api/search?query=%')
        # print(f'{r.status_code} - {r.content}')
        search_results = json.loads(r.content)
        dashboards = {}
        for search_result in search_results:
            uid = search_result['uid']
            name = search_result['title']
            dashboards[name] = uid
        return dashboards

    def dashboard_json(self, uid):
        # Make a session to store the headers instead?
        #r = requests.get(f'{self.grafana_server}/api/dashboards/uid/{uid}', headers=self.headers, verify=True)
        r = self.session.get(f'{self.grafana_server}/api/dashboards/uid/{uid}')
        return json.loads(r.content)['dashboard']

    def data_sources(self):
        #data_source = requests.get(f'{self.grafana_server}/api/datasources', headers=self.headers, verify=True)
        data_source = self.session.get(f'{self.grafana_server}/api/datasources')
        data_source = json.loads(data_source.content)
        print('Data sources:')
        print(json.dumps(data_source, indent=4))
        print()
        result = {}
        for source in data_source:
            result[source['name']] = source['uid'] 
        return result

    def new_uid_dashboard(self):

        uids = []
        for name, uid in self.list_dashboards().items():
            uids.append(name)

        result = get_uid(uids)

        return result

    def list_alerts(self):
        result = self.session.get(f'{self.grafana_server}/api/v1/provisioning/alert-rules')
        #print(result.content)
        return json.loads(result.content)

    def find_alert(self, uid=None, title=None):
        for alert in self.alerts:
            if uid is not None and uid == alert['uid']:
                return alert
            if title is not None and title == alert['title']:
                return alert
            # alert['title']
            # alert['ruleGroup']
            # alert['folderUID']
            # what is orgID?
            # need to set alert['id'] = None ?

    def query_machine_transplant(self, query, machine_name=None):
        new_query_replacements = self.query_replacements
        if machine_name is not None:
            new_query_replacements = {'Name == ': machine_name, **self.query_replacements}
        if EXTRA_PRINTS:
            print('Replacement summary:', new_query_replacements)
        for name, str_value in new_query_replacements.items():
            if EXTRA_PRINTS:
                print(f'Replacing {repr(name)} with {repr(str_value)}')
                print(f'Before: {query}')
            query = replace_query_param(query, name, str_value)  # '_measurement', 'send efficient 2')
            if EXTRA_PRINTS:
                print(f'After: {query}')
        return query

    def list_folders(self):
        r = self.session.get(f'{self.grafana_server}/api/folders')
        folders = {}
        things = json.loads(r.content)
        if EXTRA_PRINTS:
            print('Folders:')
            print(json.dumps(things, indent=4))
        full_folders = {}
        for folder_item in things:
            title = folder_item['title']
            uid = folder_item['uid']
            # print('FOLDER ID FOUND:', folder_item['id'])
            folders[title] = uid
            full_folders[title] = folder_item
        return folders, full_folders

    '''
    def folder_info(self, uid):
        r = self.session.get(f'{self.grafana_server}/api/folders/{uid}')
        return json.loads(r.content)
    '''
    
    def renew_folder(self, name):
        if name in self.folders and name in self.full_folders:
            return
        self.folders, self.full_folders = self.list_folders()
        if name in self.folders:
            return
        uids = [uid for _, uid in self.folders.items()]
        uid = get_uid(uids)
        #self.folders[name] = uid
        request_data = {'uid': uid, 'title': name}
        if name is None:
            raise Exception('Name is None!')
        r = self.session.post(f'{self.grafana_server}/api/folders', json=request_data)
        # relist the folders:
        self.folders, self.full_folders = self.list_folders()
        print(f'FOLDER CREATE {repr(name)}: {r.status_code}: {r.reason}')

    def rule_group_for_folder(self, folder_name, dash_name):
        #print(self.folders)
        #print(folder_name)
        if dash_name == 'Printer 2':
            pass  # input('Press enter to continue...')
        folder_uid = self.folders[folder_name]
        group = dash_name
        #r = self.session.get(f'{self.grafana_server}/api/v1/provisioning/folder/{folder_uid}/rule-groups/{group}')
        r = self.session.get(f'{self.grafana_server}/api/ruler/grafana/api/v1/rules/{folder_name}/{group}')
        rule_group = json.loads(r.content)
        #print(json.dumps(rule_group, indent=2))
        #exit()
        if dash_name == 'Printer 2':
            print('Return value:')
            print(rule_group)
            #input('Press enter to continue...')
        '''\
{
    "message": "rulegroup not found",
    "traceID": ""
}'''
        #if rule_group.get('message') == 'rulegroup not found':
        #    return None
        if not rule_group['rules']:
            return None
        if EXTRA_PRINTS:
            print('Rule group:')
            print(json.dumps(rule_group, indent=4))
        return rule_group
    
    def ensure_folders(self, dash_name, machine_name):
        # Deals with folder and alert group existence.
        self.renew_folder(machine_name)
    
    def delete_alerts(self, folder_name, group_name):
        r = self.session.delete(f'{self.grafana_server}/api/ruler/grafana/api/v1/rules/{folder_name}/{group_name}', verify=True)
        #r = self.session.delete(f'{self.grafana_server}/api/ruler/grafana/api/v1/rules/{folder_name}/{group_name}', json=upload_json, verify=True)
        print(f'DELETE ALERTS: {r.status_code}: {r.reason}')

    def update_alerts(self, folder_name, upload_json):
        r = self.session.post(f'{self.grafana_server}/api/ruler/grafana/api/v1/rules/{folder_name}', json=upload_json, verify=True)
        print(f'UPDATE ALERTS: {r.status_code}: {r.reason}')

    def renew_alert(self, dash_name, machine_name, template_dash_name=None, template_alert_folder_name=None, rule_group=None, upload_alerts=True, store_alerts=False, reload_template_from_file=False):
        self.ensure_folders(dash_name, machine_name)
        rule_group = self.alert_transplant(machine_name, machine_name, dash_name, template_dash_name=template_dash_name, template_alert_folder_name=template_alert_folder_name, rule_group=rule_group, store_alerts=store_alerts, reload_template_from_file=reload_template_from_file)
        # Upload the alerts
        if upload_alerts:
            # See if excluding the above works, and if just the method for updating existing rule groups work, even for creating new ones:
            # For updating interval of a rule group:
            #self.renew_folder(folder_name)
            folder_name = machine_name
            folder_uid = self.folders[folder_name]
            if True:
                upload_json = rule_group
                #{folderrule_group  # Do not want interval 0, need to get old interval...
                #}
                if True or dash_name == 'Printer 2':
                    #r = self.session.put(f'{self.grafana_server}/api/v1/provisioning/folder/{machine_name}/rule-groups/{dash_name}', json=upload_json)
                    group_name = dash_name
                    self.delete_alerts(folder_name, group_name)
                    #input('[Delete] Press enter to continue...')
                    #r = self.session.put(f'{self.grafana_server}/api/v1/provisioning/folder/{folder_uid}/rule-groups/{group_name}', json=upload_json)
                    #prev_rules = upload_json['rules']
                    #upload_json['rules'] = [prev_rules[0]]
                    folder_name = upload_json['folder']
                    del upload_json['folder']
                    #upload_json['interval'] = 0
                    self.update_alerts(folder_name, upload_json)
                    if EXTRA_PRINTS:
                        print(json.dumps(upload_json, indent=2))
                        print(self.folders)
                    #input('[Post] Press enter to continue...')
            else:
                print('WOULD HAVE UPLOADED ALERTS!!!')
            # For creating new: (I could not find anything about that...)
        return rule_group

    def template_alerts(self, template_dash_name, template_alert_folder_name, reload_template_from_file=False):
        #will
        rule_group = self.rule_group_for_folder(template_alert_folder_name, template_dash_name)
        #print(rule_group)
        #exit()
        template_alert_exists = rule_group is not None
        if reload_template_from_file or not template_alert_exists:
            # Load from file instead
            rule_group = self.load_alert_from_file(template_alert_folder_name)
        return rule_group

    # target['query'] = self.query_machine_transplant(target['query'], machine_name)
    def alert_transplant(self, alert_folder_name, machine_name, dash_name, template_dash_name=None, template_alert_folder_name=None, rule_group=None, store_alerts=False, reload_template_from_file=False):
        
        if rule_group is None:
            rule_group = self.template_alerts(template_dash_name, template_alert_folder_name, reload_template_from_file=reload_template_from_file)
        else:
            rule_group = copy.deepcopy(rule_group)

        #alert = self.find_alert(template_alert_name, title=template_alert_name)
        data_uid = self.datas['InfluxDB']
        existing_rule_group = self.rule_group_for_folder(alert_folder_name, dash_name)
        if 'folderId' in rule_group:
            del rule_group['folderId']
        rule_group['folder'] = alert_folder_name  # machine_name
        #rule_group['folderUid'] = self.folders[machine_name]
        # Name of the rule group below:
        #rule_group['title'] = dash_name
        rule_group['name'] = dash_name
        alerts = rule_group['rules']
        for i, rule in enumerate(alerts):

            alert = rule['grafana_alert']
            
            if 'id' in alert:
                del alert['id']
            if 'orgId' in alert:
                del alert['orgId']
            if 'updated' in alert:
                del alert['updated']
            #alert['id'] = None  # i + 1

            '''
            this_uid = None
            if dash_name == 'Printer 2':
                print(f'Existing: {existing_rule_group}')
                # input('Press enter to continue...')
            if existing_rule_group is not None:
                for existing_rule in existing_rule_group['rules']:
                    existing_alert = existing_rule['grafana_alert']
                    if dash_name == 'Printer 2':
                        temp_existing_title = existing_alert['title']
                        temp_title = alert['title']
                        print(f'Title: {repr(temp_title)} Existing title: {repr(temp_existing_title)}')
                        # input('Press enter to continue...')
                    if existing_alert['title'] == alert['title']:
                        this_uid = existing_alert['uid']
                        break
            if this_uid is None:
                this_uid = str(uuid.uuid4())
            if dash_name == 'Printer 2':
                pass  # input('Press enter to finish...')
            '''

            alert['uid'] = None  # this_uid
            #alert['folderUID'] = self.folders[machine_name]
            #alert['ruleGroup'] = dash_name
            #alert['namespace_uid'] = self.folders[machine_name]
            if 'namespace_uid' in alert:
                del alert['namespace_uid']
            if 'namespace_id' in alert:
                del alert['namespace_id']
            if 'rule_group' in alert:
                del alert['rule_group']
            #alert['rule_group'] = dash_name
            for data_item in alert['data']:
                if data_item['datasourceUid'] == '__expr__':
                    continue
                data_item['datasourceUid'] = data_uid
                model = data_item['model']
                #print(model)
                if 'datasource' in model:
                    model['datasource']['uid'] = data_uid
                model['maxDataPoints'] = MAX_DATA_POINTS
                model['query'] = self.query_machine_transplant(model['query'], machine_name)
        if store_alerts:
            self.store_alert(alert_folder_name, rule_group=rule_group)
        return rule_group


    def update_dashboard(self, folder_name, dashboard):
        folder_uid = self.folders[folder_name]
        full_folder = self.full_folders[folder_name]
        print(full_folder, 'MI:', self.full_folders['Machine Info'])
        print('UPDATE FOLDER UID:', folder_uid, 'ID:', full_folder['id'])
        print('DASH UID:', dashboard['uid'])
        upload_json = {
            'dashboard': dashboard,
            'overwrite': True,
            #'folderUid': folder_uid,
            'folderId': full_folder['id'],
              'message': 'Uploaded by Three Amigos!'
        }
        r = self.session.post(f'{self.grafana_server}/api/dashboards/db', json=upload_json, verify=True)
        print(f'UPDATE DASHBOARD: {r.status_code}: {r.reason}')

    def renew_dashboard(self, mm, dash_name, machine_name=None, template_dash_name=None, template_dashboard=None, upload_dashboard=True, store_dashboard=False, reload_template_from_file=False):
        # Create the folder for alerts if it does not exist
        if machine_name is not None:
            self.ensure_folders(dash_name, machine_name)
        dashboard = self.dashboard_transplant(mm, dash_name, machine_name, template_dash_name=template_dash_name, template_dashboard=template_dashboard, store_dashboard=store_dashboard, reload_template_from_file=reload_template_from_file)
        # Upload the dashboard
        if upload_dashboard:
            
            # NOTE: THIS WHOLE FUNCTION IS NOT USED ANY MORE!
            folder_name = 'Machine Info'
            if dash_name == 'Test_Homepage':
                folder_name = 'Homepage'
            self.renew_folder(folder_name)
            folder_uid = self.folders[folder_name]

            print(f'Using: {repr(folder_name)} with uid {repr(folder_uid)} for {dash_name}...')

            if True:
                self.upload_dashboard(folder_name, dashboard)
                #input('Press enter to continue...')
                if dash_name == 'Printer 3':
                    #time.sleep(4)
                    exit()
            else:
                print('WOULD HAVE UPLOADED DASHBOARD!!!')
        return dashboard

    def load_alert_from_file(self, alert_folder_name):
        path = os.path.join(self.alert_directory, f'{alert_folder_name}.json')
        if not os.path.exists(path):
            raise AlertNotFound(f'Path: {path}')
        with open(path) as f:
            rule_group = json.loads(f.read())
        return rule_group

    def load_dashboard_from_file(template_dash_name):
        path = os.path.join(self.dashboard_directory, f'{template_dash_name}.json')
        if not os.path.exists(path):
            raise DashboardNotFound(f'Path: {path}')
        with open(path) as f:
            dashboard = json.loads(f.read())
        return dashboard
    
    def template_dashboard(self, template_dash_name, reload_template_from_file=False):
        uid = self.dashboards.get(template_dash_name)
        template_dashboard_exists = uid is not None
        if reload_template_from_file or not template_dashboard_exists:
            # Load from file instead
            return self.load_dashboard_from_file(template_dash_name)
        return self.dashboard_json(uid)

    # Use the new amigos template files
    def full_transplant(self, mm, amigos_template, machine, general_info=None, data_source='InfluxDB'):
        dash_name = machine['dash_name']
        machine_name = machine['name']
        dash_uid = machine['dash_uid']

        template_dash_name = amigos_template['dash_name']
        template_machine_name = amigos_template['machine_name']
        template_alert_folder_name = template_machine_name

        amigos_template = copy.deepcopy(amigos_template)

        template_rule_group = amigos_template['rule_group']
        template_dashboard = amigos_template['dashboard']
        # print("1. SJGKFJGDSAKFJGAKDSFJG", type(template_dashboard))
        #rule_group = self.renew_alert(dash_name, machine_name, rule_group=template_rule_group, upload_alerts=UPLOAD_THINGS, store_alerts=store_things)  # TODO: REPLACE!
        alert_folder_name = machine_name
        dashboard = self.dashboard_transplant(mm, dash_name, machine_name, general_info=general_info, template_dash_name=template_dash_name, template_dashboard=template_dashboard, dashboard_uid=dash_uid, store_dashboard=False, reload_template_from_file=False)
        amigos_template['dashboard'] = dashboard

        if machine_name is not None:
            rule_group = self.alert_transplant(alert_folder_name, machine_name, dash_name, template_dash_name=template_dash_name, template_alert_folder_name=template_alert_folder_name, rule_group=template_rule_group, store_alerts=False, reload_template_from_file=False)
            amigos_template['rule_group'] = rule_group

        return amigos_template

    # This should work for more than just the printers...
    def dashboard_transplant(self, mm, dash_name, machine_name, general_info=None, template_dash_name=None, template_machine_name=None, template_dashboard=None, dashboard_uid=None, store_dashboard=False, reload_template_from_file=False, data_source='InfluxDB'):

        # TODO: Combine this and alert_transplant and use the template alert/dashboard json format that is new...
    
        # Remove template_dash_name from an input arguemnt here? No: When machine_name is None for a general dashboard, it is still needed!
        
        if machine_name is not None:
            machine = mm.get_machine(machine_name)
            machine_type = machine['type']
            # TODO: mm is only supposed to be used when mysterious forces align
            template_machine_name, template_dash_name = mm.machine_templates[machine_type]
    
        data_uid = self.datas[data_source]
        data = template_dashboard
        if data is None:
            data = self.template_dashboard(template_dash_name, reload_template_from_file=reload_template_from_file)
        else:
            data = copy.deepcopy(data)
        if dashboard_uid is not None:
            this_uid = dashboard_uid
        else:
            this_uid = self.dashboards.get(dash_name)
            if this_uid is None:
                this_uid = self.new_uid_dashboard()
        data['title'] = dash_name
        data['id'] = None
        #cloud_uid = data['uid']
        data['uid'] = this_uid
        is_homepage = machine_name is None  # dash_name == self.home_dashboard_name

        if EXTRA_PRINTS:
            print(list(data))
        for panel in data['panels']:

            if not is_homepage:
                if panel['type'] == 'dashlist':
                    machine_config = machine.get('config', {})
                    folder_name = machine_config.get('home_folder', 'Homepage')
                    self.dashboard_list_rule(self, mm, panel, None, folder=folder_name)
                if panel['type'] == 'alertlist':
                    folder = panel['options']['folder']
                    folder['title'] = machine_name
                    # Hmm......... Need to list alert folders, create a new one if necessary... Then grab this uid...
                    #folders = self.list_folders()
                    self.renew_folder(machine_name)
                    folder['uid'] = self.folders[machine_name]

                    #self.alerts
                
                # TODO: Make the below a callback in a machine specific python module file?
                if panel['type'] == 'timeseries':
                    for override in panel['fieldConfig']['overrides']:
                        if override['__systemRef'] == 'hideSeriesFrom':
                            matcher = override['matcher']['options']
                            if EXTRA_PRINTS:
                                print(matcher)
                            matcher['names'] = [name.replace(template_machine_name, machine_name) for name in matcher['names']]

                # Update video streams
                if machine_name == 'Printer 003':
                    print('Look:', panel['type'])
                if panel['type'] == 'text':
                    options = panel['options']
                    content = options['content']
                    mode = options['mode']
                    if machine_name == 'Printer 003':
                        print('Mode:', mode)
                    if mode == 'html':
                        soup = BeautifulSoup(content, 'html.parser')
                        if machine_name == 'Printer 003':
                            print('Right mode!', 'img' in soup, 'script' in soup)
                            #print(soup.get('img'), soup.get('script'))
                            #print(soup.img, soup.script)
                            #print(soup.find('img') is not None, soup.find('script') is not None)
                        if soup.find('img') is not None and soup.find('script') is not None:
                            img_id = soup.img.get('id')
                            img_link = soup.img.get('src')
                            if machine_name == 'Printer 003':
                                print(f'Right soup, img_id={repr(img_id)} and img_link={repr(img_link)}')
                            if img_id is not None and img_link is not None:
                                if machine_name == 'Printer 003':
                                    print('Made it to replacements!')
                                parts = img_link.split('/')
                                cam_type = parts[-1]
                                pi_name = mm.pi_name_for_machine(machine_name)
                                content = content.replace(img_link, f'{CAMERA_URL}/{pi_name}/{machine_name}/{cam_type}')
                                content = content.replace(img_id, f'{cam_type}_{machine_name}_{pi_name}'.replace(' ', '_'))
                                options['content'] = content
                #if machine_name == 'Printer 003':
                #    input('Press enter to continue...')

            panel['datasource']['uid'] = data_uid
            panel['maxDataPoints'] = MAX_DATA_POINTS

            #if not is_homepage:
            for target in panel.get('targets', []):
                # machine_name=None for when not is_homepage
                if EXTRA_PRINTS:
                    print(target)
                if target['datasource']['type'] == 'influxdb':
                    if EXTRA_PRINTS:
                        print('Before:', )
                        print('Before:', target['query'])
                    target['query'] = self.query_machine_transplant(target['query'], machine_name)
                    if EXTRA_PRINTS:
                        print('After:', target['query'])
                    target['datasource']['uid'] = data_uid
                    #if machine_name == 'Printer 003':
                    #    input('Press enter to continue...')
                #target['refId'] = 'Printer 1'  # Do not change.
                # https://grafana.com/docs/grafana/latest/panels-visualizations/query-transform-data/expression-queries/
                # - it is the name of this query expression so they can be chained together. It does not need to be changed, it would be difficult to change it all consistently!
                #target['datasource']['uid'] = 'd4b4454d-6066-48d9-b249-9945d114c190'
                # I do not think the above is necessary to change, since the above changed datasource uid, which is different, has seemed to suffice without problems
        # TODO: Apply these modifiers outside this function, so mm is not required here!
        if is_homepage:
            if general_info is None:
                print('GENERAL INFO WOULD BE UESFUL FOR:', dash_name)
            else:
                general_type = general_info['type']
                # Run the dashboard modifier scripts:
                for modifier in self.dashboard_modifiers:
                    if modifier.general_type != general_type:
                        print(f'FOR {dash_name}, SKIPPED MODIFIER!!!')
                        continue
                    #data = modifier.modify_home_dashboard(data, self, mm)
                    print(f'Modifying {dash_name} of {general_type}!')
                    modifier.modify_home_dashboard(data, self, mm, {} if general_info is None else general_info)
        if store_dashboard:
            self.store_dashboard(dash_name, dashboard=data)
        return data#, cloud_uid


    def panel_replacements(self, mm, dashboard, panel_replace_rules, machines, general_info):
        for panel in dashboard['panels']:
            panel_type = panel['type']
            print(panel)
            print(f'Panel {panel_type}:')
            #input('Press enter to continue...')
            #panel_title = panel['title']
            applicable = False
            for the_rule in panel_replace_rules:
                conditions, rule = the_rule[:2]
                extra_args = {}
                if len(the_rule) > 2:
                    extra_args = the_rule[2]
                passed_conditions = True
                for condition in conditions:
                    parts = condition.split('/')
                    value = parts.pop(-1)
                    found = True
                    track = panel
                    for part in parts:
                        if part not in track:
                            found = False
                            break
                        track = track[part]
                    if not found:
                        if value != '':
                            passed_conditions = False
                            break
                        track = ''
                        found = True
                    if track != value:
                        passed_conditions = False
                        break
                print(f'Condition Check: {conditions}')
                print(f'Evaluation: {passed_conditions}')
                #input('Press enter to continue...')
                if passed_conditions:
                    applicable = True
                    break
            if not applicable:
                print(f'No rule applicable!')
                #input('Press enter to continue...')
                continue
            '''
            possible_titles = panel_query_replace_rules.get(panel_type)
            if possible_titles is None:
                continue
            rule = possible_titles.get(panel_title)
            if rule is None:
                continue
            '''
            if EXTRA_PRINTS:
                print(f'Rule being applied!')
            rule(self, mm, panel, machines, **extra_args)
            if EXTRA_PRINTS:
                print('After:')
                print(panel)
            #input('Press enter to continue...')
            #targets = panel['targets']
            #panel['targets'] = rule(self, mm, panel, machines)

    def influxdb_target(self, query, ref_id='A', hide=False):
        target = {
                    'datasource': {
                        'type': 'influxdb',
                        'uid': self.data_uid
                    },
                    'hide': hide,
                    'query': query,
                    'refId': ref_id
                }
        return target

    def expression_target(self, expression, ref_id='B', hide=False):
        target = {
                    'datasource': {
                        'name': 'Expression',
                        'type': '__expr__',
                        'uid': '__expr__'
                    },
                    'expression': expression,
                    'hide': hide,
                    'refId': ref_id,
                    'type': 'math'
                }
        return target

    def sum_for_machines_rule(self, md, mm, panel, machines):
        targets = panel['targets']
        # self and md are the same in this case...
        target_template = targets[0]
        query_template = target_template['query']
        new_targets = []
        for i, machine in enumerate(machines):
            machine_name = machine['name']
            query = md.query_machine_transplant(query_template, machine_name)
            target = md.influxdb_target(query, ref_id=f'summand_{i}', hide=True)
            new_targets.append(target)
        prev_expression_target = targets[-1]
        ref_id = prev_expression_target['refId']
        expression = ' + '.join([f'$summand_{i}' for i in range(len(machines))])
        target = md.expression_target(expression, ref_id=ref_id)
        new_targets.append(target)
        panel['targets'] = new_targets

    def dashboard_list_rule(self, md, mm, panel, machines, **extra_args):
        folder_name = extra_args.get('folder', 'Machine Info')
        self.renew_folder(folder_name)
        # print(self.full_folders)
        # print(self.folders)
        folder = self.full_folders[folder_name]
        panel['options']['folderId'] = folder['id']

    def alert_group_folder_name_from_dashboard_name(self, dash_name, dashboard=None):
        if dashboard is None:
            dashboard = self.dashboard_json(self.dashboards[dash_name])
        folder_name = None
        for panel in dashboard['panels']:
            if panel['type'] == 'alertlist':
                folder_name = panel['options']['folder']['title']
                break
        if folder_name is None:
            raise AlertNotFound(f'Could not find alert list for dashboard {dash_name}')
        return folder_name

    # Can include the home dashboard
    def store_dashboard(self, dash_name, dashboard=None, dashboard_directory=None):
        if dashboard_directory is None:
            dashboard_directory = self.dashboard_directory
        out_path = os.path.join(dashboard_directory, f'{dash_name}.json')
        if dashboard is None:
            dashboard = self.dashboard_json(self.dashboards[dash_name])
        with open(out_path, 'w') as f:
            f.write(json.dumps(dashboard, indent=4))
            print(f'Wrote {out_path}')

    # Do not include the home dashboard!
    def store_alert(self, folder_name, rule_group=None, alert_directory=None):  # dash_name):
        if alert_directory is None:
            alert_directory = self.alert_directory
        out_path = os.path.join(alert_directory, f'{folder_name}.json')
        #folder_name = self.alert_group_folder_name_from_dashboard_name(dash_name)
        if rule_group is None:
            rule_group = self.rule_group_for_folder(folder_name, dash_name)
        with open(out_path, 'w') as f:
            f.write(json.dumps(rule_group, indent=4))
            print(f'Wrote {out_path}')

def replace_query_param(query, name, str_value):
    split_at = f'{name}"'
    parts = query.split(split_at)
    for i in range(1, len(parts)):
        part = parts[i]
        # split_at + 
        parts[i] = f'{str_value}"' + part.split('"', 1)[1]
    return split_at.join(parts)


#       Investigate why alert uid changes each time for gen_alert/Printer 003.json
#       And why uid of new printer 3 dashboard changes each time.
#       FIGURED IT OUT: The dashboards are not uploaded, so the uids are not found in grafana already...

#  2. Test running the pi manager at home, including managing dashboards by using a "home.ini" config file or something... (changes whether to use localhost or iotcapstone.space, etc... and whether to generate dummy data, etc)
#  7. Move image streaming wrangler code into pi backend manager. (cannot test this part at home, or at least cannot make it available by the iotcapstone domain...)
#  8. Move specific image grabbing code into the machine module.
#  9. Move image streaming wrangler code for images of finished prints into the machine module.
# 10. Move pi_data_collection into the machine module, and have the machine module uploaded to the pi and run the appropriate functions (specify which functions should run as separate screen processes).
# 11. Either specify pi requirements.txt for each machine module code that runs on the pi, or use pipimport.

# TODO:
# 12. Add the ability to manage a list of template machines and dashboards in pi_manager.
# 13. Add pi_manager list for managing home/general pages in grafana - duplicate, remove, edit, add, etc...
# 14. pi_manager use a cached requests session...
# 15. Add button to propage template changes for dashboards in the lists for managing those in pi_manager. (use tabs to see these lists? machine dashbaords, general dashboards, and machines)
# 16. Store templates in a templates folder in grafana and another templates folder for home/general dashboards. (need template alert rule groups too under a template folder...)
#     (automatically use first machine of a new type seen as template, and save it,  (later:) though change the data sources to use the dummy data source)
#     (Do the same for new alert names, store in the pi_manager_backend database under which machine types each type of alert should go, later make this managable with pi_manager)
# 17. Pull from these template dashboard locations with names the same as the template names.

# 18. Write machine specific module functions to produce dummy data.
# 19. Run those in backend manager.
# 20. Make image streaming wrangler only retry for found machine_ips, since it currently remembers old machines that are now off.
# 21. Use custom socket as opposed to requests for getting mjpeg streams in wrangler. Can also write custom version
#     of livestream that sends images even before the complete image is received...

# TODO: (low priority) Make the point name depend on the machine type for dashboards...

# TODO: Main API functions callable from fastapi (and command line here):
#       new_from_template, delete, new_template_from_template, reload_template, reload_all, ...

# Simple test of processed output for a new printer without uploading, check for homepage and new printer page...


if __name__ == '__main__':

    print('TODO: Modify main code for testing purposes, too much has changed for the below code to just work...')

    exit(1)

    parser = argparse.ArgumentParser(prog=sys.argv[0], description='Dynamically generate dashboards for monitoring workshop equipment.')
    parser.add_argument('-b', '--bucket', help='choose bucket name for influxdb queries', default='Test')
    parser.add_argument('-p', '--point-name', dest='measurement_name', help='choose measurement name for influxdb queries', default='send efficient 2')
    parser.add_argument('-d', '--home_dashboard', dest='home_dashboard', help='Name of the home dashboard', default='Test_Homepage')
    parser.add_argument('--reload-templates', dest='reload_templates', action='store_true')
    parser.add_argument('--no-reload-templates', dest='reload_templates', action='store_false')
    parser.set_defaults(reload_templates=False)

    args = parser.parse_args()

    print(parser)
    #home_dashboard_name = args.home_dashboard
    bucket = args.bucket
    measurement_name = args.measurement_name

    reload_template_from_file = args.reload_templates
    print('Reload template from file:', reload_template_from_file)

    if reload_template_from_file:
        print('Will reload templates from files!')


    # Info about which are templates... {machine_type: machine name serving as template (extract template...)}
    machine_templates = {'Printer': ('Printer 001', 'Printer 1')}

    machines = [
        {'name': 'Printer 001', 'dash_name': 'Printer 1', 'type': 'Printer'},
        {'name': 'Printer 002', 'dash_name': 'Printer 2', 'type': 'Printer'},
        #{'name': 'Printer 003', 'dash_name': 'Printer 3', 'type': 'Printer'},
    ]

    md = MachineDashboards(bucket=bucket, measurement_name=measurement_name)
    mm = MachineManager(machines, machine_templates)

    GEN_DASH_DIRECTORY = 'gen_dash'
    GEN_ALERT_DIRECTORY = 'gen_alert'

    for path in [GEN_DASH_DIRECTORY, GEN_ALERT_DIRECTORY]:
        ensure_path(path)

    home_dash_name = 'Test_Homepage'
    template_things = [('Homepage', None, home_dash_name)]

    # Store all templates in a dict
    used_templates = []
    for machine in mm.machines:
        machine_name = machine['name']
        dash_name = machine['dash_name']
        machine_type = machine['type']
        if machine_type in used_templates:
            continue
        template_machine_name, template_dash_name = mm.machine_templates[machine_type]

        template_things.append((machine_type, template_machine_name, template_dash_name))

        used_templates.append(machine_type)

    dashboards_and_alerts = {}
    for machine_type, template_machine_name, template_dash_name in template_things:

        dashboard = md.template_dashboard(template_dash_name, reload_template_from_file=reload_template_from_file)
        md.store_dashboard(template_dash_name, dashboard=dashboard)

        rule_group = None
        print('Name:', machine_type, template_machine_name)
        if template_machine_name is not None:
            rule_group = md.template_alerts(template_dash_name, template_machine_name, reload_template_from_file=reload_template_from_file)
            md.store_alert(template_machine_name, rule_group=rule_group)

        dashboards_and_alerts[machine_type] = (dashboard, rule_group)

    UPLOAD_THINGS = True  # False

    # Handle homepage:
    print('Dealing with homepage...')
    template_dashboard, _ = dashboards_and_alerts['Homepage']
    dashboard = md.renew_dashboard(mm, home_dash_name, template_dashboard=template_dashboard, upload_dashboard=UPLOAD_THINGS, store_dashboard=False)
    md.store_dashboard(home_dash_name, dashboard=dashboard, dashboard_directory=GEN_DASH_DIRECTORY)

    # Loop through all machines and use the stored templates to produce new dashboards. Do not update or store.
    for machine in machines:
        machine_name = machine['name']
        print('Name:', machine_name)
        machine_type = machine['type']
        dash_name = machine['dash_name']
        template_dashboard, template_rule_group = dashboards_and_alerts[machine_type]
        if template_dashboard is None:
            print('TD None!')
            exit()
        if template_rule_group is None:
            print('TRG None!')
            exit()
        # template_alert_folder_name is None, though should not be...
        rule_group = md.renew_alert(dash_name, machine_name, rule_group=template_rule_group, upload_alerts=UPLOAD_THINGS, store_alerts=False)
        dashboard = md.renew_dashboard(mm, dash_name, machine_name, template_dashboard=template_dashboard, upload_dashboard=UPLOAD_THINGS, store_dashboard=False)

        # Write rule_group and dashboard to a folder for viewing:
        md.store_alert(machine_name, rule_group=rule_group, alert_directory=GEN_ALERT_DIRECTORY)
        md.store_dashboard(dash_name, dashboard=dashboard, dashboard_directory=GEN_DASH_DIRECTORY)

