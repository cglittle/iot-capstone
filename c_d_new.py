import numpy as np
import numpy.linalg as la
import cv2 as cv
from matplotlib import pyplot as plt
from skimage import measure
import time
from threading import Thread, Semaphore
import configparser
import os
import os.path
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime


print_sem = Semaphore(1)

old_print = print
def print(*args, **kw):
    print_sem.acquire()
    old_print(*args, **kw)
    print_sem.release()


CONFIG_FILE = 'iot4config.ini'

config = configparser.ConfigParser()
if os.path.exists(CONFIG_FILE):
    config.read(CONFIG_FILE)

printer_info = configparser.ConfigParser()
printer_info.read('printer_info.ini')
printer_name = printer_info['default']['printer_name']
print(printer_name)


USE_GUI = False

influxdb_instances = {}
#influxdb_infos = []
#influxdb_options
print(list(config))
for name in list(config):
    subvals = config[name]
    if name == 'DEFAULT':
        continue
    instance = config[name]
    influxdb_instances[name] = instance
    use_info = instance
    bucket = use_info['bucket']
    org = use_info['org']
    token = use_info['token']
    url = use_info['url']
    print('InfluxDB:', name)
    print('Bucket:', bucket)
    print('Org:', org)
    print('Token:', token)
    print('URL:', url)



if not influxdb_instances:
    raise Exception('No influxdb database listed!')
    
    
message_queue = []
def write_influx(record):
    
    #current_time = datetime.utcnow().timestamp() * 1000
    #current_time = int(current_time)
    current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
    #.strftime('%Y-%m-%dT%H:%M:%SZ')
    #current_time = current_time.encode('utf8')
    print(repr(current_time))
    #record = record.field('time', current_time)
    record = record.time(current_time)
    print(record)
    message_queue.append(record)
    
    
def influxdb_writer():
    while True:
        while not message_queue:
            time.sleep(0.01)
        record = message_queue.pop(0)           
        for name, use_info in influxdb_instances.items():

            enabled = use_info['enabled']

            print(name, 'enabled:', repr(enabled))

            if not enabled:
                continue

            bucket = use_info['bucket']
            org = use_info['org']
            token = use_info['token']
            url = use_info['url']



            print('ACQUIRE:', record)
            #influx_sem.acquire()
            try:
                client = influxdb_client.InfluxDBClient(
                    url=url,
                    token=token,
                    org=org
                )
                write_api = client.write_api(write_options=SYNCHRONOUS)
                write_api.write(bucket=bucket, record=record)
            except:
                print(traceback.format_exc())
                message_queue.insert(0, record)
                print(message_queue)
                time.sleep(5)
                
            finally:
                print('RELEASE')
                #influx_sem.release()


t = Thread(target=influxdb_writer, args=(), daemon=True)
t.start()


#cap = cv.VideoCapture("http://192.168.0.66:8000/stream.mjpg")
cap = cv.VideoCapture("http://localhost:8000/stream.mjpg")
cap.set(3, 640)
cap.set(4, 420)
cap.set(cv.CAP_PROP_BUFFERSIZE, 0)
cap.set(cv.CAP_PROP_FPS, 2)

sem = Semaphore(1)
lastest_image = None

def get_images():
    global latest_image
    while True:
        success, img = cap.read()
        sem.acquire()
        latest_image = img
        sem.release()

def grab_an_image():
    global latest_image
    sem.acquire()
    latest_image = None
    sem.release()
    while latest_image is None:
        time.sleep(0.001)
    return latest_image

t = Thread(target=get_images, daemon=True)
t.start()

REF_LINE_THRESH = 1  # These are numbers out of 255
TOP_CUTOFF = 285  # From top of the frame
BOTTOM_CUTOFF = 40  # From ref line up
INTENSITY_THRESHOLD = 100  # The threshold on the difference picture to determine whether a pixel is different
PIXEL_GROUP_NUM_THRESH = 500  # Threshold for number of pixels that have to be different before the print is considered to have fallen off
def picture_preview(name):
    img = grab_an_image()
    '''
    prev_img = None
    while True:
        print("Hi")
        success, img = cap.read()
        if np.array_equal(img, prev_img):
            break
        prev_img = img

    img = prev_img
    '''
    if USE_GUI:
        cv.imshow('test', img)
    name = 'pic.jpg'
    cv.imwrite(name, img)
    print("taken!")
    return img, cv.imread(name, cv.IMREAD_GRAYSCALE)



def comparison(img1, img2):
    MIN_MATCH_COUNT = 10
    #img1 = cv.imread('object.jpg', cv.IMREAD_GRAYSCALE) # queryImage
    #img2 = cv.imread('scene.jpg', cv.IMREAD_GRAYSCALE) # trainImage
    # Initiate SIFT detector
    sift = cv.SIFT_create()
    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)
    #print(kp1)
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv.FlannBasedMatcher(index_params, search_params)
    if des1 is None or des2 is None or len(des1) == 0 or len(des2) == 0:
        return 0
    matches = flann.knnMatch(des1, des2, k=2)
    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)
    #print(good)
    above_thresh_sum = 0
    count = 0
    for match in good:
        p1 = kp1[match.queryIdx].pt
        p2 = kp2[match.trainIdx].pt
        val = ((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)**0.5
        if val >= 1:
            above_thresh_sum += val
            count += 1
        print()
    av_move = above_thresh_sum/count
    print('Average move:', av_move)

    if len(good)>MIN_MATCH_COUNT:
         src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
         dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
         M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
         matchesMask = mask.ravel().tolist()
         h,w = img1.shape
         pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
         dst = cv.perspectiveTransform(pts,M)
         img2 = cv.polylines(img2,[np.int32(dst)],True,255,3, cv.LINE_AA)
    else:
        print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
        matchesMask = None

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
        singlePointColor = None,
        matchesMask = matchesMask, # draw only inliers
        flags = 2)
    img3 = cv.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)
    if USE_GUI:
        plt.imshow(img3, 'gray')
        plt.show(block=False)
        plt.pause(1)
        plt.close()
        #return(len(good))

def comparison_correct(img1, img2):
    size = min(np.shape(img1)[0], np.shape(img2)[0])
    img1 = img1[-size:-1, :]
    img2 = img2[-size:-1, :]
    if USE_GUI:
        plt.imshow(img1, 'gray')
        plt.show(block=False)
        plt.pause(2)
        plt.imshow(img2, 'gray')
        plt.show(block=False)
        plt.pause(2)

    diff = img1.astype(np.int16) - img2.astype(np.int16)
    print(np.min(diff), np.max(diff), diff)
    diff_show = np.abs(diff)
    diff_thresh = cv.threshold(diff_show, INTENSITY_THRESHOLD, 255, cv.THRESH_BINARY)[1]
    diff_thresh = cv.erode(diff_thresh, None, iterations=2)
    diff_thresh = cv.dilate(diff_thresh, None, iterations=4)
    labels = measure.label(diff_thresh, background=0)
    print(np.bincount(np.ndarray.flatten(labels)))
    if USE_GUI:
        plt.imshow(diff_show, 'gray')
        plt.show(block=False)
        plt.pause(1)
        plt.close()
        plt.imshow(diff_thresh, 'gray')
        plt.show(block=False)
        plt.pause(1)
        plt.close()
    #diff = np.array([[1, 2, 3], [4, 5, 6]])
    avg = la.norm(diff, axis = 1)
    #print(avg.shape)
    if len(np.bincount(np.ndarray.flatten(labels))) > 1:
        return max(np.bincount(np.ndarray.flatten(labels))[1:])
    return 0


while True:
    img1_colour, img1 = picture_preview("current0.jpg")
    j = 1
    img1_colour = img1_colour[TOP_CUTOFF: , :, :]
    img1 = img1[TOP_CUTOFF: , :]
    img_height = img1_colour.shape[0]
    line_pixel = None
    index = None
    red_avg = None
    for i in range(img_height-1, -1, -1):
        red_avg = np.mean(img1_colour[i, :, 2]) - np.mean(img1_colour[i, :, 0])/2 - np.mean(img1_colour[i, :, 1])/2
        if red_avg > REF_LINE_THRESH:
            index = i
            line_pixel = red_avg
            break
    if index is None:
        print("Cant find reference line! Trying again...")
        time.sleep(5)
        #exit(0)
        continue
    img1_colour = img1_colour[0:index -BOTTOM_CUTOFF, :]
    img1 = img1[0:index - BOTTOM_CUTOFF, :]
    break

while True:
    img2_colour, img2 = picture_preview(f"current{j}.jpg")
    img2_colour = img2_colour[TOP_CUTOFF: , :, :]
    img2 = img2[TOP_CUTOFF: , :]
    img_height = img2_colour.shape[0]
    line_pixel = None
    index = None
    for i in range(img_height-1, -1, -1):
        red_avg = np.mean(img2_colour[i, :, 2]) - np.mean(img2_colour[i, :, 0])/2 - np.mean(img2_colour[i, :, 1])/2
        if red_avg > REF_LINE_THRESH:
            index = i
            line_pixel = red_avg
            break
    #print(line_pixel)
    if index is not None:
        if index - BOTTOM_CUTOFF > 0:
            img2_colour = img2_colour[0:index - BOTTOM_CUTOFF, :]
            img2 = img2[0:index - BOTTOM_CUTOFF, :]
            #plt.imshow(img1_colour)
            #plt.show(block=False)
            #plt.pause(10)
            #plt.close()
            '''plt.imshow(img1_colour)
            plt.show(block=False)
            plt.pause(2)
            plt.close()
            plt.imshow(img2_colour)
            plt.show(block=False)
            plt.pause(2)
            plt.close()'''
            indicator = comparison_correct(img1, img2)
            print(indicator)
            p = influxdb_client.Point("send efficient 2") \
                .tag("Name", printer_name) \
                .field("Fall", 0) 
            if indicator > PIXEL_GROUP_NUM_THRESH:
                print("it fell off!")
                p = influxdb_client.Point("send efficient 2") \
                .tag("Name", printer_name) \
                .field("Fall", 1) 
            write_influx(p)
            img1_colour, img1 = img2_colour, img2
            j += 1
    else:
        print("Reference line lost")
    t = time.time()
    while time.time() - t < 30:
        key = cv.waitKey(10) & 0xFF
