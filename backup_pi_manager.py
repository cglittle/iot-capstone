
#from st_keyup import st_keyup
#from streamlit_javascript import st_javascript


# $ python3 -m pip install st-click-detector

# Cool list of streamlit examples: https://github.com/jrieke/best-of-streamlit
#https://components.streamlit.app/

# TODO: See how this affects the appearance of the login, and change the layout to 'centered' if necessary for just the login page



# https://github.com/kmcgrady/streamlit-autorefresh/blob/main/streamlit_autorefresh/__init__.py

# count = st_autorefresh(interval=100, key='fizzbuzzcounter')  # limit=100, 
# 
# # The function returns a counter for number of refreshes. This allows the
# # ability to make special requests at different intervals based on the count
# if count == 0:
#     st.write("Count is zero")
# elif count % 3 == 0 and count % 5 == 0:
#     st.write("FizzBuzz")
# elif count % 3 == 0:
#     st.write("Fizz")
# elif count % 5 == 0:
#     st.write("Buzz")
# else:
#     st.write(f"Count: {count}")
# 
# exit()


# $ python3 -m pip install streamlit-autorefresh

# Reloading a streamlit page: https://docs.streamlit.io/library/api-reference/control-flow

# List of streamlit components: https://streamlit.io/components

# For live text_input update: https://github.com/blackary/streamlit-keyup
# (necessary for autocomplete to work in browser, otherwise need to press enter in each field before the value is seen in streamlit...)

# https://docs.streamlit.io/library/api-reference/text
# - Library has examples with thumbnails!

# https://stackoverflow.com/questions/72543675/is-there-a-way-to-remove-the-side-navigation-bar-containing-file-names-in-the-la
#st.markdown("<style> #MainMenu {display: none;} </style>", unsafe_allow_html=True)



#st.set_page_config(initial_sidebar_state="collapsed")


#@st.cache_data
@st.cache_resource
def get_user_table(fields, users):
    user_table = {}
    for field_name in fields:
        thing = []
        if field_name == 'N':
            thing = list(range(len(users)))
        elif field_name == 'email':
            thing = [''.join(random.choices(string.ascii_lowercase, k=5))+'@gmail.com' for i in range(len(users))]
        elif field_name == 'uid':
            thing = [random.randint(0, 100) for i in range(len(users))]
        elif field_name == 'verified':
            thing = [bool(random.randint(0, 1)) for i in range(len(users))]
        #elif field_name == 'act
        user_table[field_name] = thing
    for field_name in ['disabled']:
        if field_name == 'disabled':
            thing = [bool(random.randint(0, 1)) for i in range(len(users))]
        user_table[field_name] = thing
    return user_table

def main():

    # Reload frequently, read from an api the state, tell the api which buttons the user pressed, the api handles everything..., a new button requires new code on both that api and here in streamlit
    # TODO: Add filter with live update (not sent to api, the selection for this state is when appropriate), add + and - buttons, add select boxes, add buttons to select listed, deselect listed, invert select listed, last row repeats button options where possible and acts on all selected, button actions are sent to api along with the selected to apply to

    SEC = 1000
    count = st_autorefresh(interval=1*SEC, key='reloader')  # limit=100, 

    count

    st.markdown('''\
    # Header
    thing...

    - one
    - two
    - three

    hi
    ''')

    users = ['user1', 'user2']

    #with st.expander('Machines', expanded=True):#, label_visibility='collapsed'):
    st.text('Machines')
    if True:
        # https://docs.streamlit.io/library/api-reference/layout
        colms = st.columns((1, 2, 2, 1, 1))
        fields = ['N', 'email', 'uid', 'verified', 'action']
        for col, field_name in zip(colms, fields):
            # header
            col.write(field_name)

        user_table = get_user_table(fields, users)
        # dsahboard_generator, printer list, edit opiton inline
        # clickable text, select all text option, highlight row, auto clear on add for form
        for x, email in enumerate(user_table['email']):
            col1, col2, col3, col4, col5 = st.columns((1, 2, 2, 1, 1))
            col1.write(x)  # index
            col2.write(user_table['email'][x])  # email
            col3.write(user_table['uid'][x])  # unique ID
            #col4.write(user_table['verified'][x])   # email status
            #col4.write(user_table['verified'][x])   # email status
            col4.checkbox(' ', value=user_table['verified'][x], label_visibility='collapsed', key=f'{x}_checkbox')
            disable_status = user_table['disabled'][x]  # flexible type of button
            button_type = 'Unblock' if disable_status else 'Block'
            button_phold = col5.empty()  # create a placeholder
            def click_button(x, disable_status):
                user_table['disabled'][x] = not disable_status
                #time.sleep(5)
            do_action = button_phold.button(button_type, key=x, on_click=click_button, args=[x, disable_status])
            # if do_action:
            #     user_table['disabled'][x] = not disable_status
            #     pass # do some action with row's data
            #     #button_phold.empty()  #  remove button

    with st.expander('Add new machine'):
        #with st.form('new_machine'):
        # TODO: Make it difficult to remove items... (and impossible for templates?)
        col1, col2, col3, col4, col5 = st.columns((1, 2, 2, 1, 1))
        col1.text_input(' ', key='N', placeholder='N', label_visibility='collapsed')  # 'hidden' is worse than 'collapsed', since it has white space above!
        col2.text_input(' ', key='email', placeholder='email@gmail.com', label_visibility='collapsed')
        col3.text_input('hi', key='uid', placeholder='uid', label_visibility='collapsed')
        col4.checkbox(' ', key='verified', label_visibility='collapsed')
        #col5.text_input('', key)
        #col5.empty()
        #st.form_submit_button('Add', on_click=None)
        st.button('Add', on_click=None)

    # columns options: https://docs.streamlit.io/library/api-reference/data/st.column_config
    # https://docs.streamlit.io/library/api-reference/data/st.data_editor
    df = pd.DataFrame(
        [
           {"command": "st.selectbox", "rating": 4, "is_widget": True},
           {"command": "st.balloons", "rating": 5, "is_widget": False},
           {"command": "st.time_input", "rating": 3, "is_widget": True},
       ]
    )
    edited_df = st.data_editor(df, num_rows='dynamic', use_container_width=True, on_change=None, disabled=('col0',))#,
    #                            column_config={})   # https://blog.streamlit.io/introducing-column-config/

    favorite_command = edited_df.loc[edited_df["rating"].idxmax()]["command"]
    st.write(favorite_command)

AUTO_AUTHENTICATE = True

if __name__ == '__main__':

    if 'logged_in' not in st.session_state:
        st.session_state.logged_in = AUTO_AUTHENTICATE
    if 'failed_login' not in st.session_state:
        st.session_state.failed_login = False

    # if 'attempt_login' not in st.session_state:
    #     st.session_state.attempt_login = False

    # if st.session_state.attempt_login:
    #     st.session_state.attempt_login = False

    #     username = st.session_state['username']
    #     password = st.session_state['password']

    #     if 

    #     st.session_state.logged_in = True

    #st.text(f'Logged in? {st.session_state.logged_in}')

    if st.session_state.logged_in:
        main()
    else:
        # Exact issue: https://github.com/streamlit/streamlit/issues/7101
        # 
        #with st.sidebar:
        if 'username' in st.session_state and 'password' in st.session_state:
            print('BEFORE:', repr(st.session_state['username']), repr(st.session_state['password']))
        with st.form('login_form'):
            st.subheader('Login')
            username = st.text_input('Username', key='username', autocomplete='username', value='admin')
            password = st.text_input('Password', key='password', autocomplete='new-password', type='password')

            # Auto focus the password field:
            # ('''\
            # <script type='text/javascript'>
            #     console.log("Hi!");
            #     var the_password_input = document.querySelectorAll('[autocomplete="new-password"]')[0];
            #     the_password_input.focus();
            #     the_password_input.select();
            #     //alert('Done!');
            #     console.log("Hello!");
            # </script>
            # ''')

            #username = st_keyup('Username', key='username')
            #password = st_keyup('Password', key='password', type='password')
            # Problem: https://docs.streamlit.io/knowledge-base/using-streamlit/widget-updating-session-state, 
            def do_login():
                #st.session_state.attempt_login = True

                username = st.session_state['username']
                password = st.session_state['password']
                print(repr(username), repr(password))
                if username == 'admin' and password == 'admin':  # TODO: Load password from config file...
                    st.session_state.logged_in = True
                else:
                    st.session_state.failed_login = True
            #st.button('Login', on_click=do_login)
            st.form_submit_button('Login', on_click=do_login)
            if st.session_state.failed_login:
                st.error('Login invalid.')
                st.session_state.failed_login = False
        # if st.button('Login'):
        #     if username == 'admin' and password == 'admin':  # TODO: Load password from config file...
        #         st.session_state.logged_in = True
        #     else:
        #         st.error('Login invalid.')

