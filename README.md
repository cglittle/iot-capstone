# IoT Capstone 2023

## Description

This project implements a low cost IoT monitoring system for workshop equipment.

Initially, Makerbot Replicator+ 3D printers and custom sensors for a Pocket NC CNC machine have been supported.

## Getting started

To set up the system back end for devices to connect, a server is required. Any number of Raspberry Pi monitoring devices may be connected once this is ready.

Install InfluxDB and Grafana on the server according to their official instructions. Set up these services, and in particular add a custom bucket to InfluxDB for the main data to be store in.

Ensure to git clone this project into the desired directory and cd into this directory for all instructions that follow.

### Overview of files

config_generator.py

dashboard_generator - used by pi_manager_backend

full_image_sim.py

image_streaming_wrangler.py - used by back end

iot_influxdb.py - imported everywhere

livestream.py - used by image_streaming_wrangler

misc.py - imported everywhere

newapi.py - necesary for pi with makerbots, not in the right place

pi_manager.py - front end

pi_manager_backend.py - main back end

pi_server_api.py - runs on pi

redirector.py - main iotcapstone.space page

email.py, report_pdf.py: email and pdf reports, not yet integrated

save_pics.py - take pictures of makerbot camera

threeamigosrpc.py - Custom RPC for control and images

virtual_lab.py - Simulated machines

camera_types - cameras that can be used on the pi

dashboard_modifiers - general dashbaords such as the hoome page

stls - Stuff to 3D print

machines - Machine files to support monitoring.

### Configuration files

To set up the configuration files, run:
```
$ python3 config_generator.py
```

Note the main config file for grafana is located here: /usr/share/grafana/conf/defaults.ini, and we have these settings that differ from the default:
```ini
[panels]
disable_sanitize_html = true
[server]
domain = grafana.yourdomain.name
[analytics]
reporting_enabled = false
feedback_links_enabled = false
[security]
strict_transport_security = true
[snapshots]
enabled = false
```

It may be of interest to have a user with view only permissions with username and password both "anonymous" for convenient dashboard access for anyone if publicity is desired.

### NTP time server

This ensures that monitoring devices have a server to synchronise their time with, as the local time can drift and fail to count time when the device is off.

Find nameserver entries:

https://www.pool.ntp.org/zone/au

```
$ sudo apt-get install ntp
$ sudo nano /etc/ntp.conf
server 0.au.pool.ntp.org iburst
server 1.au.pool.ntp.org iburst
server 2.au.pool.ntp.org iburst
server 3.au.pool.ntp.org iburst
$ sudo systemctl restart ntp
$ sudo apt install ntpdate
$ ntpdate -q server-ip
$ sudo systemctl status ntp
```

Where the last command confirms the NTP time server is working.

### Setup Cloudflared

You may set this up if you have a custom domain and no other way of ensuring connections to this domain make it to your server.

Download and install cloudflared:
```
$ wget -q https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64.deb && sudo dpkg -i cloudflared-linux-amd64.deb
```

Set up your tunnel, maintunnel:
```
$ cloudflared tunnel login
$ cloudflared tunnel create maintunnel
```

This last command will print:
```
Tunnel credentials written to /path/to/.cloudflared/tunnel-id-is-here.json.
```

For each subdomain, run:
```
$ cloudflared tunnel route dns maintunnel yourdomain.name
$ cloudflared tunnel route dns maintunnel grafana.yourdomain.name
$ cloudflared tunnel route dns maintunnel influxdb.yourdomain.name
$ cloudflared tunnel route dns maintunnel images.yourdomain.name
$ cloudflared tunnel route dns maintunnel control.yourdomain.name
$ cloudflared tunnel route dns maintunnel camera.yourdomain.name
$ cloudflared tunnel route dns maintunnel missioncontrol.yourdomain.name
```

Set up the config file so routing works properly (Located in ~/.cloudflared/config.yml):
```yaml
tunnel: tunnel-id-is-here
credentials-file: /path/to/.cloudflared/tunnel-id-is-here.json

ingress:
  - hostname: grafana.mydomain.name
    service: http://localhost:3000 
  - hostname: camera.mydomain.name
    service: http://localhost:8001
  - hostname: mydomain.name
    service: http://localhost:8002
  - hostname: influxdb.mydomain.name
    service: http://localhost:8086
  - hostname: control.mydomain.name
    service: http://localhost:10000
  - hostname: images.mydomain.name
    service: http://localhost:10001
  - hostname: missioncontrol.mydomain.name
    service: http://localhost:8501
  - service: http_status:404
```

### Installing apt-cache-ng

This allows programs to be installed on a Raspberry Pi without internet access, which may be of use to developers.

More info here: https://help.ubuntu.com/community/Apt-Cacher%20NG

This allows apt packages to be installed on the pi without direct access to the internet.

First run:
```
$ sudo apt install apt-cacher-ng
```

Edit /etc/apt-cacher-ng/acng.conf:
```
ReportPage: acng-report.html
AdminAuth: username_here:password_here

LocalDirs: acng-doc /usr/share/doc/apt-cacher-ng

CacheDir: /var/cache/apt-cacher-ng

ExThreshold: 100
ExStartTradeOff: 10000m

Remap-uburep: file:ubuntu_mirrors /ubuntu ; file:backends_ubuntu 

PrecacheFor: {uburep,secdeb,debrep}/*/*{Packages,InRelease,Packages.xz,Translation,Commands}*

LogDir: /var/log/apt-cacher-ng
```

### Installing proxpi

This allows pip packages to be installed on the pi without direct access to the internet.

$ python3 -m pip install proxpi

To run it:
```
$ FLASK_APP=proxpi.server flask run --host <server ip on local network here>
```
### Running everything

To run everything manually, which must be done each time the server is restarted:

Run the server software, each command runs forever in a separate terminal:

#### Proxpi
```
$ FLASK_APP=proxpi.server flask run --host <server ip on local network here>
```

#### Grafana
```
$ sudo systemctl restart grafana-server
```
#### Influxdb
```
$ influxd
```
#### Main webpage redirector
```
$ python3 redirector.py
```
#### Pi manager backend
```
$ python3 pi_manager_backend.py
```
#### Pi manager frontend
```
$ streamlit run pi_manager.py --server.port 8501
```
#### Cloudflared
Run cloudflared for making the server accessible over the domain name:
```
$ cloudflared tunnel run maintunnel
```

### Automatically Running Everything

A better option is to automatically run everything on the server when it starts. System services can be used for this.

All of these system services should be set up on the server:

#### Proxpi

Run:
```
$ sudo nano /lib/systemd/system/proxpi.service
```

And make the following changes:
```
[Unit]
Description=Proxpi pip proxy
After=network-online.target

[Service]
ExecStart=/usr/bin/screen -S proxpi -Dm /usr/bin/bash -c "FLASK_APP=proxpi.server /home/capstone2023/.local/bin/flask run --host 192.168.0.219"
Restart=always
User=capstone2023

[Install]
WantedBy=multi-user.target
```

Then start this service:
```
$ sudo systemctl enable proxpi.service
$ sudo systemctl restart proxpi.service
$ sudo systemctl status proxpi.service
```
#### Influx DB

Run:
```
$ sudo nano /lib/systemd/system/influxd.service
```

Edit:
```
[Unit]
Description=InfluxDB
After=network-online.target

[Service]
ExecStart=/usr/bin/screen -S influxd -Dm /usr/bin/influxd
Restart=always
User=capstone2023

[Install]
WantedBy=multi-user.target
```

This service is installed by default, ready to go:
```
$ sudo systemctl enable influxd.service
$ sudo systemctl restart influxd.service
$ sudo systemctl status influxd.service
```

#### Redirector

Run:
```
$ sudo nano /lib/systemd/system/redirector.service
```

Edit:
```
[Unit]
Description=Redirector for main page
After=network-online.target

[Service]
ExecStart=/usr/bin/screen -S redirector -Dm /usr/bin/python3 /home/capstone2023/Desktop/iot-capstone/redirector.py
Restart=always
User=capstone2023

[Install]
WantedBy=multi-user.target
```

Start:
```
$ sudo systemctl enable redirector.service
$ sudo systemctl restart redirector.service
$ sudo systemctl status redirector.service
```

#### Cloudflared

Run:
```
$ sudo nano /lib/systemd/system/cloudflared.service
```

Edit
```
[Unit]
Description=Cloudflare Tunneling
After=network-online.target

[Service]
ExecStart=/usr/bin/screen -S cloudflared -Dm /usr/local/bin/cloudflared tunnel run maintunnel
Restart=always
User=capstone2023

[Install]
WantedBy=multi-user.target
```

Start:
```
$ sudo systemctl enable cloudflared.service
$ sudo systemctl restart cloudflared.service
$ sudo systemctl status cloudflared.service
```

#### Pi Manager Backend

Run:
```
$ sudo nano /lib/systemd/system/pi_manager_backend.service
```

Paste the following contents (ensure to replace the usernames appropriately):
```
[Unit]
Description=PiManBackend
After=network-online.target

[Service]
ExecStart=/usr/bin/screen -S pi_manager_backend -Dm /usr/bin/python3 /home/capstone2023/Desktop/iot-capstone/pi_manager_backend.py
Restart=always
User=capstone2023

[Install]
WantedBy=multi-user.target
```

Start:
```
$ sudo systemctl enable pi_manager_backend.service
$ sudo systemctl restart pi_manager_backend.service
$ sudo systemctl status pi_manager_backend.service
```

#### Pi Manager

Run:
```
$ sudo nano /lib/systemd/system/pi_manager.service
```

Paste the following contents (ensure to replace the usernames appropriately):
```
[Unit]
Description=PiMan
After=network-online.target

[Service]
ExecStart=/usr/bin/screen -S pi_manager -Dm /home/capstone2023/.local/bin/streamlit run /home/capstone2023/Desktop/iot-capstone/pi_manager.py --server.port 8501
Restart=always
User=capstone2023

[Install]
WantedBy=multi-user.target
```

Start:
```
$ sudo systemctl enable pi_manager.service
$ sudo systemctl restart pi_manager.service
$ sudo systemctl status pi_manager.service
```

## Setting up a monitoring device

To set up a monitoring device from scratch without using a pre-made image, first install Raspberry Pi OS on an SD card for the pi, and follow these steps:

### Getting time right on a Raspberry Pi without access to the internet

```
$ sudo nano /etc/systemd/timesyncd.conf
[Time]
#NTP=
FallbackNTP=server-ip-here
```

Reboot the pi.
```
$ sudo shutdown -r now
```

Check ntp server is being used:
```
$ sudo apt-get install ntpstat
$ ntpstat
$ timedatectl status
```

#### Set up local package manager

$ echo 'Acquire::http { Proxy "http://server-ip-here:3142"; }' | sudo tee -a /etc/apt/apt.conf.d/proxy

TODO: Ensure this is automatically run after installing everything on the SD card for a given pi.


#### Set up local python package manager

Example usage to install a package:

```
$ python3 -m pip install --trusted-host <server ip on local network here> --index-url http://<server ip on local network here>:5000/index/ <package name here>
```

Or to make this default:
```
$ python3 -m pip config --user set global.index-url http://<server ip on local network here>:5000/index/
$ python3 -m pip config --user set global.trusted-host <server ip on local network here>
$ python3 -m pip config --user set global.extra-index-url http://<server ip on local network here>:5000/index/
```

Check:
```
$ python3 -m pip config list
```

### Setting up Pi Discovery Service

The service that is set up in this section automatically connects to the server as a core component of this sytem.

All of the below commands should be run on each pi:
```
$ sudo nano /lib/systemd/system/pi_server.service
```

Paste the following contents (ensure to replace the usernames appropriately):
```
[Unit]
Description=PiServer
After=network-online.target

[Service]
ExecStart=/usr/bin/screen -S pi_server_api -Dm /usr/bin/python3 /home/pi-username-here/Desktop/pi_server_api.py
Restart=always
User=pi-username-here
KillMode=process

[Install]
WantedBy=multi-user.target
```

Then enable the service as follows:
```
$ sudo systemctl enable pi_server.service
$ sudo systemctl restart pi_server.service
```

Check that the service is running.
```
$ sudo systemctl status pi_server.service
```

To view the output at any time later:
```
$ screen -r pi_server_api
```

To exit, ctrl+a then d.

To scroll with arrow keys ctrl+a then esc. Pressing esc again exits scroll mode.

## Screenshots

The monitoring dashboard is implemented with Grafana.

There is a main page which shows all of the workshop equipment:

![Dashboard home page.](images/dash_home.png) 

The page for an individual Makerbot Replicator+ 3D printer:

![Makerbot Replicator Plus page.](images/dash_Printer_001.png) 

And the page for an individual Pocket NC CNC machine:

![Pocket NC CNC machine page.](images/dash_CNC_001.png) 

The management interface to manager monitoring devices:

![Management interface page.](images/management2.png) 

## Features

* Live machine status, such as off/on/idle/printing.
* Machine status history timeline.
* Print start, finish, and cancellation detection for 3D printers.
* Visualisation of important sensor data for each machine.
* Live video stream for each machine
* Automatic detection of faults, such as runaway extruder temperature and adhesion loss detection for 3D printing.
* Management interface for machines.
* Automatic machine detection.
* Picture of completed prints.
* Daily PDF report summary of print jobs.

## Fault Detection

Currently supported:
* Problematic extruder temperature.
* Adhesion loss detection.

## Makerbot API

The file `newapi.py` allows Makerbot Replicator Plus 3D printer discovery and communication.

Discovery example:
```python
import newapi
import time

discovery = newapi.MakerbotDiscovery()
while True:
	found = discovery.discover()
	for ip, name, identity in found:
		print('Printer:', name, 'IP:', ip)
```

Connect to a printer:
```python
import newapi

auth_code = None  # Insert authentication code to connect without user interaction
makerbot = newapi.MakerbotAPI('<ip here>', auth_code)
makerbot.connect()
makerbot.authenticate_json_rpc()
print('Authentication code:')
print(makerbot.auth_code)
```

Get printer information, including sensor data:
```python
system_info = makerbot.get_system_information()
print('Printer name:', system_info["result"]["machine_name"])
print('Other info:')
print(system_info)
```

Output:
```
Printer name: Printer 001
Other info:
{"jsonrpc": "2.0", "result": {"machine_type": "horseshoe", "toolheads": {"extruder": [{"current_temperature": 21, "tool_id": 15, "tool_present":
 true, "index": 0, "error": 0, "target_temperature": 0, "filament_presence": true, "preheating": false}]}, "ip": "172.20.10.9", "has_been_connected_to": t
rue, "api_version": "1.9.0", "firmware_version": {"major": 2, "minor": 6, "bugfix": 2, "build": 734}, "current_process": null, "machine_name": "Printer 001",
 "sound": true, "bot_type": "replicator_b", "disabled_errors": [], "auto_unload": "off"}, "id": 2}
```

```
{'machine_type': 'horseshoe', 'toolheads': {'extruder': [{'current_temperature': 209, 'tool_id': 15, 'tool_present': True, 'index': 0, 'error': 0, 'target_temperature': 208, 'filament_presence': True, 'preheating': True}]}, 'ip': '192.168.0.165', 'has_been_connected_to': True, 'api_version': '1.9.0', 'firmware_version': {'major': 2, 'minor': 6, 'bugfix': 3, 'build': 736}, 'current_process': {'reason': None, 'thing_id': 0, 'cancelled': False, 'elapsed_time': 24, 'start_time': None, 'step': 'printing', 'cancellable': True, 'progress': 2, 'can_print_again': True, 'error': None, 'username': 'cglittle', 'id': 5, 'name': 'PrintProcess', 'methods': ['load_filament', 'suspend', 'unload_filament'], 'filepath': '/home/current_thing/1-LabradorLowPoly.makerbot', 'uuid': 'f03d59be-6b71-48f4-828a-6c8e87374e71', 'print_temperatures': {'0': 208}, 'filament_extruded': 0, 'time_remaining': 2137, 'complete': False, 'extrusion_mass_g': [3.4029998779296875], 'extrusion_distance_mm': [1115.3311767578125], 'filename': '/home/current_thing/1-LabradorLowPoly.makerbot', 'time_estimation': 2161.7099609375}, 'machine_name': 'Printer 001', 'sound': True, 'bot_type': 'replicator_b', 'disabled_errors': [], 'auto_unload': 'off'}
```

Take a picture, save to a file named `image.jpg`:
```python
_, width, height, _, image_data = makerbot.get_camera_image()

with open('image.jpg', 'wb') as f:
	f.write(image_data)
```

## Roadmap

Ideas:
* More data analysis.
* Support more machines.

## Improving Pi reliability

There have been occasions where the Pis have crashed, and we cannot SSH into them, and they have to be restarted. It would be best if this restart were automated.

The solution [here](https://raspberrypi.stackexchange.com/questions/1401/how-do-i-hard-reset-a-raspberry-pi) looks promising, though it yet to be tried.

When we have tried it, and if it works, we will outline the steps to implement this below.

[](...)

## A note on sharing internet

We did this so the TV could access our dashboard. Things like the image streaming urls are not changed for LAN access, so this is the easiest option.

In the network settings, ensure the interface that should receive internet has "shared ot ther computers" selected for ipv4 and ipv6 network settings. This ensures DNS and DHCP work on that interface.

It is possible that this internet is still not available, if so follow these steps to setup the iptables or nftables properly:

With help from [here](https://superuser.com/questions/1776029/linux-computer-sharing-internet-to-router-not-working),
```
$ sudo iptables -A FORWARD -i enx00e04c68307e -o enp7s0 -j ACCEPT
$ sudo iptables -A FORWARD -i enp7s0 -o enx00e04c68307e -m state --state ESTABLISHED,RELATED -j ACCEPT
$ sudo iptables -t nat -A POSTROUTING -o enp7s0 -j MASQUERADE
```
Where enp7s0 is the network interface to share internet from, and enx00e04c68307e is the interface receiving the internet access.

However, these commands no longer work due to iptables being replaced by nftables, the alternative commands can be found:
```
$ iptables-translate -A FORWARD -i enx00e04c68307e -o enp7s0 -j ACCEPT
$ iptables-translate -A FORWARD -i enp7s0 -o enx00e04c68307e -m state --state ESTABLISHED,RELATED -j ACCEPT
$ iptables-translate -t nat -A POSTROUTING -o enp7s0 -j MASQUERADE
```

To give:
```
$ sudo nft add rule ip filter FORWARD iifname "enx00e04c68307e" oifname "enp7s0" counter accept
$ sudo nft add rule ip filter FORWARD iifname "enp7s0" oifname "enx00e04c68307e" ct state related,established  counter accept
$ sudo nft add rule ip nat POSTROUTING oifname "enp7s0" counter masquerade
```

Although the routing worked even without the very last command.

