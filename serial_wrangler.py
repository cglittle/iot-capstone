import serial
import matplotlib.pyplot as plt
import numpy as np
import time
import numpy.fft 
from threading import Semaphore, Thread
from iot_influxdb import InfluxDBWriter
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS

print_sem = Semaphore(1)
old_print = print
def print(*args, **kw):
    print_sem.acquire()
    old_print(*args, **kw)
    print_sem.release()


PLOT_RAW = False

influx_writer = InfluxDBWriter()



import serial.tools.list_ports
ports = list(serial.tools.list_ports.comports())
for p in ports:
    print(p.description)
    if "Arduino" in p.description:
        print("This is an Arduino!")


plt.ion() # Stop matplotlib windows from blocking

# Setup figure, axis and initiate plot
fig, ax = plt.subplots()
xdata, ydata = [], []
ln, = ax.plot([], [], 'ro-')
ser = serial.Serial(ports[-1].device, 9600)
count = 0
ax.set_xlim([0, 800])
ax.set_ylim([0, 1000])
if PLOT_RAW:
    ax.set_ylim([800, 900])
fdata = []


offset = 686
while True:
    s = ser.readline()
    #print(s)
    try:
        s = int(s.strip().decode('utf8'))
        #print(s)
    except:
        continue
        
 
    
    # Get the new data
    xdata.append(count)
    ydata.append(s)
        
        
    if len(xdata) > (4*200):
        xdata.pop(0)
        ydata.pop(0)
        fdata.append(numpy.max(ydata[-50:]))   
        if len(fdata) > (4*200):
            fdata.pop(0)
         
        
    if count%800 == 0 and count > 800:
        # Reset the data in the plot
        if PLOT_RAW:
            tydata = ydata
        else:
            tydata = []
            if ydata:
            
                tydata = np.abs(numpy.fft.fft(np.array(ydata)- offset))
                tydata[0:180] = 0
                tydata[-180:] = 0
                print(np.max(tydata))
                max_current = np.max(tydata)
                step = -1
                if max_current > 700:
                    step = 4
                elif max_current > 300:
                    step = 0
                
                p = influxdb_client.Point("send efficient 2") \
                    .tag("Name", influx_writer.machine_name) \
                    .field("Step", step)
                influx_writer.write_influx(p)
                print(f'This is the step: {step} motherfucker!!!')
                
                    
                     

        ln.set_xdata(np.arange(0, len(tydata), 1))
        ln.set_ydata(tydata)

        # Rescale the axis so that the data can be seen in the plot
        # if you know the bounds of your data you could just set this once
        # so that the axis don't keep changing
        #ax.relim()
        #ax.autoscale_view()
    
        # Update the window
        fig.canvas.draw()
        fig.canvas.flush_events()
    count += 1
