import livestream
from threading import Thread, Semaphore
import cv2 as cv
import time
import requests
import traceback
import influxdb_client
import configparser
import os
import os.path
from pi_manager_backend import MachinesBackend
from misc import SEC, MIN, HOUR, DAY, WEEK, MONTH
from misc import TrackedImage, get_principal_config
import json
from threeamigosrpc import AmigosRPC, AmigosConnectionClosed

# TODO: Make this file importable and used in pi_manager_backend.py

HOME_TEST = False

IMAGE_TIMEOUT = 1 if HOME_TEST else 15

MJPEG_CONTTENT_TYPE = 'multipart/x-mixed-replace; boundary='

WAIT_BOUNDARY_STATE = 0
WAIT_HEADER_STATE = 1
WAIT_IMAGE_STATE = 2
WAIT_NEWLINE_STATE = 3

DEBUG_STATES = False


class WranglerThreadSignal:
    def __init__(self):
        self.destruct = False


class ImageStreamingWrangler:

    def __init__(self, default_image=None):
        
        self.default_image = default_image
        if self.default_image is None:
            with open('no_printer.jpg','rb') as f:
                self.default_image = f.read()
                
        self.latest_images = {}

        self.mb = MachinesBackend()
        
    def streaming_thread(self):
        latest_images = self.latest_images
        default_image = self.default_image
        other_self = self
        class CustomStreamingHandler(livestream.StreamingHandler):
            def path_terminator(self):
                path = self.path[len('/'):].replace('%20', ' ')
                # https://localhost:8001/pi_cam/Printer 001  'Printer 001'
                if '/' not in path:
                    self.send_error(404, 'Bad path!')
                    return False

                # TODO: Make printer_name pi_name and look up the subpath to find the right camera name for this printer, which depends on the machine... So we need a subpath for machine too...
                self.pi_name, self.machine_name, self.subpath = path.split('/', 2)
                #print('LATEST IMAGES:', latest_images)
                if self.pi_name not in latest_images:
                    self.send_error(404, 'No such pi.')
                    return False

                #streams = latest_images[self.pi_name]

                '''
                if self.machine_name not in streams:
                    self.send_error(404, 'No such machine.')
                    return False
                '''

                ref_response = other_self.mb.get_camera_refs(self.pi_name, self.machine_name)
                self.camera_refs = ref_response['cameras']
                #print('REF RESPONSE!!!!!!!!!', ref_response)
                if self.camera_refs is None:
                    #self.send_error(404, 'No camera refs for this machine.')
                    self.send_error(404, 'No such machine.')
                    return False

                if self.subpath not in self.camera_refs:
                    self.send_error(404, 'No such camera stream for this printer.')
                    return False
                
                self.pi_cam_name = self.camera_refs[self.subpath]

                self.tracked_image = latest_images[self.pi_name][self.pi_cam_name]
            
                #print('Pi name:', repr(self.pi_name))
                #print('Machine name:', repr(self.machine_name))
                #print("Subpath", repr(self.subpath))

                #print('Streams:', streams, self.subpath in streams)
                self.next_image_num = 0
                return True
                
            def callback(self):
                tracked_image = self.tracked_image
                while True:
                    #tracked_image = latest_images[self.pi_name][self.pi_cam_name]
                    image_num = tracked_image.count
                    image_data = tracked_image.image
                    if time.time() - tracked_image.timestamp > IMAGE_TIMEOUT:
                        tracked_image.count += 1
                        tracked_image.image = default_image
                        tracked_image.timestamp = time.time()
                    if image_num < self.next_image_num:
                        time.sleep(0.05)
                        continue
                    self.next_image_num = image_num + 1
                    #print('Sent image', self.next_image_num)
                    #print(type(image_data))
                    #print(len(image_data))
                    return image_data
        address = ('', 8001)
        server = livestream.StreamingServer(address, CustomStreamingHandler)
        server.serve_forever()

    def amigos_video_stream(self, pi_name, camera_name, amigos_client, signal_ob, tracked_image):    
        while not signal_ob.destruct:

            still_open, have_command = amigos_client.check_open_and_command()

            if not still_open:
                print(f'Connection closed for {pi_name}/{camera_name} camera input...')
                break

            if not have_command:
                time.sleep(0.001)
                continue

            # What about a timeout???
            try:
                command = amigos_client.recieve()
            except BrokenPipeError:
                print(f'Connection closed for {pi_name}/{camera_name} camera input...')
                break

            #command['file_name']
            img = b''
            try:
                for chunk in command['file_stream']:
                    img += chunk
                    # TODO: Send chunks immediately (later...)
            except BrokenPipeError:
                print(f'Connection closed for {pi_name}/{camera_name} camera input...')
                break

            tracked_image.count += 1
            tracked_image.image = img
            tracked_image.timestamp = time.time()

    def run(self):
    
        found_machines = []
    
        t = Thread(target=self.streaming_thread, args=(), daemon=True)
        t.start()

        self.camera_recv_threads = {}

        principal_config = get_principal_config()
        images_port = principal_config['amigos_images_port']

        self.amigos_server = AmigosRPC(server=True, host='0.0.0.0', port=images_port)

        self.signal_obs = {}

        while True:

            # LISTEN HERE INSTEAD USING AMIGOS RPC!

            client_addr, amigos_client = self.amigos_server.accept()

            command = amigos_client.recieve()['data']

            print('Initial command received:')
            print(json.dumps(command, indent=2))

            pi_name = command['pi_name']
            




            pis = []
            # This did not work before, maybe a race condition? While loop should fit it!
            first = True
            t = time.time()
            while pi_name not in pis:# and time.time() - t < 5:
                if not first:
                    print('Waiting...')
                    time.sleep(0.5)
                else:
                    first = False
                pis = self.mb.list_pis()
                #print('Machines are:', machines)
        
        
            
            
            
            camera_name = command['camera_name']
            associated_machines = command['associated_machines']
            if associated_machines is not None:
                print("There are associated Machine:", associated_machines, camera_name)
                machine_name, machine_camera_name, program_type = associated_machines
                machines = []
                # This did not work before, maybe a race condition? While loop should fit it!
                first = True
                t = time.time()
                while machine_name not in machines:# and time.time() - t < 5:
                    if not first:
                        print('Waiting...')
                        time.sleep(0.5)
                    else:
                        first = False
                    machines = self.mb.total_machines()
                    #print('Machines are:', machines)
                if machine_name not in machines:
                    print('BADBADBAD: Timeout, cannot proceed to update config, machine likely failed to connect!!!!')
                else:
                    print(f'Found the machine {machine_name}!')
                    machine_info = machines[machine_name]
                    config = machine_info['config']
                    #if program_type not in config:
                    #   config[program_type] = {}
                    #program_config = config[program_type]
                    if 'cameras' not in config:
                       config['cameras'] = {}
                    camera_associations = config['cameras']
                    if camera_associations.get(machine_camera_name) != camera_name:
                    
                        camera_associations[machine_camera_name] = camera_name
                        self.mb.replace_config(pi_name, machine_name, config)
                

            signal_ob = WranglerThreadSignal()

            if pi_name not in self.camera_recv_threads:
                self.camera_recv_threads[pi_name] = {}
                self.signal_obs[pi_name] = {}
                self.latest_images[pi_name] = {}
            pi_cam_recv_ts = self.camera_recv_threads[pi_name]

            if camera_name in pi_cam_recv_ts:
                # Stop the last thread!
                old_t = pi_cam_recv_ts[camera_name]
                print(f'Waiting for thread {pi_name}/{camera_name} to terminate...')
                old_signal_ob = self.signal_obs[pi_name][camera_name]
                old_signal_ob.destruct = True
                old_t.join()
                print(f'Terminated {pi_name}/{camera_name}!')

            
            tracked_image = self.latest_images[pi_name].get(camera_name)
            if tracked_image is None:
                tracked_image = TrackedImage(self.default_image) 
            tracked_image.image = self.default_image
            t = Thread(target=self.amigos_video_stream, args=(pi_name, camera_name, amigos_client, signal_ob, tracked_image), daemon=True)

            pi_cam_recv_ts[camera_name] = t
            self.signal_obs[pi_name][camera_name] = signal_ob
            self.latest_images[pi_name][camera_name] = tracked_image

            t.start()

def mjpeg_images(url, timeout=10):
    stream = requests.get(url, stream=True, timeout=timeout)
    if not stream.ok:
        print(f'Status: {stream.status_code} Reason: {stream.reason}')
        raise Exception('Stream not ok...')
    content_type = stream.headers['Content-Type']
    print('Type:', repr(content_type))
    if 'boundary=' not in content_type:
        raise Exception('Bad content type!')
    boundary = content_type.split('boundary=', 1)[1]
    if boundary.startswith('"') and boundary.endswith('"'):
        boundary = boundary[1:-1]
    print('Boundary:', repr(boundary))
    boundary = b'--' + boundary.encode('utf8')
    boundary_nl = boundary + b'\r\n'
    print('Boundary:', repr(boundary))
    state = WAIT_BOUNDARY_STATE
    image_size = None
    buf = b''
    chunk_size = 1024
    for chunk in stream.iter_content(chunk_size=chunk_size):
        #print('New chunk:', len(chunk), type(chunk))
        #print('Frame boundary present:', b'FRAME' in chunk)
        #print('Frame --boundary present:', b'--FRAME' in chunk)
        buf += chunk
        while True:

            if DEBUG_STATES:
                print('Chunk length:', len(chunk))
                print('Current state:', state, 'image size:', image_size)

            if state == WAIT_BOUNDARY_STATE:
                if len(buf) < len(boundary_nl):
                    break
                if not buf.startswith(boundary_nl):
                    print('Buf start:', repr(buf[:len(boundary_nl)]))
                    raise Exception('Bad start!!!')
                buf = buf[len(boundary_nl):]
                state = WAIT_HEADER_STATE
            elif state == WAIT_HEADER_STATE:
                if b'\r\n\r\n' not in buf:
                    break

                image_size = None

                headers, buf = buf.split(b'\r\n\r\n', 1)
                for header_line in  headers.split(b'\r\n'):
                    name, value = header_line.split(b':')
                    name = name.strip()
                    value = value.strip()
                    if name.lower() == 'content-length':
                        image_size = int(value)
                        #print('Found image size header:', image_size)

                state = WAIT_IMAGE_STATE
            elif state == WAIT_IMAGE_STATE:
                if image_size is None:
                    if b'\r\n' + boundary not in buf:
                        break
                    img, buf = buf.split(b'\r\n' + boundary, 1)

                    buf = boundary + buf

                    state = WAIT_BOUNDARY_STATE
                else:
                    if len(buf) < image_size:
                        break
                    img = buf[:image_size]
                    buf = buf[image_size:]

                    state = WAIT_NEWLINE_STATE

                yield img
            elif state == WAIT_NEWLINE_STATE:
                if len(buf) < 2:
                    break
                if not buf.startswith(b'\r\n'):
                    print('Beginning:', repr(buf[:2]))
                    raise Exception('Unexpected beginning!!!')
                buf = buf[2:]
                state = WAIT_BOUNDARY_STATE

        
if __name__ == '__main__':
    
    isw = ImageStreamingWrangler()
    isw.run()
    
