import asyncio
import influxdb_client
from influxdb_client.client.write_api import ASYNCHRONOUS
import random
import time
from threading import Thread


'''
This is a program which will send data from the sensors directly to influxdb without using telegraf. 
It will write the data asynchronously into influxdb. 
The data will be sent at random times.
It is currently using random values for the data, for application, these values would be the
values read from the printer.
'''

# This would have to be changed for different influxdbs
bucket = "data"
org = "Capstone"
token = "z4RcZfk0S0jGhY4U5GBSziX5MtGUSBlni0y-mOJXwPMI5swpJBOqcpajC2m6z5EASLGpJttJSkYgDYAGu110sg=="
# Store the URL of your InfluxDB instance
url = "http://localhost:8086"

client = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
)
# create a list of all the printer names
printer_names = ['Printer 001', 'Printer 002', 'Printer 003', 'Printer 004']

write_api = client.write_api(write_options=ASYNCHRONOUS)

class AsyncLoopThread(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.loop = asyncio.new_event_loop()

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.loop.run_forever()


# Create a function which represents the printer readings asynchronously
async def printer_async(device_name, measurement_time):
    await asyncio.sleep(measurement_time)
    print(device_name +" has finished")
    p = {"Device Name": device_name,
            "temperature_bed": random.randint(0, 40)}
    write_api.write(bucket=bucket, org=org, record=p)
    return device_name




# Create a list of all the printers

async def main():
    loop_handler = AsyncLoopThread()
    loop_handler.start()
    future_dict = {}
    for printer in printer_names:
        future_dict[printer] = None
    while True:
        for printer in printer_names:
            if future_dict[printer] is not None and not future_dict[printer].done():
                continue
            print('Add ' + printer + ' to the loop')
            future = asyncio.run_coroutine_threadsafe(printer_async(printer, random.randint(1,6)), loop_handler.loop)
            future_dict[printer] = future
        await asyncio.sleep(4)



        #await asyncio.sleep(3)

asyncio.run(main())
