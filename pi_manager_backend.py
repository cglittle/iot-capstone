
import os
import os.path
this_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(this_path)

import uuid
import importlib
import glob
import socket
import time
import socket
import sys
from threading import Thread, Semaphore
from fastapi import FastAPI, Request, Body
import uvicorn
import requests
import json
import configparser
import select
#from image_streaming_wrangler import 
from iot_influxdb import InfluxDBWriter
import traceback

#from pi_control_api import PiAPI
from threeamigosrpc import ThreeAmigosAPIServer, AmigosRPC
from misc import get_principal_config, SEC, MIN, HOUR

class InternalPiControlMsg:
    def __init__(self, rpc_message):
        self.response = None
        self.rpc_message = rpc_message

PI_API_TIMEOUT = 30

class AmigosConnectionException(Exception):
    pass

# TODO: Include hostname in amigosrpc to allow the possibility of and amigos rpc reverse proxy!
class PiAPI:
    def __init__(self):
        self.messages = {}
        self.sems = {}

    def check_pi(self, pi_name):
        if pi_name not in self.messages:
            raise Exception('No such pi has been discovered!')

    def get(self, pi_name, url):
        return self.send_and_wait_amigos(pi_name, {'amigos_version': 1, 'host': pi_name, 'method': 'get', 'url': url})

    def send_and_wait_amigos(self, pi_name, rpc_message):
        # TODO: Make this error immediately instead of timing out and not erroring... (like for post when doing kill that will also kill the connection)
        self.check_pi(pi_name)
        sem = self.sems[pi_name]
        msg = InternalPiControlMsg(rpc_message)
        sem.acquire()
        if pi_name not in self.messages:
            # Such as when the connection has been severed.
            raise AmigosConnectionException('Connection failed!')
        self.messages[pi_name].append(msg)
        sem.release()
        timeout = False
        t = time.time()
        while msg.response is None and not timeout:
            time.sleep(0.001)
            timeout = time.time() - t >= PI_API_TIMEOUT
        if timeout:
            raise Exception('Timeout waiting for respnse!')
            # return None
        if 'error' in msg.response:
            raise AmigosConnectionException('Connection failed!')
        return msg.response

    def post(self, pi_name, url, json=None, filename=None, filestream=None, filecontent=None):
        params = json
        post_rpc = {'data': params, 'amigos_version': 1, 'host': pi_name, 'method': 'post', 'url': url}
        if filename is not None:
            post_rpc['file_name'] = filename
            if filestream is None and filecontent is None:
                raise Exception('Need either file content or a file stream when a file name is specified for posting!')
        if filestream is not None:
            post_rpc['file_stream'] = filestream
        if filecontent is not None:
            post_rpc['file_content'] = filecontent
        return self.send_and_wait_amigos(pi_name, post_rpc)
    
    def run(self, pi_name, amigos_client):

        queue = []
        sem = Semaphore()
        self.sems[pi_name] = Semaphore()
        sem.acquire()
        self.messages[pi_name] = queue
        sem.release()

        last_keep_alive_t = time.time()

        while True:

            if not queue and time.time()-last_keep_alive_t >= 1*MIN:
                rpc_message = {'data': None, 'amigos_version': 1, 'host': pi_name, 'method': 'keepalive', 'url': None}
                msg = InternalPiControlMsg(rpc_message)
                queue.append(msg)

            if not queue:

                socket_still_open = amigos_client.check_open()
                '''
                # THIS DOES NOT WORK. TODO: Fix, need to use .read(), perhaps non-blocking...
                try:
                    ready_read, ready_write, in_error = select.select([amigos_client.sock], [amigos_client.sock], [], None)
                except select.error:
                    print('Select error!')
                    socket_still_open = False
                '''
                if not socket_still_open:
                    break

                time.sleep(0.001)
                continue

            last_keep_alive_t = time.time()

            sem.acquire()
            msg = queue.pop(0)
            sem.release()
            rpc_message = msg.rpc_message

            connection_cancelled = False
            try:
                amigos_client.send(rpc_message)
                response = amigos_client.recieve()
            except BrokenPipeError:
                connection_cancelled = True

            if connection_cancelled:
                break

            msg.response = response

        sem.acquire()
        del self.messages[pi_name]
        sem.release()

    def run_program(self, pi_name, program):
        r = self.post(pi_name, '/run/program', json={'command': program})

    def kill_program(self, pi_name, program):
        try:
            r = self.post(pi_name, '/kill/program', json={'command': program})
        except requests.exceptions.ConnectionError:
            pass  #
            print(traceback.format_exc())
            
    def upload_program(self, pi_name, fname):
        r = self.post(pi_name, '/upload/program', filename=fname, filestream=open(fname, 'rb'))

    def admin(self, pi_name, program):
        r = self.post(pi_name, '/admin', json={'command': program})
        
    def edit_pi(self, pi_name, new_pi_name):
        r = self.post(pi_name, '/pi/edit', json={'new_name': new_pi_name})

    def add_machine(self, pi_name, machine_info):
        r = self.post(pi_name, '/machines/add', json=machine_info)

    def edit_machine(self, pi_name, prev_machine_name, machine_info):
        r = self.post(pi_name, '/machines/edit', json={'prev_name': prev_machine_name, 'info': machine_info})

    def delete_machine(self, pi_name, machine_name):
        r = self.post(pi_name, '/machines/delete', json={'name': machine_name})

    def replace_config(self, pi_name, machine_name, config):
        r = self.post(pi_name, '/machines/replace_config', json={'name': machine_name, 'config': config})

    def replace_pi_config(self, pi_name, config):
        r = self.post(pi_name, '/replace_pi_config', json={'config': config})

    def available_programs(self, pi_name):
        return self.post(pi_name, '/program/available', json={})

    def running_programs(self, pi_name, machine_name):
        return self.post(pi_name, '/program/running', json={'name': machine_name})

    def restart_program(self, pi_name, machine_name, program_type):
        return self.post(pi_name, '/program/restart', json={'name': machine_name, 'program_type': program_type})

    def stop_all_programs(self, pi_name, machine_name):
        return self.post(pi_name, '/program/stop_all', json={'name': machine_name})

    def stop_program(self, pi_name, machine_name, program_type):
        return self.post(pi_name, '/program/stop', json={'name': machine_name, 'program_type': program_type})

def pi_timedout(found_pi):
    return time.time() - found_pi.t >= PI_TIMEOUT


def new_pi_api_control(ip, amigos_client):
    
    try:
        print('INITIAL SEND!')
        amigos_client.send({'amigos_version': 1, 'host': None, 'method': 'get', 'url': '/machines'})
        print('INITIAL WAIT!')
        response = amigos_client.recieve()
        print('INITIAL RESPONSE!')
        print("dsafsadfasdf", response)
        if 'data' in response:
            pi_info = response['data']
        else:
            pi_info = response

        
        pi_name = pi_info['name']
            
        print('Found Pi:', pi_name)
            
        if pi_name in found_pis:
            print('There must have been a problem removing the previous pi!', pi_name)
            del found_pis[pi_name]
        
        # pi_name_for_machine works this way when dashboards are autoamtically added for new machines. This is relevant for getting the pi_name for the urls when those dashbaords are made for image streaming...
        found_pis[pi_name] = DiscoveredPi(pi_info, ip)
        check_update_pis(found_pis[pi_name])

        for machine in pi_info['machines']:
        
            machine_name = machine['name']
            
            print(machine_name)
            
            found_machines[machine_name] = DiscoveredMachine(machine, ip, pi_name)
            check_update_machines(found_machines[machine_name])
        
        print(f'UPDATING PI: {pi_name}')
        # TODO: Just run programs, do not re-upload by default (better would be to check if changes in file hash to determine whether to upload)
        # mb.update([pi_name])

        rpi_api.run(pi_name, amigos_client)
        print('PIHASBEENRUN!')

    finally:
        # Clean up the connection
        amigos_client.sock.close()

    # If we get here, say that the pi is now off!
    # There is no longer a timeout.
    del found_pis[pi_name]

    
def connect_to_server():
    control_port = principal_config['amigos_control_port']
    amigos_server = AmigosRPC(server=True, host='0.0.0.0', port=control_port)
    while True:
        # Wait for a connection
        print('waiting for a connection')
        client_address, amigos_client = amigos_server.accept()
        

        print('Connection from', client_address)

        ip = client_address[0]
        t = Thread(target = new_pi_api_control, args = (ip, amigos_client), daemon = True)
        t.start()


principal_config = get_principal_config()

host = 'localhost'
port = principal_config['fastapi_internal_port']
# 8000


class MachineManager:

    def __init__(self, machines, machine_templates, machine_types):

        # Make this a list of dictionaries? Each specifies a name and a machine type...
        self.machines = machines
        
        self.machine_templates = machine_templates

        # These are modules:
        self.machine_types = machine_types

        self.machine_types_dict = {} 
        for machine in self.machine_types:
            self.machine_types_dict[machine.machine_type] = machine

    def filter_machines_by_tag(self, tag, machines=None):
        if machines is None:
            machines = self.machines
        new_machines = []
        for machine in machines:
            machine_module = self.get_machine_module(machine)
            if machine_module is None:
                continue
            if tag in machine_module.tags:
                new_machines.append(machine)
        return new_machines

    def get_machine(self, machine_name):
        for machine in self.machines:
            if machine_name == machine['name']:
                return machine
        return None
    
    def get_machine_name_for_dash(self, dash_name):
        for machine in self.machines:
            if dash_name == machine['dash_name']:
                return machine['name']
        return None
    
    def get_machine_module(self, machine):
        machine_name = machine['name']
        machine_type = machine['type']  # machine_name.split(' ')[]
        if machine_type not in self.machine_types_dict:
            print('')
        machine_module = self.machine_types_dict.get(machine_type)
        if machine_module is None:
            print('YOU SHOULD MAKE A MACHINE MODULE FOR', repr(machine_type))
        return machine_module

    def pi_name_for_machine(self, machine_name):
        print('LOOKING FOR PIS FOR MACHINE NAME:', machine_name)
        print(data_base['pis'])
        for pi_name, pi_info in data_base['pis'].items():
            for machine in pi_info['machines']:
                if machine['name'] == machine_name:
                    print('FOUND', pi_name)
                    return pi_name
        print('DID NOT FIND...')
        return None
            

    def add_machine(self, machine_name, dash_name, machine_type):
        dash_uid = str(uuid.uuid4())
        machine = {'name': machine_name, 'dash_name': dash_name, 'dash_uid': dash_uid, 'type': machine_type}
        self.machines.append(machine)
        return machine
   

class MachinesBackend:

    def __init__(self):
        self.session = requests.Session()
        self.server = server

    def named_get(self, url):
        r = self.session.get(f'{self.server}{url}')
        return json.loads(r.content)

    def named_post(self, url, json_params):
        r = self.session.post(f'{self.server}{url}', json=json_params)
        return json.loads(r.content)

    def list_pis(self):
        r = self.session.get(f'{self.server}/list_pis')
        return json.loads(r.content)

    def total_pis(self):
        r = self.session.get(f'{self.server}/total_pis')
        return json.loads(r.content)
    
    def list_machines(self):
        r = self.session.get(f'{self.server}/list_machines')
        return json.loads(r.content)
        
    def total_machines(self):
        r = self.session.get(f'{self.server}/total_machines')
        return json.loads(r.content)
        
    def update(self, pi_names):
        r = self.session.post(f'{self.server}/update_pis', json={'name': pi_names})
        
    def kill_program(self, machine_names, program):
        r = self.session.post(f'{self.server}/kill', json={'name': machine_names, 'program': program})
        
    def run_program(self, machine_names, program):
        r = self.session.post(f'{self.server}/run', json={'name': machine_names, 'program': program})
        
    def upload_program(self, machine_names, program):
        r = self.session.post(f'{self.server}/upload', json={'name': machine_names, 'program': program})

    def update_dashboards(self, machine_names):
        r = self.session.post(f'{self.server}/dashboards/update', json = {'machines': machine_names})

    def add_machine(self, machine_name, machine_type, dash_name, machine_parent):
        r = self.session.post(f'{self.server}/add_machine', json={'name': machine_name, 'type': machine_type, 'dash_name': dash_name, 'parent': machine_parent})
        #return json.loads(r.content)

    def edit_machine(self, prev_parent, prev_machine_name, machine_name, machine_type, dash_name, machine_parent):
        r = self.session.post(f'{self.server}/edit_machine', json={'prev_parent': prev_parent, 'prev_name': prev_machine_name, 'name': name, 'type': machine_type, 'dash_name': dash_name, 'parent': machine_parent})
        
    def delete_pi(self, pi_name):
        r = self.session.post(f'{self.server}/delete_pi', json={'name': pi_name})

    def edit_pi(self, prev_pi_name, pi_name):
        r = self.session.post(f'{self.server}/edit_pi', json={'prev_name': prev_pi_name, 'name': pi_name})

    def disconnect_pis(self, pi_names):
        r = self.session.post(f'{self.server}/disconnect_pis', json={'names': pi_names})

    def dashboard_links(self):
        r = self.session.get(f'{self.server}/dashboard_links')
        return json.loads(r.content)

    def delete_machines(self, machine_names):
        r = self.session.post(f'{self.server}/delete_machines', json={'names': machine_names})

    def list_machine_configurations(self):
        r = self.session.get(f'{self.server}/list_machine_configurations')
        return json.loads(r.content)

    def list_pi_configurations(self):
        r = self.session.get(f'{self.server}/list_pi_configurations')
        print(r.content)
        return json.loads(r.content)

    def replace_config(self, pi_name, machine_name, config_json):
        r = self.session.post(f'{self.server}/replace_config', json={'parent': pi_name, 'name': machine_name, 'config': config_json})

    def replace_pi_config(self, pi_name, config_json):
        r = self.session.post(f'{self.server}/replace_config', json={'name': pi_name, 'config': config_json})

    def list_dash_templates(self):
        r = self.session.get(f'{self.server}/list_dash_templates')
        return json.loads(r.content)

    def get_camera_refs(self, pi_name, machine_name):
        r = self.session.post(f'{self.server}/get_camera_refs', json={'parent': pi_name, 'name': machine_name})
        return json.loads(r.content)

    def available_programs(self, pi_name):
        r = self.session.post(f'{self.server}/program/available', json={'parent': pi_name})
        return json.loads(r.content)

    def running_programs(self, pi_name, machine_name):
        r = self.session.post(f'{self.server}/program/running', json={'parent': pi_name, 'name': machine_name})
        return json.loads(r.content)['running']

    def restart_programs(self, pi_name, machine_name, program_type):
        r = self.session.post(f'{self.server}/program/restart', json={'parent': pi_name, 'name': machine_name, 'program_type': program_type})
        return json.loads(r.content)

    def stop_all_programs(self, pi_name, machine_name):
        r = self.session.post(f'{self.server}/program/stop_all', json={'parent': pi_name, 'name': machine_name})
        return json.loads(r.content)

    def stop_program(self, pi_name, machine_name, program_type):
        r = self.session.post(f'{self.server}/program/stop', json={'parent': pi_name, 'name': machine_name, 'program_type': program_type})
        return json.loads(r.content)
    
    def available_server_programs(self):
        r = self.session.post(f'{self.server}/server_program/available', json={})
        return json.loads(r.content)

    def running_server_programs(self, machine_name):
        r = self.session.post(f'{self.server}/server_program/running', json={'name': machine_name})
        return json.loads(r.content)['running']

    def restart_server_programs(self, pi_name, machine_name, program_type):
        r = self.session.post(f'{self.server}/server_program/restart', json={'parent': pi_name, 'name': machine_name, 'program_type': program_type})
        return json.loads(r.content)

    def stop_all_server_programs(self, machine_name):
        r = self.session.post(f'{self.server}/server_program/stop_all', json={'name': machine_name})
        return json.loads(r.content)

    def stop_server_program(self, machine_name, program_type):
        r = self.session.post(f'{self.server}/server_program/stop', json={'name': machine_name, 'program_type': program_type})
        return json.loads(r.content)
    #def pi_login(self, machine_name):
    #    r = self.session.post(f'{self.server}/pi_login', json = {'name': machine_names})
    #    params = json.loads(r.content)
    #    return params['username'], params['password']

    def reload_modules(self, pi_name, machine_name):
        r = self.session.post(f'{self.server}/modules/reload', json={'name': machine_name, 'parent': pi_name})
        return json.loads(r.content)

    def reload_all_modules(self):
        r = self.session.post(f'{self.server}/modules/reload_all', json={})
        return json.loads(r.content)

    def reload_server_modules(self):
        r = self.session.post(f'{self.server}/server_modules/reload', json={})
        return json.loads(r.content)
    
#       new_from_template, delete, new_template_from_template, reload_template, reload_all, ...
# edit name: Needs to work (use json rpc or something simpler in pi_client and pi manger backend?), send machine type too, show machine ip, support machine auto discovery too for new machines, automatically create new dashboard, etc...
# Have images in pi backend (taken from image_streaming_wrangler) send from the pis directly? (little to gain though...)
        
def write_to_db():
    with open(MACHINE_NAME_DATABASE, 'w') as f:
        # f.write(json.dumps(data_base))
        f.write(json.dumps(data_base, indent=2))

def login_for_machine(machine_name):
    return username, password

class UploadableFile:
    def __init__(self, fname, run=False):
        self.fname = fname
        self.run = run
    
# TODO: Upload the files for the kinds of machines... (may need to create a folder for that on the pi)
pi_upload_folders = ['camera_types', 'machines']

# TODO: Add remote_config.json and config.json to the list...
machine_files = {
    'Makerbot Printer' : [
        UploadableFile('newapi.py'),
    ],
    'Pocket NC CNC' : [
    ],
}
for machine_type, files in machine_files.items():
    #'pi_client.py', 
    # pi_server_api.py autostarts, so we do not need to do any runinng:
    # files.extend([UploadableFile('iot4config.ini'), UploadableFile('livestream.py', run=True), UploadableFile('iot_influxdb.py'), UploadableFile('misc.py'), UploadableFile('pi_server_api.py'), UploadableFile('threeamigosrpc.py')])
    files.extend([UploadableFile('config.json'), UploadableFile('remote_config.json'), UploadableFile('iot_influxdb.py'), UploadableFile('misc.py'), UploadableFile('pi_server_api.py'), UploadableFile('threeamigosrpc.py')])

for pi_upload_folder in pi_upload_folders:
    for path in glob.glob(os.path.join(pi_upload_folder, '*.py')):
        for machine_type, files in machine_files.items():
            # None of these should be run alone:
            files.append(UploadableFile(path))

class DiscoveredMachine:
    
    def __init__(self, machine, ip, pi_name):
        self.t = time.time()
        self.machine = machine
        self.machine_name = self.machine['name']
        self.ip = ip
        self.pi_name = pi_name

class DiscoveredPi:
    
    def __init__(self, pi_info, ip):
        self.t = time.time()
        self.pi_info = pi_info
        self.name = self.pi_info['name']
        self.ip = ip

def check_update_machines(found_machine):
    pi_machine_info = found_machine.machine
    builtin_pi_config = pi_machine_info.get('config', {})
    for machine in mm.machines:
        if found_machine.machine_name == machine['name'] : 
            #machine['
            break
    else: 
        machine_info = found_machine.machine
        dash_name = machine_info['dash_name']
        machine_type = machine_info['type']
        machine = mm.add_machine(found_machine.machine_name, dash_name, machine_type)
        machine['config'] = builtin_pi_config
    # machine['config'] = builtin_pi_config
    # TODO: If there exists a data_base['pis'] entry for the machine, update that config too, or throw an error if these differ?

    machines = data_base['machines']
    if found_machine.machine_name not in machines:
        machines[found_machine.machine_name] = found_machine.machine
        write_to_db()

        print('Making dashboard for newly found machine...')
        update_machine_dashboard(machine_name=found_machine.machine_name)
        update_general_dashboards()

        # Start the server programs...
        start_all_programs(found_machine.pi_name, machine)
    
def check_update_pis(found_pi):
    pis = data_base['pis']
    if found_pi.name not in pis:
        pis[found_pi.name] = found_pi.pi_info
        write_to_db()
    
found_machines = {}
found_pis = {}

PI_TIMEOUT = 30

app = FastAPI()

@app.post('/get_camera_refs')
def get_camera_refs(params : dict = Body(...)):

    pi_name = params['parent']
    machine_name = params['name']

    pis = data_base['pis']

    if pi_name not in pis:
        print(f'No such pi: {pi_name}')
        return {'cameras': None}

    machines = data_base['machines']

    pi_info = pis[pi_name]

    if machine_name not in machines:
        print(f'No such machine: {machine_name} for pi: {pi_name}')
        return {'cameras': None}
    machine_info = machines[machine_name]
    '''
    for machine_info in pi_info['machines']:
        if machine_info['name'] == machine_name:
            break
    else:  # No break
        print(f'No such machine: {machine_name} for pi: {pi_name}')
        return {'cameras': None}
    '''
    print(machine_info)
    machine_config = machine_info.get('config', {})
    print('Getting Camera Refs!!!!!!!!!')
    print(machine_config)
    cameras = machine_config.get('cameras', {})
    return {'cameras': cameras}

@app.get('/list_dash_templates')
def list_dash_templates():
    return mm.machine_templates

@app.get('/list_machine_configurations')
def list_machine_configurations():
    configs = {}
    for machine_info in mm.machines:
        machine_name = machine_info['name']
        configs[machine_name] = machine_info.get('config', {})
    return configs  # A dictionary based on machine name...

@app.get('/list_pi_configurations')
def list_pi_configurations():
    pis = data_base['pis']
    configs = {}
    for pi_name, pi_info in pis.items():
        configs[pi_name] = pi_info.get('config', {'cameras': {}})
    return configs  # A dictionary based on machine name...

@app.post('/replace_config')
def replace_config(params : dict = Body(...)):
    pi_name = params['parent']
    machine_name = params['name']
    config = params['config']
    machines = data_base['machines']
    if machine_name not in machines:
        print('No such machine:', machine_name)
        return
    machine_info = machines[machine_name]
    machine_info['config'] = config
    write_to_db()
    pis = data_base['pis']
    pi_info = pis[pi_name]
    for machine_info in pi_info['machines']:
        if machine_info['name'] == machine_name:
            break
    else:  # No break
        print('No such machine:', machine_name)
        return
    machine_info['config'] = config
    write_to_db()

    rpi_api.replace_config(pi_name, machine_name, config)

@app.post('/replace_pi_config')
def replace_pi_config(params : dict = Body(...)):
    pi_name = params['name']
    config = params['config']
    pis = data_base['pis']
    print('Replaced!', pi_name, config)
    if pi_name not in pis:
        print('No such pi:', pi_name)
        return
    pi_info = pis[pi_name]
    pi_info['config'] = config
    write_to_db()

    rpi_api.replace_pi_config(pi_name, config)

@app.get('/list_pis')
def list_pis():
    result = {}
    for pi_name, found_pi in found_pis.items():
        if pi_name not in rpi_api.messages:
            continue
        result[pi_name] = found_pi.ip
    return result

@app.get('/total_pis')
def total_pis():
    return data_base['pis']

@app.get("/list_machines")
def list_machines():
    print(found_machines)
    machines = data_base['machines']
    any_change = False
    for machine_name, machine in found_machines.items():
        if machine_name not in machines:
            machine_type = machine_name.split(' ')[0]
            machines[machine_name] = machine.machine#{'name':machine_name, 'type': machine_type}
            any_change = True
            
    if any_change:
        write_to_db()
    cur_t = time.time()
    # TODO: Ensure this does not show non-connected pis...
    return {machine_name: found_machine.ip for machine_name, found_machine in found_machines.items() if found_machine.pi_name in found_pis}  # cur_t - found_machine.t < PI_TIMEOUT
    
# After terminating programs and using button that does this, we can test upload on pi connect.
@app.post("/disconnect_pis")
def disconnect_pis(params : dict = Body(...)):
    pi_names = params['names']

    for pi_name in pi_names:

        if pi_name not in found_pis:
            print(f'Pi {pi_name} is not connected!')
            return

        del found_pis[pi_name]

@app.post("/delete_pi")
def edit_pi(params : dict = Body(...)):
    pi_name = params['name']

    pis = data_base['pis']
    if pi_name not in pis:
        print(f'No such pi: {pi_name}...')
        return
    
    if pi_name in found_pis:
        print(f'Cannot delete a connected pi: {pi_name}.')
        return

    # Delete the database reference to the pi and its machines
    pis = data_base['pis']
    pi_info = pis.pop(pi_name)

    machines = data_base['machines']
    for machine_info in pi_info['machines']:
        machine_name = machine_info['name']
        if machine_name not in machines:
            print(f'Problem, did not find {machine_name} for {pi_name} in database...')
            continue
        machines.pop(machine_name)
    
@app.post("/edit_pi")
def edit_pi(params : dict = Body(...)):
    prev_pi_name = params['prev_name']
    pi_name = params['name']
    any_change = False
    any_change = any_change or prev_pi_name != pi_name
    if not any_change:
        print('No changes for pi...')
        return
    
    if pi_name != prev_pi_name:
        pis = data_base['pis']
        for _, pi_info in pis.items():
            if pi_info['name'] == pi_name:
                print('This pi name is taken:', pi_name)
                return

    pis = data_base['pis']
    if prev_pi_name not in pis:
        print('No such pi:', prev_pi_name)
        return
    pi_info = pis[prev_pi_name]
    del pis[prev_pi_name]
    pi_info['name'] = pi_name
    pis[pi_name] = pi_info

    # Change machines.json on the pi itself
    rpi_api.edit_pi(prev_pi_name, pi_name)

    write_to_db()
    
@app.post("/edit_machine")
def edit_machine(params : dict = Body(...)):
    prev_pi_name = params['prev_parent']
    pi_name = params['parent']
    machine_name = params['name']
    prev_machine_name = params['prev_name']

    if pi_name not in found_pis:
        print(f'ERROR: Pi {pi_name} needs to be on to edit a machine.')
        return

    machines = data_base['machines']
    machine_info = machines.get(prev_machine_name)
    if machine_info is None:
        print('No such machine:', prev_machine_name)
        return

    machine_type = params['type']
    prev_machine_type = machine_info['type']
    dash_name = params['dash_name']
    prev_dash_name = machine_info['dash_name']
    any_change = False
    any_change = any_change or prev_pi_name != pi_name
    any_change = any_change or prev_machine_name != machine_name
    any_change = any_change or prev_machine_type != machine_type
    any_change = any_change or prev_dash_name != dash_name
    if not any_change:
        print('No changes for machine...')
        return

    if machine_name != prev_machine_name:
        if machine_name in machines:
            print('This machine name is taken:', machine_name)
            return

    if machine_type != prev_machine_type:
        print('Changing machine type not currently supported...')
        return

    # Ensure the new machine type exists...
    if machine_type not in mm.machine_types:
        print(f'Unrecognised machine type: {machine_type}.')
        print(f'All: {mm.machine_types}')
        return

    # Ensure the new dash name does not exist...
    for a_machine_info in data_base['machines']:
        if a_machine_info['name'] == prev_machine_name:
            continue
        if a_machine_info['dash_name'] == dash_name:
            print(f'Dashboard name exists: {dash_name}')
            return

    pis = data_base['pis']
    if prev_pi_name not in pis:
        print('No such prev pi:', prev_pi_name)
        return
    if pi_name not in pis:
        print('No such pi:', pi_name)
        return

    prev_machines = prev_pi_info['machines']
    machines = pi_info['machines']

    if machine_name not in [machine['name'] for machine in prev_machines]:
        print(f'Machine {machine_name} not in the machines for pi {pi_name}:')
        print(machines)
        return

    # TODO: Stop programs for this machine...

    prev_pi_info = pis[prev_pi_name]
    pi_info = pis[pi_name]

    new_machines = []
    for machine in prev_machines:
        if machine['name'] == machine_name:
            continue
        new_machines.append(machine)
    prev_pi_info['machines'] = new_machines

    pi_info['machines'].append({'name': machine_name, 'type': machine_type})

    machine_info['name'] = machine_name
    machine_info['type'] = machine_type
    machine_info['dash_name'] = dash_name

    if prev_machine_name != machine_name:
        del machines[prev_machine_name]
        machines[machine_name] = machine_info

    # Changes the actual name of the dashboard in Grafana.

    #prev_template_machine_name, prev_template_dash_name = mm.machine_templates[machine_type]

    '''
    if dash_name != prev_dash_name:
        delete_machine_dashboard(prev_machine_name, prev_dash_name)
    if machine_name != prev_machine_name:
        # Delete alerts if machine name changed...
        delete_machine_alerts(prev_machine_name, prev_dash_name)
    '''

    #folder_name = machine_name
    #dashboard = 
    #md.update_dashboard(folder_name, dashboard)

    #template_machine_name, template_dash_name = mm.machine_templates[machine_type]

    rpi_api.edit_machine(pi_name, prev_machine_name, machine_info)
    write_to_db()

    # Update the measurement name in InfluxDB:
    influx_writer.change_measurement_name(prev_machine_name, machine_name)

    update_machine_dashboard(machine_name=machine_name, old_machine_name=prev_machine_name, old_dash_name=prev_dash_name)

    # Update home dashboard
    update_general_dashboards()

    # TODO: Restart programs for this machine...
    # TODO: Change name of program runner things and camera things...
    #start_program(pi_name, machine_info, program_type)

@app.post("/add_machine")
def add_machine(params : dict = Body(...)):
    #params = params.json()
    print(repr(params))
    dash_name = params['dash_name']
    pi_name = params['parent']
    name = params['name']
    machine_type = params['type']

    pis = data_base['pis']
    if pi_name not in pis:
        print('Pi unrecognised:', repr(pi_name))
        return

    if pi_name not in found_pis:
        print(f'ERROR: Pi {pi_name} needs to be on to add a machine.')
        return

    # alt: if name in 
    for _, machine in mm.machines.items():
        if machine['name'] == name:
            print('This machine name is taken:', name)
            return

    if machine_type not in mm.machine_types:
        print(f'Unrecognised machine type: {machine_type}.')
        print(f'All: {mm.machine_types}')
        return

    # Ensure the new dash name does not exist...
    for a_machine_info in data_base['machines']:
        if a_machine_info['dash_name'] == dash_name:
            print(f'Dashboard name exists: {dash_name}')
            return

    pis = data_base['pis']
    pis[pi_name]['machines'].append({'name': name, 'type': machine_type})

    machine = mm.add_machine(name, dash_name, machine_type)
    data_base['machines'][name] = machine  # {'name':name, 'type': machine_type, 'dash}

    print('ADDED!!!!!!!!!!!', name)
    # Change machines.json on the pi itself:
    rpi_api.add_machine(pi_name, prev_machine_name, machine)

    write_to_db()
    # {'Printer 001': {'name': 'Printer 001', 'type': 'Printer'}}

    update_machine_dashboard(machine_name=name)

    # Update home dashboard
    update_general_dashboards()

@app.get("/total_machines")
def total_machines():
    return data_base['machines']
    
    
@app.post("/update_pis")
def update_pis(params : dict = Body(...)):
    print('Updating...')
    for pi_name in params['name']:
        if pi_name not in found_pis:
            print('skipping machines')
            continue
        found_pi = found_pis[pi_name]
        ip = found_pi.ip
        new_machine_files = []
        for machine in found_pi.pi_info['machines']:
            machine_type = machine['type']
            for uploadable_machine_file in machine_files[machine_type]:
                machine_file = uploadable_machine_file.fname
                if machine_file not in new_machine_files:
                    new_machine_files.append(uploadable_machine_file)
        for uploadable_machine_file in new_machine_files:
            f = uploadable_machine_file.fname
            rpi_api.upload_program(pi_name, f)
            #if f.endswith('.py') and f != 'pi_client.py':
            if uploadable_machine_file.run:
                rpi_api.run_program(pi_name, f.split('.')[0])
        try:
            rpi_api.kill_program(pi_name, 'pi_server_api')
        except:
            print(traceback.format_exc())
        # rpi_api.kill_program(pi_name, 'pi_client')


# TODO: Reload server modules, reload pi modules, reload all pi modules (at least all that are on)
@app.post("/modules/reload")
def reload_modules(params : dict = Body(...)):
    pi_name = params['parent']
    machine_name = params['name']
    pass
@app.post("/modules/reload_all")
def reload_all_modules(params : dict = Body(...)):
    pass
@app.post("/server_modules/reload")
def reload_server_modules(params : dict = Body(...)):
    pass
   
@app.post("/kill")
def kill_program(params : dict = Body(...)):
    program = params['program']
    
    for machine_name in params['name']:
        ip = found_machines[machine_name].ip
        rpi_api.kill_program(program.split('.')[0],ip)
        

@app.post("/run")
def run_program(params : dict = Body(...)):
    program = params['program']
    
    for machine_name in params['name']:
        ip = found_machines[machine_name].ip
        if program.endswith('.py') and f != 'pi_client.py':
            rpi_api.run_program(program.split('.')[0], ip)
    

@app.post("/upload")
def upload_program(params : dict = Body(...)):
    
    for machine_name in params['name']:
        ip = found_machines[machine_name].ip
        #for f in params[program]:
        pi_name = found_machines[machine_name].pi_name
        f = params['program']
        rpi_api.upload_program(pi_name,f)
        print('uploading ', f)

@app.post("/delete_machines")
def delete_machines(params : dict = Body(...)):
    machine_names = params['names']
    machines = data_base['machines']
    for machine_name in machine_names:
        if machine_name not in machines:
            print('No such machine to delete:', machine_name)
            continue
        machine_info = machines[machine_name]
        del machines[machine_name]
        if machine_name in found_machines:
            del found_machines[machine_name]
        # TODO: Delete the dashboard in Grafana and associated alerts!

        # Delete on the raspberry pi:
        # PROBLEM: What if we edit, add, or delete a machine for a pi that is not connected???
        #         Delete from here, but when pi turns on, it will be added back. That is fine. Alternative is a git like version control on the thing...
        #         Edit and add should require that the pi is on.
        if machine_name in found_machines:
            found_machine = found_machines[machine_name]
            pi_name = found_machine.pi_name

            rpi_api.delete_machine(pi_name, machine_name)

            print(f'Deleted {machine_name} from pi {pi_name}.')
        else:
            print(f'Pi for {machine_name} is not on, doing a soft delete.')

        print(f'Deleted {machine_name}!')

    # TODO: Delete from mm.machine_templates if applicable

    update_general_dashboards()

    write_to_db()

@app.get("/dashboard_links")
def dashboard_links():
    return dashboard_links  # {'links': dashboard_links}

def start_all_programs(pi_name, machine_info):
    machine_type = machine_info['type']
    machine_name = machine_info['name']
    program_types = pr.named_programs.get(machine_type, {})
    # programs_options = pr.get_programs_options(machine_info)
    for program_type in program_types:
        # mcahine_info, program_type, options
        print(f'Starting {machine_name}/{program_type}...')
        # config_options = programs_options.get(program_type, {})
        # [pr.start(machine_info, program_type, args=(influx_writer, isw, pi_name, machine_info, config_options, found_machines, found_pis))
        start_program(pi_name, machine_info, program_type)
        print(f'Started {machine_name}/{program_type}!')
def start_program(pi_name, machine_info, program_type):
    programs_options = pr.get_programs_options(machine_info)
    config_options = programs_options.get(program_type, {})
    pr.start(machine_info, program_type, args=(influx_writer, isw, pi_name, machine_info, config_options, found_machines, found_pis))
def stop_program(machine_info, program_type):
    machine_name = machine_info['name']
    program_lst = pr.get_running(machine_info)
    for this_program_info in program_lst:
        this_program_name = this_program_info['name']
        this_program_type = this_program_info['type']
        if program_type == this_program_type:
            print(f'Killing program: {this_program_name}...')
            pr.stop(machine_info, this_program_name)
@app.post('/server_program/available')
def available_server_programs(params : dict = Body(...)):
    # Return binned by machine_type
    available = {}
    print('called ', pr.named_programs)
    for machine_type, program_dict in pr.named_programs.items():
        available[machine_type] = list(program_dict)
    return available
@app.post('/server_program/running')
def running_server_programs(params : dict = Body(...)):
    machine_name = params['name']
    machine_info = machines.get(machine_name)
    if machine_info is None:
        print('Youd did wrong')
        return {'error': None}
    running_lst = pr.get_running(machine_info)
    # Each item in the result is {'name': ..., 'type': ...}
    return {'running': running_lst}
@app.post('/server_program/restart')
def restart_server_program(params : dict = Body(...)):
    pi_name = params['parent']
    machine_name = params['name']
    machine_info = machines.get(machine_name)
    if machine_info is None:
        error_msg = f'Did not find requested name: {machine_name}...'
        print(error_msg)
        return {'error': error_msg}
    program_type = params['program_type']
    print(f'Requested killing program {program_type} for machine {machine_name}:')
    stop_program(machine_info, program_type)
    print(f'Starting program: {program_type}...')
    start_program(pi_name, machine_info, program_type)
    print('The restarting is over.')
@app.post('/server_program/stop_all')
def stop_all_server_programs(params : dict = Body(...)):
    machine_name = params['name']
    machine_info = machines.get(machine_name)
    if machine_info is None:
        error_msg = f'Did not find requested name: {machine_name}...'
        print(error_msg)
        return {'error': error_msg}
    print(f'Killing all programs for machine {machine_name}:')
    program_lst = pr.get_running(machine_name)
    for program_info in program_lst:
        program_name = program_info['name']
        print(f'Killing program: {program_name}...')
        pr.stop(machine_info, program_name)
    print('Killing over.')
    #pr.stop_all(machine_info, )
@app.post('/server_program/stop')
def stop_server_program(params : dict = Body(...)):
    machine_name = params['name']
    machine_info = machines.get(machine_name)
    if machine_info is None:
        error_msg = f'Did not find requested name: {machine_name}...'
        print(error_msg)
        return {'error': error_msg}
    program_type = params['program_type']
    print(f'Killing program {program_type} for machine {machine_name}...')
    stop_program(machine_info, program_type)
    # stop_program(machine_info, program_type)
    # pr.stop(machine_info, program_name)
    print('The killing is over.')


# parent
@app.post('/program/available')
def available_programs(params : dict = Body(...)):
    pi_name = params['parent']
    return rpi_api.available_programs(pi_name)['data']
# parent, name
@app.post('/program/running')
def running_programs(params : dict = Body(...)):
    pi_name = params['parent']
    machine_name = params['name']
    return rpi_api.running_programs(pi_name, machine_name)['data']
# parent, name, program_type
@app.post('/program/restart')
def restart_program(params : dict = Body(...)):
    pi_name = params['parent']
    machine_name = params['name']
    program_type = params['program_type']
    return rpi_api.restart_program(pi_name, machine_name, program_type)['data']
# parent, name
@app.post('/program/stop_all')
def stop_all_programs(params : dict = Body(...)):
    pi_name = params['parent']
    machine_name = params['name']
    return rpi_api.stop_all_programs(pi_name, machine_name)['data']
# parent, name, program_type
@app.post('/program/stop')
def stop_the_program(params : dict = Body(...)):
    pi_name = params['parent']
    machine_name = params['name']
    program_type = params['program_type']
    return rpi_api.stop_program(pi_name, machine_name, program_type)['data']


# TODO: templates folder, where each file is associated with a machine type?
#       (file is json, containing both alerts and dashbaord)
#       * Fall back to this file (named according to the machine type) if there
#         is no existing machine and dashboard being used for the template...
#       * Thus, we can delete the machines for these templates, and still recover by
#         remaking a machine with the same name, and it will become the template.
#         (the first machine of that type when there is no template becomes the template.
#            * Need to indicate which machines are the templates in another list.
#       * NOTE: PROBLEM: It is now the case that we need two files to add a mahcine,
#                        the machine module file and the json backup.

'''
def delete_machine_alerts(machine_name, dash_name):
    pass
    # TODO: Implement

def delete_machine_dashboard(machine_name, dash_name):
    pass
    # TODO: Implement!
    if machine_name not in dashboard_links:
        print(f'Machine {machine_name} not in dashboard links!')
        print(dashboard_links)
        return
    del dashboard_links[machine_name]
'''

def get_template_json(template_name):
    with open(f'{MACHINES_DIRECTORY}/{template_name}') as f:
        content = f.read()
    template_info = json.loads(content)
    return template_info

reload_template_from_file = False
UPLOAD_THINGS = True
store_things = False  # True

def get_amigos_template_fname(machine_type=None, general_dash_name=None, general_info=None):
    if machine_type is None:
        if general_dash_name is None:
            raise Exception('Either machine name or general dashboard name must be specified!!!')
        print(md.dashboard_modifiers)

        for dash_mod in md.dashboard_modifiers:
            if general_info['type'] == dash_mod.general_type:
                the_module = dash_mod
                break
        else:  # No break
            print('Unrecognised general dashboard:', general_dash_name)
            return None
    elif machine_type not in mm.machine_types_dict:
        print('Unrecognised machine type:', machine_type)
        return None
    else:
        machine_module = mm.machine_types_dict[machine_type]
        the_module = machine_module
    module_fname, module_ext = os.path.splitext(the_module.__file__)
    amigos_template_fname = f'{module_fname}.json'
    return amigos_template_fname

def get_amigos_template(amigos_template_fname, machine_type=None, dash_name=None, machine_name=None):
    template_dash_name, template_machine_name = dash_name, machine_name
    if machine_type is not None:
        if machine_type not in mm.machine_templates:
            #template_name = data_base['default_template']
            template_machine_type = data_base['default_template']
            template_pair = mm.machine_templates.get(template_machine_type)
            if template_pair is None:
                # Read it from file...
                amigos_template = get_template_json(template_machine_type)
                template_pair = [amigos_template['machine_name'], amigos_template['dash_name']]
            #mm.machine_templates[machine_type] = template_pair
            template_machine_name, template_dash_name = template_pair
            mm.machine_templates[machine_type] = [machine_name, dash_name]
        else:
            template_machine_name, template_dash_name = mm.machine_templates[machine_type]

    rule_group = None
    if machine_type is not None:
        alert_folder_name = template_machine_name
        # Do not require existence of rule_group, only dashboards... if alerts exist, we grab them and delete them before reuploading...
        rule_group = md.rule_group_for_folder(alert_folder_name, template_dash_name)
        #print("3. SJGKFJGDSAKFJGAKDSFJG", type(rule_group))
    dashboard_uid = md.dashboards.get(template_dash_name, None)
    print("2. SJGKFJGDSAKFJGAKDSFJG", template_dash_name)
    dashboard_exists = dashboard_uid is not None  # and rule_group is not None
    if dashboard_exists:
        dashboard = md.dashboard_json(dashboard_uid)
        # Grab from grafana:
        amigos_template = {'dash_name': template_dash_name, 'machine_name': template_machine_name, 'dashboard': dashboard, 'rule_group': rule_group}
    else:
        amigos_template_exists = os.path.exists(amigos_template_fname)
        if not amigos_template_exists:
            if machine_name is None:
                raise Exception(f'Dashboard template not found ({template_dash_name}) at {repr(amigos_template_fname)}, and creation of new ones without an existing template file is not yet supported.')
            # NOTE: This part is really unnecessary 
            # Use the default template, and write it
            template_machine_type = data_base['default_template']
            template_amigos_template_fname = get_amigos_template_fname(machine_type=template_machine_type, dash_name=dash_name, machine_name=machine_name)
            # TODO: Use shutil or something for the file copy?
            with open(template_amigos_template_fname) as fr:
                with open(amigos_template_fname, 'w') as fw:
                    fw.write(fr.read())
            #mm.machine_templates[template_machine_type]
            #data_base[]
            #pass
        # Read from file:
        with open(amigos_template_fname) as f:
            amigos_template = json.loads(f.read())

    return amigos_template

def update_machine_dashboard(machine_name=None, general_dash_name=None, general_info=None, old_machine_name=None, old_dash_name=None, relist=True):
    global dashboard_links
    if machine_name is None:
        if general_dash_name is None:
            raise Exception('Either machine name or general dashboard name must be specified!!!')
        dash_name = general_dash_name
        old_dash_name = general_dash_name

        # f'{GEN_DASH_DIR}/{dash_name}.json'
        amigos_template_fname = get_amigos_template_fname(general_dash_name=general_dash_name, general_info=general_info)
        machine_type = None
        amigos_template = get_amigos_template(amigos_template_fname, machine_type, old_machine_name, old_dash_name)

        dash_uid = md.dashboards.get(dash_name)
        if dash_uid is None:
            dash_uid = str(uuid.uuid4())  # md.new_uid_dashboard()
        # dash_uid = amigos_template['dashboard']['uid']

        #machine_type = None

        machine = {'name': machine_name, 'dash_name': dash_name, 'dash_uid': dash_uid, 'type': machine_type}
    else:
        machine = mm.get_machine(machine_name)
        dash_name = machine['dash_name']
        machine_type = machine['type']

        # Get from machine module under this machine type...
        amigos_template_fname = get_amigos_template_fname(machine_type=machine_type)
        print("Amigos template fname is this ", amigos_template_fname)
        amigos_template = get_amigos_template(amigos_template_fname, machine_type, old_machine_name, old_dash_name)

        # Modify template machine name and dash name if appropriate. (This needs to be done after the new dashboard is made above, so that a template dashboard of the claimed name is always available)
        for a_machine_type in list(mm.machine_templates):
            template = mm.machine_templates[a_machine_type]
            tem_machine_name, tem_dash_name = template
            if tem_machine_name == old_machine_name:
                mm.machine_templates[a_machine_type] = [machine_name, dash_name]

    # Check if such a dashboard and alerts exists
    alert_folder_name = machine_name


    # dash_uid is in machine info, so we can upload and override the old immediately...
    #dash_uid = machine['dash_uid']

    # TODO: For this and general dashboard update, need to use the new template storage mechanism...
    # Problem: Machine manager does not handle the list of machines...
    print(f'Updaing dashboard for {machine_name}...')

    if old_machine_name is None:
        old_machine_name = machine_name
    if old_dash_name is None:
        old_dash_name = dash_name
    old_alert_folder_name = old_machine_name
    old_group_name = old_dash_name

        
    # TODO: See if this is even needed...
    #template_machine_name, template_dash_name = mm.machine_templates[machine_type]

    amigos_template = md.full_transplant(mm, amigos_template, machine, general_info=general_info)

    dashboard = amigos_template['dashboard']
    rule_group = amigos_template['rule_group']

    folder_name = principal_config['default_machine_folder']
    if machine_name is None:  # General dashboard
        folder_name = general_info['folder']
    else:
        folder_overrides = principal_config['machine_folders']
        print('CUR FOLDER NAME:', folder_name)
        print('FOLDER OVERRIDE:', folder_overrides)
        print('CUR MACHINE TYPE:', machine_type)
        if machine_type in folder_overrides:
            folder_name = folder_overrides[machine_type]
        print('NEW FOLDER NAME:', folder_name)
        machine_config = machine.get('config', {})
        # NOTE: We can have in machine_info: 'config': {'folder': 'Prioritised Grafana Folder Name'}
        print('MACHINE CONFIG:', machine_config)
        folder_name = machine_config.get('folder', folder_name)
    # 'Machine Info'
    print('RENEWING FOLDER FOR DASHBOARD:', folder_name, dash_name)
    md.renew_folder(folder_name)
    folder_uid = md.folders[folder_name]
    md.update_dashboard(folder_name, dashboard)

    # Delete the old alerts name, and make the alerts with new names.

    # When uploading:
    if machine_name is not None: # Cannot be a general dashboard though
        md.renew_folder(alert_folder_name)  # Ensure folder even if rule group does not exist...
    # NOTE: THIS SHOULD NEVER BE NONE< hopefully we can have a rule group with no alerts assigned?
    if rule_group is not None:
        #group_name = dash_name
        md.delete_alerts(old_alert_folder_name, old_group_name)
        # Upload here...
        if 'folder' in rule_group:
            folder_name = rule_group['folder']
            del rule_group['folder']
        md.update_alerts(alert_folder_name, rule_group)
    # Always upload the dashboard:

    # Write new templates:
    # template_dashboard, template_rule_group
    write_template = False
    if machine_name is None:
        write_template = True
    elif mm.machine_templates[machine_type][0] == machine_name:
        write_template = True
    if write_template:
        amigos_template['dash_name'] = dash_name
        amigos_template['machine_name'] = machine_name
        print(amigos_template_fname)
        
        with open(amigos_template_fname, 'w') as f:
            f.write(json.dumps(amigos_template, indent=2))

    #template_dashboard = md.template_dashboard(template_dash_name, reload_template_from_file=reload_template_from_file)
    #md.store_dashboard(template_dash_name, dashboard=template_dashboard)
    
    #template_rule_group = md.template_alerts(template_dash_name, template_machine_name, reload_template_from_file=reload_template_from_file)
    #md.store_alert(template_machine_name, rule_group=template_rule_group)


    #dashboards_and_alerts[machine_type] = (dashboard, rule_group)
    
    
    # template_alert_folder_name is None, though should not be...
    # NOTE: mm should not need to be given to md?
    #rule_group = md.renew_alert(dash_name, machine_name, rule_group=template_rule_group, upload_alerts=UPLOAD_THINGS, store_alerts=store_things)
    #dashboard = md.renew_dashboard(mm, dash_name, machine_name, template_dashboard=template_dashboard, upload_dashboard=UPLOAD_THINGS, store_dashboard=store_things)

    # Write rule_group and dashboard to a folder for viewing:
    #md.store_alert(machine_name, rule_group=rule_group, alert_directory=GEN_ALERT_DIRECTORY)
    #md.store_dashboard(dash_name, dashboard=dashboard, dashboard_directory=GEN_DASH_DIRECTORY)
    '''
    if new_template_made:
        mm.machine_templates[machine_type] = [machine_name, dash_name]
        write_to_db()
        print('Wrote new templates to database!')
    '''

    write_to_db()

    if relist:
        md.relist_all()

    # We should be able to get the link with everything relisted after the dashboard is created:
    dashboard_links = md.dashboard_links(mm)
    # print('UPDATED DASH LINKS:', list(dashboard_links))
    # print('IS IT HERE?', 'Test Printer 1' in dashboard_links)
    #dashboard_links[machine_name] = None

def update_general_dashboards(relist=True):
    print('Updating general dashboards...')
    # for dash_name in data_base['general_dashboard_templates']:
    for dash_name, general_dash_info in principal_config['general_dashboards'].items():
        #dash_name = general_dash_info['name']
        print(f'Updaing general dashboard {dash_name}...')
        # uploads, but does not store: (this seems wrong, it should be storing...)
        #md.renew_dashboard(mm, dash_name, template_dash_name=dash_name, upload_dashboard=UPLOAD_THINGS, store_dashboard=store_things)
        #machine_name = None
        update_machine_dashboard(general_dash_name=dash_name, general_info=general_dash_info, relist=False)

    if relist:
        md.relist_all()
    
@app.post("/dashboards/update")
def upload_program(params : dict = Body(...)):
    for machine_name in params['machines']:
        update_machine_dashboard(machine_name=machine_name, relist=False)
        # Need to associate this machine name to a dashboard name... (that should be stored in the database)
    # Now update the home dashboard:
    update_general_dashboards()
server = f'http://{host}:{port}'
if __name__ == "__main__":

    print_sem = Semaphore(1)
    old_print = print
    def print(*args, **kw):
        print_sem.acquire()
        old_print(*args, **kw)
        print_sem.release()


    # default_home_dashboard_name = 'Test_Homepage'

    influx_writer = InfluxDBWriter()

    influxdb_instances = influx_writer.influxdb_instances
    influx_instance = influxdb_instances['local']
    bucket = influx_instance['bucket']
    #influx_instance['org']
    measurement_name = influx_instance['measurement']
    #bucket = 'Test'
    #point_name = 'send efficient 2'

    
    MACHINE_NAME_DATABASE = 'machines.json'
    data_base = {}
    if os.path.exists(MACHINE_NAME_DATABASE):
        with open(MACHINE_NAME_DATABASE) as f:
            data_base = json.loads(f.read())

    if 'machines' not in data_base:
        data_base['machines'] = {}
        
    data_base['machines'] = {machine_name: machine for machine_name, machine in data_base['machines'].items() if machine is not None}

    if 'pis' not in data_base:
        data_base['pis'] = {}

    for machine_name, machine in data_base['machines'].items():
        print(machine)
        if 'dash_name' not in machine:
            parts = machine_name.split(' ')
            num = int(parts.pop(-1))
            base_name = ' '.join(parts)
            dash_name = f'{base_name} {num}'
            print(f'Created new dash name: {dash_name}')
            machine['dash_name'] = dash_name
    if 'machine_dashboard_templates' not in data_base:
        # We actually want a separate template dashboard for this...
        data_base['machine_dashboard_templates'] = {'Makerbot Printer': ('Printer 001', 'Printer 1')}
        
    if 'default_template' not in data_base or data_base['default_template'] == ['Printer 001', 'Printer 1']:
        # The below is the default machine type name instead, since
        # the machine name can actually change... e.g. 'Printer'
        data_base['default_template'] = 'Makerbot Printer'
        # NOTE: Only if there is no stored template dashboard on Grafana for 'Printer', read from file. Always write to file.
        # Template file: {'default_machine_name': 'Printer 001', 'default_dash_name': 'Printer 1', 'dashboard': ..., 'alerts': ...}




    
    from dashboard_generator import MachineDashboards, ensure_path  # MachineManager, 
    from image_streaming_wrangler import ImageStreamingWrangler
    from misc import ProgramRunner, RunningProgram, modules_in_path

    isw = ImageStreamingWrangler()
    t = Thread(target=isw.run, daemon=True)
    t.start()

    # TODO: LOAD THE BELOW URL FROM A CONFIG FILE!
    md = MachineDashboards(bucket=bucket, measurement_name=measurement_name, grafana_server_url=principal_config['grafana_display_url'])
    machines = data_base['machines']
    machine_templates = data_base['machine_dashboard_templates']
    print(machine_templates)
    MACHINES_DIRECTORY = 'machines'
    for path in [MACHINES_DIRECTORY]:
        ensure_path(path)
    print('Loading machine types modules...')
    machine_types_modules = modules_in_path(MACHINES_DIRECTORY, constraint = lambda module: hasattr(module, 'machine_type'))
    mm = MachineManager([machine for machine_name, machine in machines.items()], machine_templates, machine_types_modules)

    rpi_api = PiAPI()

    # Start the programs:
    print('Program runner stuff:')
    #named_programs = {machine_module.machine_type: machine_module for machine_module in machine_types_modules}
    pr = ProgramRunner(server=True, api_callback_args=(app, rpi_api))  # named_programs=named_programs, 
    # for machine_name, machine_info in machines.items():
    for pi_name, pi_info in data_base['pis'].items():
        for machine_info in pi_info['machines']:
            print(f'For {machine_name}:')
            start_all_programs(pi_name, machine_info)

    # Add the uid to the machine info if it is not there already:
    for machine_name, machine in machines.items():
        dash_name = machine['dash_name']
        if 'dash_uid' not in machine:
            if dash_name not in md.dashboards:
                print(f'PROBLEM: No dashboard for {dash_name}.')
                exit(1)
            dash_uid = md.dashboards[dash_name]
            machine['dash_uid'] = dash_uid

    #GEN_DASH_DIR = 'general_dashboards'
    #ensure_path(GEN_DASH_DIR)

    # Need to make the new kinds of templates:
    if False:
        print('Checking for old machine templates...')
        for machine_type, (machine_name, dash_name) in data_base['machine_dashboard_templates'].items():
            amigos_template_fname = get_amigos_template_fname(machine_type=machine_type)
            if amigos_template_fname is None:
                print('TODO: Handle this case (low priority)!')
                continue
            if os.path.exists(amigos_template_fname):
                continue
            print('Writing new template type:', amigos_template_fname)
            alert_folder_name = machine_name
            rule_group = md.load_alert_from_file(alert_folder_name)
            dashboard = md.load_dashboard_from_file(dash_name)
            amigos_template = {'dash_name': dash_name, 'machine_name': machine_name, 'dashboard': dashboard, 'rule_group': rule_group}
            with open(amigos_template_fname, 'w') as f:
                f.write(json.dumps(amigos_template, indent=2))
        # Do not forget the general dashboards
        print('Checking for old general dashboards...')
        for dash_name in data_base['general_dashboard_templates']:
            amigos_template_fname = get_amigos_template_fname(general_dash_name=dash_name)
            #amigos_template_fname = os.path.join(GEN_DASH_DIR, f'{dash_name}.json')
            if os.path.exists(amigos_template_fname):
                continue
            print('Writing new template type:', amigos_template_fname)
            #alert_folder_name = 
            #md.load_alert_from_file(alert_folder_name)
            dashboard = md.load_dashboard_from_file(dash_name)
            amigos_template = {'dash_name': dash_name, 'machine_name': None, 'dashboard': dashboard, 'rule_group': None}
            with open(amigos_template_fname, 'w') as f:
                f.write(json.dumps(amigos_template, indent=2))
        
    # machine_name: dashboard link
    dashboard_links = md.dashboard_links(mm)

    mb = MachinesBackend()

    #import pi_control
    #import configparser
    #printer_info = configparser.ConfigParser()
    #printer_info.read('pi_infos.ini')
    #printer_info.read('grafana_config.ini')
    #api_key = printer_info['default']['api_key']
    #default = printer_info['DEFAULT']
    #username = default['username']
    #password = default['password']
    
    t = Thread(target=connect_to_server, args=(), daemon=True)
    t.start()

    #uvicorn.run(app, host='0.0.0.0', port=8000)
    uvicorn.run(app, host=host, port=port)

