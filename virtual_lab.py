'''
Runs pi_server_api many times to create a virtual laboratory with many machines.

TODO: Git add this...
'''

import sys
import json
import os
import os.path
import subprocess as sp
import copy
import argparse
from threading import Thread
import time


def generate_lab(lab_architecture, lab_folder, remote_lab=False):

    if os.path.exists(lab_folder):
        raise Exception(f'Lab folder {lab_folder} already exists!')

    # Create the lab folder:
    os.mkdir(lab_folder)
    machines_folder = os.path.join(lab_folder, 'machines')
    # os.mkdir(machines_folder)

    # Copy the files that can be copied:
    sp.call(['touch', os.path.join(lab_folder, 'home_test')])
    code_files = ['pi_server_api.py', 'iot_influxdb.py', 'misc.py', 'newapi.py', 'threeamigosrpc.py']
    for fname in code_files:
        sp.call(['cp', fname, os.path.join(lab_folder, fname)])
    sp.call(['cp', '-R', 'machines', machines_folder])
    sp.call(['cp', '-R', 'camera_types', os.path.join(lab_folder, 'camera_types')])

    # Generate config.json:
    with open('remote_config.json') as f:
        principal_config = json.loads(f.read())
    if not remote_lab:
        principal_config['influxdb_instances']['local']['url'] = 'http://localhost:8086'
        principal_config['amigos_control'] = 'http://localhost:10000'
        principal_config['amigos_images'] = 'http://localhost:10001'
    with open(os.path.join(lab_folder, 'config.json'), 'w') as f:
        f.write(json.dumps(principal_config, indent=2))

    lab_info = {}

    machine_info_files = {}
    for i, (pi_name, machine_entries) in enumerate(lab_architecture.items()):
        
        machines = []
        pi_config = {}

        machines_info = {'name': pi_name, 'machines': machines, 'config': pi_config}

        pi_config['cameras'] = {'pi_cam1': {'type': 'virtcam', 'options': {'fname': '../test_algo/trial_10_%s.jpg'}}}

        for machine in machine_entries:

            machine_info = copy.deepcopy(machine)

            machine_config = machine_info.get('config', {})
            
            machine_config['home_folder'] = 'New Sim Homepager'

            cameras = machine_config.get('cameras', {})
            cameras['pi_cam'] = 'pi_cam1'
            machine_config['cameras'] = cameras

            machine_config['cam_fname'] = '../test_algo/trial_15_%s.jpg'

            machine_info['config'] = machine_config
            #machine_info['name'] = machine['name']
            #machine_info['type'] = machine['type']
            #machine_info['dash_name'] = machine['dash_name']
            #machine_info['config'] = copy

            machines.append(machine_info)

        # Generate machines_info.json:
        machines_fname = f'machine_info_{i}.json'

        with open(os.path.join(lab_folder, machines_fname), 'w') as f:
            f.write(json.dumps(machines_info, indent=2))

        lab_info[pi_name] = machines_fname

        #machine_info_files[pi_name] = ...

    with open(os.path.join(lab_folder, 'lab_info.json'), 'w') as f:
        f.write(json.dumps(lab_info, indent=2))

def run_lab(lab_folder, wait=False):
    os.chdir(lab_folder)

    with open('lab_info.json') as f:
        lab_info = json.loads(f.read())

    commands = []

    for pi_name, machines_fname in lab_info.items():

        command = ['python3', 'pi_server_api.py', '-f', machines_fname]

        commands.append(command)

    '''
    if len(commands) == 1:
        sp.call(commands[0])
        return
    '''

    threads = []

    for command in commands:
        print(f'Running {command}...')
        t = Thread(target=sp.call, args=(command,), daemon=True)
        t.start()
        threads.append(t)
        
        if wait:
            time.sleep(30)  # input('Press enter to connect the next machine...')

    print('Waiting for threads to complete...')
    for t in threads:
        t.join()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog=sys.argv[0], description='Connect to IoT Capstone monitoring system as any number of virtual monitoring devices.')
    parser.add_argument('-o', '--lab_folder', dest='lab_folder', help='Location of the output lab folder that will be cleared and regenerated.', default='virtual_lab')
    parser.add_argument('-f', '--lab_config', dest='lab_config', help='Location of the input lab config file to specify the lab structure.', default='lab_config.json')
    parser.add_argument('--regenerate', dest='regenerate_lab', help='Delete and regenerate the lab folder if it already exists.', action='store_true')
    parser.add_argument('--run', dest='run_lab', help='Run the lab, off by default.', action='store_true')
    parser.add_argument('--remote', dest='remote_lab', help='Connect to the lab remotely instead of locally.', action='store_true')
    parser.add_argument('--wait', dest='wait', help='Wait for user input before connecting each machine.', action='store_true')
    parser.set_defaults(wait=False)
    parser.set_defaults(regenerate_lab=False)
    parser.set_defaults(run_lab=False)
    parser.set_defaults(remote_lab=False)

    args = parser.parse_args()

    # TODO: Create machines/virtual_makerbot.py and machines/virtual_makerbot.json
    # TODO: Load lab architecture from config file...
    lab_folder = args.lab_folder
    remote_lab = args.remote_lab

    lab_config_file = args.lab_config

    print('Regenerate lab?', args.regenerate_lab)
    print('Remote lab?', args.remote_lab)
    print('Run lab?', args.run_lab)
    print('Lab folder:', lab_folder)
    print('Lab config:', lab_config_file)
    if input('Proceed with these options (y/n)? ') != 'y':
        print('Quitting...')
        exit(1)
    
    if not os.path.exists(lab_config_file):
        print('File not found:', lab_config_file)
        exit(1)


    # lab_architecture = {'Sim Pi 001': [{'name': 'Sim Printer 001', 'type': 'Sim Makerbot Printer', 'dash_name': 'New Sim Printer 1', 'config': {'folder': 'New Sim Printers'}}]}
    with open(lab_config_file) as f:
        lab_architecture = json.loads(f.read())

    do_the_gen = False
    if not os.path.exists(lab_folder):
        do_the_gen = True
    elif args.regenerate_lab:
        sp.call(['rm', '-R', lab_folder])
        do_the_gen = True

    if do_the_gen:
        print('Generating lab...')
        generate_lab(lab_architecture, lab_folder, remote_lab=remote_lab)
        print('Lab generated.')

    # Run it:
    if args.run_lab:
        print('Running lab...')
        run_lab(lab_folder, wait=args.wait)

    if not do_the_gen and not args.run_lab:
        print('Nothing to do.')


