# Makerbot Replicator+ 3D Printer

# A note on controlling multiple printers with one pi when using Klipper:
# https://github.com/AbomShep/-KlipperInfo/blob/master/helpful-tips-1/multiple-klipper-boards-on-one-pi.md
from fastapi import FastAPI, Request, Body
import numpy as np
import traceback
# Not sure if the .. is necessary below, need to test!
from misc import SEC, MIN, HOUR
from misc import is_server
# TODO: Remote this, better to import when used in the class...
if is_server():
    # We are importing for the server...
    pass
else:
    # We are importing for the pi...
    import newapi
    pass
    
import numpy.linalg as la
import cv2 as cv
from skimage import measure
import time
from threading import Thread, Semaphore
import os
import os.path
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime

# Do not include spaces (some .splits() in places grab the first word by spaces, and expect it to be this...)
machine_type = 'Makerbot Printer'
name_prefix = 'Printer'

# These tags can be referenced in dashboard modifications to filter machines to display and grab data for in specific panels.
tags = ['printer', 'machine']


# Need code for modifying the main dashboard panels related to these machines.
# (what about other kinds of printers, should there be categories? I would prefer something more dynamic, like tag classes that deal with panels that can group the machines in arbitrary, including overlapping, ways)


REF_LINE_THRESH = 1  # These are numbers out of 255
TOP_CUTOFF = 285  # From top of the frame
BOTTOM_CUTOFF = 40  # From ref line up
INTENSITY_THRESHOLD = 100  # The threshold on the difference picture to determine   whether a pixel is different
PIXEL_GROUP_NUM_THRESH = 500  # Threshold for number of pixels that have to be different before the print is considered to have fallen off

ARMED = False
DETONATE = True

# TODO: Put in misc
class SelfDestructException(Exception):
    pass

# TODO: Inherit and appropriate class from misc...
class FallOffDetection:
    
    def __init__(self, influx_writer, camera_man, machine_info, config_options, *args):

        self.camera_name = config_options.get('c_d_cam', 'pi_cam1')
        #self.tracked_image = camera_man.get_tracked_image(self.camera_name)
        self.camera_reader = camera_man.get_camera_reader(self.camera_name)
        if self.camera_reader is None:
            raise Exception(f'Could not find camera stream for: {self.camera_name}...')

        #self.img_c = -1

        machine_name = machine_info['name']
        self.destruct = ARMED

        self.lastest_image = None
        self.influx_writer = influx_writer
        self.printer_name = machine_name


    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def picture_preview(self,name):
        img = self.camera_reader.new_and_latest_image(callback=self.destruct_check)
           
        #nparr = np.fromstring(img_str, np.uint8)
        #img_np = cv2.imdecode(nparr, cv2.CV_LOAD_IMAGE_COLOR)
           
        # self.grab_an_image()
        name = 'pic.jpg'
        #cv.imwrite(name, img)
        with open(name,'wb') as f:
            f.write(img)
        img = cv.imread(name)
        print("taken!")
        return img, cv.imread(name, cv.IMREAD_GRAYSCALE)

    def comparison(self, img1, img2):
        MIN_MATCH_COUNT = 10
        #img1 = cv.imread('object.jpg', cv.IMREAD_GRAYSCALE) # queryImage
        #img2 = cv.imread('scene.jpg', cv.IMREAD_GRAYSCALE) # trainImage
        # Initiate SIFT detector
        sift = cv.SIFT_create()
        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1,None)
        kp2, des2 = sift.detectAndCompute(img2,None)
        #print(kp1)
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks = 50)
        flann = cv.FlannBasedMatcher(index_params, search_params)
        if des1 is None or des2 is None or len(des1) == 0 or len(des2) == 0:
            return 0
        matches = flann.knnMatch(des1, des2, k=2)
        # store all the good matches as per Lowe's ratio test.
        good = []
        for m,n in matches:
            if m.distance < 0.7*n.distance:
                good.append(m)
        #print(good)
        above_thresh_sum = 0
        count = 0
        for match in good:
            p1 = kp1[match.queryIdx].pt
            p2 = kp2[match.trainIdx].pt
            val = ((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)**0.5
            if val >= 1:
                above_thresh_sum += val
                count += 1
            print()
        av_move = above_thresh_sum/count
        print('Average move:', av_move)

        if len(good)>MIN_MATCH_COUNT:
             src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
             dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
             M, mask = cv.findHomography(src_pts, dst_pts, cv.RANSAC,5.0)
             matchesMask = mask.ravel().tolist()
             h,w = img1.shape
             pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
             dst = cv.perspectiveTransform(pts,M)
             img2 = cv.polylines(img2,[np.int32(dst)],True,255,3, cv.LINE_AA)
        else:
            print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
            matchesMask = None

        draw_params = dict(matchColor = (0,255,0), # draw matches in green color
            singlePointColor = None,
            matchesMask = matchesMask, # draw only inliers
            flags = 2)
        img3 = cv.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

            #return(len(good))

    def comparison_correct(self, img1, img2):
        size = min(np.shape(img1)[0], np.shape(img2)[0])
        img1 = img1[-size:-1, :]
        img2 = img2[-size:-1, :]


        diff = img1.astype(np.int16) - img2.astype(np.int16)
        print(np.min(diff), np.max(diff), diff)
        diff_show = np.abs(diff)
        diff_thresh = cv.threshold(diff_show, INTENSITY_THRESHOLD, 255, cv.THRESH_BINARY)[1]
        diff_thresh = cv.erode(diff_thresh, None, iterations=2)
        diff_thresh = cv.dilate(diff_thresh, None, iterations=4)
        labels = measure.label(diff_thresh, background=0)
        print(np.bincount(np.ndarray.flatten(labels)))

        avg = la.norm(diff, axis = 1)
        #print(avg.shape)
        if len(np.bincount(np.ndarray.flatten(labels))) > 1:
            return max(np.bincount(np.ndarray.flatten(labels))[1:])
        return 0

    def run(self):
   
        while True:
            #self.destruct_check()
            img1_colour, img1 = self.picture_preview("current0.jpg")
            j = 1
            img1_colour = img1_colour[TOP_CUTOFF: , :, :]
            img1 = img1[TOP_CUTOFF: , :]
            img_height = img1_colour.shape[0]
            line_pixel = None
            index = None
            red_avg = None
            for i in range(img_height-1, -1, -1):
                red_avg = np.mean(img1_colour[i, :, 2]) - np.mean(img1_colour[i, :, 0])/2 - np.mean(img1_colour[i, :, 1])/2
                if red_avg > REF_LINE_THRESH:
                    index = i
                    line_pixel = red_avg
                    break
            if index is None:
                print("Cant find reference line! Trying again...")
                #time.sleep(5)
                self.destructible_sleep(5)
                #exit(0)
                continue
            img1_colour = img1_colour[0:index -BOTTOM_CUTOFF, :]
            img1 = img1[0:index - BOTTOM_CUTOFF, :]
            break

        while True:
            img2_colour, img2 = self.picture_preview(f"current{j}.jpg")
            img2_colour = img2_colour[TOP_CUTOFF: , :, :]
            img2 = img2[TOP_CUTOFF: , :]
            img_height = img2_colour.shape[0]
            line_pixel = None
            index = None
            for i in range(img_height-1, -1, -1):
                red_avg = np.mean(img2_colour[i, :, 2]) - np.mean(img2_colour[i, :, 0])/2 - np.mean(img2_colour[i, :, 1])/2
                if red_avg > REF_LINE_THRESH:
                    index = i
                    line_pixel = red_avg
                    break
            #print(line_pixel)
            if index is not None:
                if index - BOTTOM_CUTOFF > 0:
                    img2_colour = img2_colour[0:index - BOTTOM_CUTOFF, :]
                    img2 = img2[0:index - BOTTOM_CUTOFF, :]
         
                    indicator = self.comparison_correct(img1, img2)
                    print(indicator)
                    p = influxdb_client.Point(self.influx_writer.measurement_name) \
                        .tag("Name", self.printer_name) \
                        .field("Fall", 0) 
                    if indicator > PIXEL_GROUP_NUM_THRESH:
                        print("it fell off!")
                        p = influxdb_client.Point(self.influx_writer.measurement_name)\
                        .tag("Name", self.printer_name) \
                        .field("Fall", 1) 
                    self.influx_writer.write_influx(p)
                    img1_colour, img1 = img2_colour, img2
                    j += 1
            else:
                print("Reference line lost")
            t = time.time()
            while time.time() - t < 30:
                key = cv.waitKey(10) & 0xFF
                self.destruct_check()
                

STEP_NUMBERS = {'idle': 0,
                'clear_build_plate': 7,
                'initial_heating': 1,
                'homing': 2,
                'transfer': 10,
                'final_heating': 3,
                'printing': 4,
                'cleaning_up': 5,
                'completed': 6,
                'cooling': 8,
                'cancelling': 9,
                'verify_firmware': 11,
                'writing': 12,
                'suspended': 13,  # 0
                'suspending': 14,
                'unloading_filament': 15,
                'handling_recoverable_filament_jam': 16,
                'preheating_resuming': 17,
                }
# extrusion, 


# MAIN_CONFIG_FILE = 'pi_setup.ini'

# TODO: Inherit and appropriate class from misc...
class PiDataCollection:

    program_name = 'pi_data_collection'
    def __init__(self, influx_writer, camera_man, machine_info, config_options, *args):
        machine_name = machine_info['name']
        self.destruct = ARMED

        self.camera_name = config_options.get('cam_name', 'main_cam')
        self.camera_man = camera_man

        self.influx_writer = influx_writer
        self.printer_name = machine_name

        # send_to_server = config['DEFAULT']['send_to_server']
        self.broadcast_interface = machine_info.get('config', {}).get('broadcast_interface')
        #self.authentication_code = config_options.get('auth_token')
        self.authentication_code = machine_info.get('config', {}).get('auth_token')
        self.send_to_server = True
        #self.program_type = program_type
    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def makerbot_printer_send(self):
        machine = self.makerbot
        name = self.printer_name
        
        try:
            #image_num = 0
            prev_step = -1
            while True:
                print('Getting system info...')
                # Retrieve data from system
                data = machine.get_system_information()
                
                print("Got system info")
                print(data["result"])#["machine_name"])
                if not self.send_to_server:
                    #time.sleep(30)
                    self.destructible_sleep(30)
                    continue
                current_process = data['result']['current_process']
                # Send the name of the device and the extruder temperature to influxdb
                p = influxdb_client.Point(self.influx_writer.measurement_name) \
                    .tag("Name", data["result"]["machine_name"]) \
                    .field("Target Temperature", data["result"]["toolheads"]["extruder"][0]["target_temperature"]) \
                    .field("Extruder Temperature", data["result"]["toolheads"]["extruder"][0]["current_temperature"])

                # Set the step to be idle and percentage done to be 0 as default

                step_name = 'idle'
                perc_done = 0

                # if the printer is printing get the step and the percentage done of the job
                if current_process is not None:
                    step_name = current_process['step']
                    perc_done = current_process.get('progress', 0)
                    print_job_name = current_process.get('filepath')
                    if print_job_name is not None:
                        print_job_name = os.path.split(print_job_name)
                        print_job_name = print_job_name[1]
                    time_remaining = current_process.get('time_remaining')
                    time_estimation = current_process.get('time_estimation')
                    filiment_weight = current_process.get('extrusion_mass_g')
                    if filiment_weight is not None:
                        filiment_weight = filiment_weight[0]
                    time_elapsed = current_process.get('elapsed_time')
                    p = p.field('Print Job Name', print_job_name) \
                        .field('Time Remaining', time_remaining) \
                        .field('Time Estimation',  time_estimation) \
                        .field('Fillament Weight',  filiment_weight) \
                        .field('Time Elapsed', time_elapsed) 
                    
                step_num = STEP_NUMBERS[step_name]
                p = p.field("Percentage Done", perc_done) \
                    .field("Step", step_num)
                    
                
                # Get latest image
                #_, width, height, _, yuv_image = machine.get_camera_image()
                #latest_images[name] = (yuv_image, image_num)
                #image_num += 1

                did_send_event = False
                           
                if step_num == STEP_NUMBERS['idle']:
                    tmp = 0
                if step_num == STEP_NUMBERS['printing'] and prev_step != step_num and       prev_step != -1:
                    p = p.field('Job Start Event', 1)
                    p = p.field("Print Event", 1)
                    did_send_event = True
                
                # change STEP_NUMBERS TO "completed" 	
                if step_num == STEP_NUMBERS["completed"] and prev_step != step_num and prev_step != -1: 
                    # machine.save_camera_jpg(f"test_{name}.jpg")
                    p = p.field('Job Finish Event', 1)
                    p = p.field("Print Event", 3)
                    did_send_event = True
                
                if step_num == STEP_NUMBERS['idle'] and prev_step == 5:
                    p = p.field('Print Event', 2)
                    did_send_event = True
               
                if not did_send_event:
                    p = p.field('Print Event', 0)


                # if the printer is not printing send data every 30 seconds
                if current_process is None:
                    #await asyncio.sleep(30)
                    #time.sleep(30)
                    self.destructible_sleep(30)

                # if the printer is printing send data every 5 seconds
                else:
                    #await asyncio.sleep(5)
                    #time.sleep(5)
                    self.destructible_sleep(5)

                print('Waiting to send to influxdb...')
                # write the data into influxdb

                self.influx_writer.write_influx(p)
                print('sent')
                #print(p)
                prev_step = step_num

        except Exception as e:
            if isinstance(e, SelfDestructException):
                self.turn_printer_off()
                print('MAKERBOT CONTINUE RAISE:')
                raise e
            else:
                print('MAKERBOT EXCEPTION:')
                print(traceback.format_exc())
        try:
            self.turn_printer_off()
        except Exception as e:
            print(traceback.format_exc())

    def turn_printer_off(self):
        machine = self.makerbot
        name = self.printer_name
        print(name, 'is being turned off!')
        #del current_printers[name]
        print(name, "has turned off!")
        try:
            machine.finish()
        except Exception as e:
            print(traceback.format_exc())
        print(name, "is finished!")
        p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", self.printer_name)
        p = p.field("Step", -1)
        self.influx_writer.write_influx(p)

    def run(self):

        #self.
        self.ready = False
        other_self = self

        class Camera:
            def __init__(self, options):
                pass  # self.last_image_num = -1
            def get_frame(self):
                while not other_self.ready:
                    other_self.destructible_sleep(0.05)
                _, width, height, _, yuv_image = other_self.makerbot.get_camera_image()
                #self.latest_images[self.name] = (yuv_image, image_num)
                #image_num += 1

                #image_data, image_num = latest_images[self.printer_name]
                '''
                if image_num > self.last_image_num:
                    #time.sleep(0.05)
                    self.destructible_sleep(0.05)
                    continue
                '''
                #self.last_image_num = image_num
                return yuv_image

        self.camera_man.kill_camera(self.camera_name)
        camera_options = {}
        pi_camera_name = f'{self.camera_name}_{self.printer_name}'
        self.camera_man.add_camera(pi_camera_name, Camera, camera_options, (self.printer_name, self.camera_name, self.program_type))
        
        if self.authentication_code is None:
            # This is a problem!
            print(f'No authentication code configured for Makerbot printer {self.printer_name}, quitting process...')
            return

        print('Sending off...')
        name = self.printer_name
        p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", name) \
                .field("Extruder Temperature", 0) \
                .field("Target Temperature", 0) \
                .field("Percentage Done", 0) \
                .field("Step", -1)

        self.influx_writer.write_influx(p)
        print(f'We have declared that printer {self.printer_name} is off!')

        #print('This is the authenticaton dict', self.authentication_dict)
        discovery = newapi.MakerbotDiscovery(device=self.broadcast_interface)
        print('Start')

        while True:
            found = discovery.discover(delay_broadcast = True)
            print('Found:', found)
            for ip, name, identity in found:
                if name == self.printer_name:
                    break
            else:  # No break
                continue

            try:
                print('Found printer:', name)

                self.makerbot = newapi.MakerbotAPI(ip, self.authentication_code)
                print('Connecting...', ip)
                self.makerbot.connect()
                print('Authenticating...')
                self.makerbot.authenticate_json_rpc()
                print('Getting system info...')
                self.ready = True

                self.makerbot_printer_send()
            except Exception as e:
                # print(traceback.format_exc())
                if isinstance(e, SelfDestructException):
                    print('MAKERBOT RUN CONTINUE RAISE:')
                    raise e
                else:
                    print('MAKERBOT RUN EXCEPTION:')
                    print(traceback.format_exc())

# These run as separate programs on the pi...
pi_programs = {'fall_off': FallOffDetection, 'printer_data': PiDataCollection}


pi_apt_reqs = []
pi_pip_reqs = []

class MachineOff:
    def __init__(self, influx_writer, image_streaming_wrangler, pi_name, machine_info, config_options, found_machines, found_pis, *args):
        machine_name = machine_info['name']
        self.destruct = ARMED

        self.influx_writer = influx_writer
        self.machine_name = machine_name
        self.isw = image_streaming_wrangler

        self.found_machines = found_machines
        self.found_pis = found_pis

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def run(self):
        while True:
            print(f'Checking if {self.machine_name} is off...')

            printer_is_on = self.machine_name in self.found_machines

            if True or not printer_is_on:

                print('Off machine:', self.machine_name, 'has had job event 0.')
                p = influxdb_client.Point(self.influx_writer.measurement_name) \
                        .tag("Name", self.machine_name) \
                        .field("Print Event", 0) \
                        .field("Step", -1)
                p = p.field('Job Start Event', 0)
                p = p.field('Job Finish Event', 0)
                # Send that the printer
                self.influx_writer.write_influx(p)

            else:
                print('Skipped printer', self.machine_name)

            self.destructible_sleep(0.5*HOUR)
            
# TODO: Inherit and appropriate class from misc...
class TakePicture:
    def __init__(self, influx_writer, image_streaming_wrangler, pi_name, machine_info, config_options, found_machines, found_pis, *args):
        machine_name = machine_info['name']
        self.destruct = ARMED

        self.influx_writer = influx_writer
        self.machine_name = machine_name
        self.isw = image_streaming_wrangler

        self.found_machines = found_machines
        self.found_pis = found_pis
        
        self.completed_prints_path = 'completed_prints/'

        if not os.path.exists(self.completed_prints_path):
            os.mkdir(self.completed_prints_path)

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)
        
    def take_picture(self, device_name):
        query_events = f'from(bucket:"{self.influx_writer.bucket}")\
        |> range(start: -1m)\
        |> filter(fn:(r) => r._measurement == "{self.influx_writer.measurement_name}")\
        |> filter(fn:(r) => r._field == "Print Event")\
        |> filter(fn:(r) => r.Name == "{device_name}")'
        events_table = self.influx_writer.query_api.query(org=self.influx_writer.org, query=query_events)
        take_pic = False
        for table in events_table:
            for record in table.records:
                if record.get_value() == 3:
                    take_pic = True
                    time_stamp = record.get_time()
                    print('Taken!')
        if take_pic:
            tracked_image = self.isw.latest_images[device_name]['main_cam']
            image_data = tracked_image.image
            
            f = open(f'{self.completed_prints_path}{device_name}_{time_stamp}.jpg', 'wb')
            f.write(image_data)
            f.close()

    def run(self):
        while True:
            print(f'checking for finished prints by {self.machine_name}')
            #for pi_name in self.isw.latest_images:
            self.take_picture(self.machine_name)
            #time.sleep(30)
            self.destructible_sleep(30)


# This runs in a more integrated way... How to deal with multiple of the same machine,
# cannot have two screen sessions with the same name... Likely will not
# use screen?
# In pi_manager_backend, restart these on pi connect for each machine.
server_programs = {'save_pics': TakePicture, 'machine_off': MachineOff}

server_apt_reqs = []
server_pip_reqs = []

# 1. TODO: On server, option to run one per tag or per specific machine instance. (e.g. taking pictures of completed prints could apply to tag 'Printer', some customised code and still be used per machine here though...)
# 2. TODO: Figure out way to print to different log files or screen sessions here...

# Should be provided by machines with tag 'printer', so that the homepage can display all of these queries
def printer_stat_query():
    pass

# Use the pictures taken above
def pdf_report_section():
    pass

# TODO: Put this in misc
def remove_fast_api_callback(app, url):
    while True:
        did_delete = False
        for i, route in enumerate(app.router.routes):
            if route.path_format == url:
                print('Found and removed url api endpoint on server:', repr(url))
                del app.router.routes[i]
                break
        else:  # No break:
            break

def pi_server_api_callback(app, psapi):
    # Unload old:
    @app.post('/setup/makerbot/new_auth_token')
    def func(params):
        params = params['data']
        pi_name = params['parent']
        machine_name = params['name']

        machines = psapi.machines_info['machines']
        i = psapi.find_machine_index(machine_name)
        if i is None:
            print('AUTH GET: No such machine found:', machine_name)
            return
        machine_info = psapi.machines[i]

        print(f'Getting authentication token for {machine_name}, likely for configuration...')
        broadcast_interface = machine_info.get('config', {}).get('broadcast_interface')
        discovery = newapi.MakerbotDiscovery(device=broadcast_interface)
        t = time.time()
        found = False
        while time.time() - t <= 30:
            found = discovery.discover(wait = 2, delay_broadcast=False)
            print('Found printers:', found)
            for ip, name, identity in found:
                if name == machine_name:
                    break
            else:  # No break
                continue
            found = True
            break
        if not found:
            print('Did not discover printer!')
            return {'token': None, 'error': 'printer not discovered'}
        makerbot = newapi.MakerbotAPI(ip)
        print('Connecting...', ip)
        makerbot.connect()
        print('Authenticating...')
        try:
            makerbot.authenticate_fcgi(timeout=5)
        except:
            print('Error during authenticate:', traceback.format_exc())
            return {'token': None, 'error': 'at authenticate'}
        auth_code = makerbot.auth_code
        print(f'Found token for {machine_name}:', auth_code)
        return {'token': auth_code}

def pi_manager_backend_api_callback(app, rpi_api):
    # Note there will be no mb.thing for it, so the requests session post will need to be performed.
    # (or an mb.named_post('name_here', ...) function to do that?)
    remove_fast_api_callback(app, '/setup/makerbot/new_auth_token')
    print('JHFDSZKGLSHDFLKJGHDSFLKJGHDSLKJFHGK')
    def func(params : dict = Body(...)):
        print('Here are the params', params)
        pi_name = params['parent']
        machine_name = params['name']
        response = rpi_api.post(pi_name, '/setup/makerbot/new_auth_token', json = params)
        return response
    app.add_api_route('/setup/makerbot/new_auth_token', func, methods=["POST"])

'''
def initial_setup():
    pass
'''

# TODO: Initial setup callback in machine module to do things like prompt to user to hit enter and then store the authentication code.
#       (if there are steps to follow, the user needs to receive the instructions somewhere...)
def setup_streamlit_page(st, mb, config, pi_name, machine_name, machine_type, dash_name, row):

    # NOTE: To go home: st.session_state['page'] = None

    # TODO: Make API call to change config value...
    # mb.replace_pi_config(pi_name, new_config_json)

    st.write('Press authenticate to retrieve a new authentication token for the Makerbot printer. After pressing this button, refresh to when authenticated on the printer itself by pressing the button on the Makerbot machine, then the auth token should appear below. Then, press "Update Config" to commit this authentication token to the machine configuration.')

    # TODO: Allow another name in the config as an input here for choosing the name of the makerbot to target...

    def request_authenticate():
        params = {'parent': pi_name, 'name': machine_name}
        print('Sending data to get auth token:', params)
        result = mb.named_post('/setup/makerbot/new_auth_token', params)['data']
        print('Authenticate response:', result)
        auth_token = result['token']
        st.session_state['makerbot_auth_token'] = auth_token
    st.button('Request Authentication', on_click=request_authenticate, key='request_authenticate')
    # NOTE: For now, there will be a hang in the web page. Better to permit the pi to send data back at a later time, and poll for that data here?
    #       (TODO: a timeout here at least would be appropriate, something like 20 seconds...)

    auth_token = st.session_state.get('makerbot_auth_token')
    # result = mb.named_post('/setup/makerbot/poll_new_auth')
    # auth_token = result['token']

    if auth_token is None:
        # At least show the auth token when it is configured:
        auth_token = config.get('auth_token', None)

    # WRONG CONFIG, this is for pi...
    # st.write(config)
    if auth_token is not None:
        st.write(f'Authentication token: {auth_token}')
    else:
        st.write('No authentication configured.')

    def update_config():
        if auth_token is None:
            print('Cannot update blank authentication token!')
            return
        #result = mb.named_post('/setup/makerbot/', json={})
        config['auth_token'] = auth_token
        mb.replace_config(pi_name, machine_name, config)
    st.button('Update Config', on_click=update_config, key='setup_new_config')

