
from fastapi import FastAPI, Request, Body
import traceback
import numpy as np
from misc import SEC, MIN, HOUR
from misc import is_server
if is_server():
    # We are importing for the server...
    pass
else:
    # We are importing for the pi...
    pass

import time
import os
import os.path
import influxdb_client
from datetime import datetime
from misc import ARMED, DETONATE
import random

machine_type = 'Sim Makerbot Printer'

tags = ['sim printer']

# TODO: Put in misc
class SelfDestructException(Exception):
    pass

step_numbers = {'idle': 0,
                'off': -1,
                'clear_build_plate': 7,
                'initial_heating': 1,
                'homing': 2,
                'transfer': 10,
                'final_heating': 3,
                'printing': 4,
                'cleaning_up': 5,
                'completed': 6,
                'cooling': 8,
                'cancelling': 9,
                'verify_firmware': 11,
                'writing': 12,
                'suspended': 13,  # 0
                'suspending': 14,
                'unloading_filament': 15,
                'handling_recoverable_filament_jam': 16,
                'preheating_resuming': 17,
                }

print_events = {'default': 0, 'start': 1, 'cancel': 2, 'finish': 3}

closing_time = 12*HOUR  # 8*HOUR

START_DAY_OFF_WAIT = lambda t: 1*MIN  # 120*MIN
LATE_DAY_OFF_WAIT = lambda t: 1*MIN  # (closing_time - t) - 60*MIN
END_DAY_OFF_WAIT = lambda t: 1*MIN  # 120*MIN 

IDLE_MIN_WAIT = lambda t:1*MIN
IDLE_WAS_OFF_MAX_WAIT = lambda t: 1*MIN  # 20*MIN
IDLE_WAS_ON_MAX_WAIT = lambda t: 1*MIN  # 40*MIN

MIN_PRINT_TIME = lambda t: 1*MIN  # 20*MIN
MAX_PRINT_TIME = lambda t: 10*MIN  # 180*MIN

CANCEL_MIN_WAIT = lambda t: 20*SEC
CANCEL_MAX_WAIT = lambda t: 90*SEC

HOMING_TIME = 30*SEC

MIN_COMPLETION_TIME = lambda t: 1*MIN
MAX_COMPLETION_TIME = lambda t: 2*MIN  # 20*MIN


# TODO: Inherit and appropriate class from misc...
class SimDataCollection:

    def __init__(self, influx_writer, camera_man, machine_info, config_options, *args):
        pass

        machine_name = machine_info['name']
        self.destruct = ARMED

        self.camera_name = config_options.get('cam_name', 'main_cam')
        self.camera_man = camera_man

        self.influx_writer = influx_writer
        self.printer_name = machine_name

        machine_config = machine_info.get('config', {})

        self.fname_format = machine_config['cam_fname']

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def send_data(self, current_temp=0, target_temp=0, progress=0, job_name=None, time_remaining=None, time_estimation=None, time_elapsed=None, event='default', step='off'):

        if step not in step_numbers:
            raise Exception(f'Unrecognised step: {step}.')

        step = step_numbers[step]

        name = self.printer_name
        p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", name) \
                .field("Extruder Temperature", current_temp) \
                .field("Target Temperature", target_temp) \
                .field("Percentage Done", progress) \
                .field("Step", step)


        if job_name is not None:
            p = p.field('Print Job Name', jon_name)
        if time_remaining is not None:
            p = p.field('Time Remaining', time_remaining)
        if time_estimation is not None:
            p = p.field('Time Estimation',  time_estimation)
        if time_elapsed is not None:
            p = p.field('Time Elapsed', time_elapsed) 

        p = p.field('Print Event', print_events[event]) 

        if event == 'start':
            p = p.field('Job Start Event', 1)
        elif event == 'finish':
            p = p.field('Job Finish Event', 1)

        self.influx_writer.write_influx(p)

    def old_state_machine(self):

        step = 'off'
        step_wait = 0
        step_t = time.time()

        # Do a simple loop of the machine state:
        while True:
            
            print('Sending data:')

            self.send_data(current_temp=0, target_temp=0, progress=0, event='default', step=step)

            t = time.time()
            elapsed = t - step_t
            delay = step_wait - elapsed
            if delay <= 0:

                print('Moving to next step:')

                if step == 'off':
                    step = 'idle'
                    step_wait = random.randint(30, 180)
                elif step == 'idle':
                    step = 'off'
                    step_wait = random.randint(10, 60)

                print(f'Step {step} will last {step_wait}s.')

                step_t = t
            else:
                print('Time until next state:', delay)

            print('Waiting...')

            self.destructible_sleep(5)

    def run(self):
    
        print('[PROGRAM] ENTER')

        other_self = self

        class Camera:
            def __init__(self, options):
                self.frame_i = 1
                #self.fname_format = fname_format
            def get_frame(self):
                prev_fname = None
                while True:
                    fname = other_self.fname_format % self.frame_i
                    if os.path.exists(fname):
                        break
                    if prev_fname == fname:
                        raise Exception(f'DID NOT FIND IMAGE: {fname}')
                    self.frame_i = 1
                    prev_fname = fname
                # print('Sending virt main cam image:', fname)
                time.sleep(1/2)
                if fname is not None:
                    self.frame_i += 1
                    with open(fname, 'rb') as f:
                        return f.read()
        print('[PROGRAM] CAMERA')
        self.camera_man.kill_camera(self.camera_name)
        camera_options = {}
        pi_camera_name = f'{self.camera_name}_{self.printer_name}'
        self.camera_man.add_camera(pi_camera_name, Camera, camera_options, (self.printer_name, self.camera_name, self.program_type))
        
        print('[PROGRAM] STATE MACHINE')

        # Send that it is off:
        #print('Sending off...')
        #self.send_data(step='off')
        #print('Sent off.')

        # self.old_state_machine()
        self.new_state_machine()

    def new_state_machine(self):
        next_func, next_args = self.off_status, (self.printer_name, 0, 0)
        while next_func is not None:
            print('Next:', next_func, next_args)
            next_func, next_args = next_func(*next_args)
            # Fix where invoked to be a return, and then fix returns to do something else, like return None?
        print('Next func was None! Quitting...')

    # Added self arguments and self. to calls. Added self destructible sleep as well.
    # TODO: Decrease times

    def off_status(self, printer_name, print_num, current_time):
        p = influxdb_client.Point(self.influx_writer.measurement_name) \
            .tag("Name", printer_name) \
            .field("Fall", 0)
        self.influx_writer.write_influx(p)
        if (closing_time - current_time) > (120+60)*MIN:
            wait_time = random.randint(0*MIN, START_DAY_OFF_WAIT(current_time))
        elif (closing_time - current_time) > 70*MIN:
            wait_time = random.randint(0*MIN, LATE_DAY_OFF_WAIT(current_time))
        else:
            wait_time = END_DAY_OFF_WAIT(current_time)
        step = -1
        p = influxdb_client.Point(self.influx_writer.measurement_name) \
            .tag("Name", printer_name) \
            .field("Step", step) \
            .field("Print Event", 0)
        self.influx_writer.write_influx(p)
        # print(wait_time)
        time_counter = 0
        while time_counter < wait_time:
            time_counter += 5
            current_time += 5
            self.destructible_sleep(5)
        # print("moving to idle")
        if (closing_time - current_time) > 40*MIN:
            return self.idle_status, (printer_name, True, 21, print_num, current_time)
        else:
            return self.off_status, (printer_name, print_num, current_time)


    def idle_status(self, printer_name, was_off, current_temp, print_num, current_time):
        p = influxdb_client.Point(self.influx_writer.measurement_name) \
            .tag("Name", printer_name) \
            .field("Fall", 0)
        self.influx_writer.write_influx(p)
        if was_off:
            wait_time = random.randint(IDLE_MIN_WAIT(current_time), min(IDLE_WAS_OFF_MAX_WAIT(current_time), (closing_time - current_time)))
        else:
            wait_time = random.randint(IDLE_MIN_WAIT(current_time), min(IDLE_WAS_ON_MAX_WAIT(current_time), (closing_time - current_time)))
        # print(wait_time)
        time_counter = 0
        while time_counter < wait_time:
            step = 0
            if current_temp > 21:
                current_temp -= round(max(1, (current_temp - 21) / 16) + random.normalvariate(0, 1))

            p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", printer_name) \
                .field("Step", step) \
                .field("Extruder Temperature", current_temp)
            self.influx_writer.write_influx(p)
            time_counter += 5
            current_time += 5
            self.destructible_sleep(5)
        next_state = random.randint(1, 100)
        if (next_state < 40 and not was_off) or (closing_time - current_time) <= 40*MIN:
            # print("moving to off")
            return self.off_status, (printer_name, print_num, current_time)
        else:
            # print("moving to printing")
            return self.printing_status, (printer_name, current_temp, print_num, current_time)


    def printing_status(self, printer_name, current_temp, print_num, current_time):
        print_num += 1

        print_name = printer_name + "_print_" + str(print_num)

        p = influxdb_client.Point(self.influx_writer.measurement_name) \
            .tag("Name", printer_name) \
            .field("Print Event", 1)
        self.influx_writer.write_influx(p)
        print_time = random.randint(MIN_PRINT_TIME(current_time), min(MAX_PRINT_TIME(current_time), (closing_time - current_time) - 10*MIN))
        # print(print_time)
        time_counter = 0
        fall_value = random.randint(1, 100)
        fall_time = random.randint(20, 85)
        if fall_value <= 25:
            cancellation_value = 100
        else:
            cancellation_value = random.randint(1, 100)
        cancellation_time = random.randint(CANCEL_MIN_WAIT(current_time), CANCEL_MAX_WAIT(current_time))
        while current_temp < 180:
            current_temp += round(7 + random.normalvariate(0, 1))
            step = 1
            p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", printer_name) \
                .field("Step", step) \
                .field("Extruder Temperature", current_temp) \
                .field("Print Job Name", print_name)
            self.influx_writer.write_influx(p)
            current_time += 5
            self.destructible_sleep(5)
        while time_counter < HOMING_TIME:
            step = 2
            current_temp += round(random.normalvariate(0, 1))
            p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", printer_name) \
                .field("Step", step) \
                .field("Extruder Temperature", current_temp) \
                .field("Print Job Name", print_name)
            self.influx_writer.write_influx(p)
            time_counter += 5
            current_time += 5
            self.destructible_sleep(5)
        time_counter = 0
        while current_temp < 215:
            current_temp += round(5 + random.normalvariate(0, 1))
            step = 3
            p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", printer_name) \
                .field("Step", step) \
                .field("Extruder Temperature", current_temp) \
                .field("Print Job Name", print_name)
            self.influx_writer.write_influx(p)
            current_time += 5
            self.destructible_sleep(5)
        while time_counter < print_time:
            step = 4
            current_temp += round(random.normalvariate(0, 2) - (current_temp - 215) / 40)
            p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", printer_name) \
                .field("Step", step) \
                .field("Extruder Temperature", current_temp) \
                .field("Print Job Name", print_name) \
                .field("Elapsed Time", time_counter) \
                .field("Time Remaining", print_time - time_counter) \
                .field("Percentage Done", round(time_counter * 100 / print_time))
            self.influx_writer.write_influx(p)
            time_counter += 5
            current_time += 5
            self.destructible_sleep(5)
            if cancellation_value <= 0 and round(time_counter * 100 / print_time) >= cancellation_time:
                p = influxdb_client.Point(self.influx_writer.measurement_name) \
                    .tag("Name", printer_name) \
                    .field("Print Event", 2)
                self.influx_writer.write_influx(p)
                return self.idle_status, (printer_name, False, current_temp, print_num, current_time)
            if fall_value <= 25 and round(time_counter * 100 / print_time) >= fall_time:
                p = influxdb_client.Point(self.influx_writer.measurement_name) \
                    .tag("Name", printer_name) \
                    .field("Fall", 1)
                self.influx_writer.write_influx(p)
                if round(time_counter * 100 / print_time) >= (fall_time + 10):
                    p = influxdb_client.Point(self.influx_writer.measurement_name) \
                        .tag("Name", printer_name) \
                        .field("Print Event", 2)
                    self.influx_writer.write_influx(p)
                    return self.idle_status, (printer_name, False, current_temp, print_num, current_time)
        p = influxdb_client.Point(self.influx_writer.measurement_name) \
            .tag("Name", printer_name) \
            .field("Print Event", 3)
        self.influx_writer.write_influx(p)
        time_counter = 0
        completion_time = random.randint(MIN_COMPLETION_TIME(current_time), min(MAX_COMPLETION_TIME(current_time), (closing_time - current_time) - 5*MIN))
        while time_counter < completion_time:
            step = 6
            if current_temp > 21:
                current_temp -= round(max(1, (current_temp - 21) / 16) + random.normalvariate(0, 1))
            p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag("Name", printer_name) \
                .field("Step", step) \
                .field("Extruder Temperature", current_temp) \
                .field("Print Job Name", print_name)
            self.influx_writer.write_influx(p)
            time_counter += 5
            current_time += 5
            self.destructible_sleep(5)
        # print("moving to idle")
        self.influx_writer.write_influx(p)
        return self.idle_status, (printer_name, False, current_temp, print_num, current_time)


pi_programs = {'printer_data': SimDataCollection}

pi_apt_reqs = []
pi_pip_reqs = []
# TODO: Completed prints too!


class PrinterOff:
    def __init__(self, influx_writer, image_streaming_wrangler, pi_name, machine_info, config_options, found_machines, found_pis, *args):
        machine_name = machine_info['name']
        self.destruct = ARMED

        self.influx_writer = influx_writer
        self.machine_name = machine_name
        self.isw = image_streaming_wrangler

        self.found_machines = found_machines
        self.found_pis = found_pis

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def run(self):
        while True:
            print(f'Checking if {self.machine_name} is off...')

            printer_is_on = self.machine_name in self.found_machines

            if not printer_is_on:

                print('Off printer:', self.machine_name, 'has had print event 0.')
                p = influxdb_client.Point(self.influx_writer.measurement_name) \
                        .tag("Name", self.machine_name) \
                        .field("Print Event", 0) \
                        .field("Step", -1)

                # Send that the printer
                self.influx_writer.write_influx(p)

            else:
                print('Skipped printer', self.machine_name)

            self.destructible_sleep(0.5*HOUR)


class TakePicture:
    def __init__(self, influx_writer, image_streaming_wrangler, pi_name, machine_info, config_options, found_machines, found_pis, *args):
        machine_name = machine_info['name']
        self.destruct = ARMED

        self.influx_writer = influx_writer
        self.machine_name = machine_name
        self.isw = image_streaming_wrangler

        self.found_machines = found_machines
        self.found_pis = found_pis
        
        self.completed_prints_path = 'completed_prints/'

        if not os.path.exists(self.completed_prints_path):
            os.mkdir(self.completed_prints_path)

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)
        
    def take_picture(self, device_name):
        query_events = f'from(bucket:"{self.influx_writer.bucket}")\
        |> range(start: -1m)\
        |> filter(fn:(r) => r._measurement == "{self.influx_writer.measurement_name}")\
        |> filter(fn:(r) => r._field == "Print Event")\
        |> filter(fn:(r) => r.Name == "{device_name}")'
        events_table = self.influx_writer.query_api.query(org=self.influx_writer.org, query=query_events)
        take_pic = False
        for table in events_table:
            for record in table.records:
                if record.get_value() == 3:
                    take_pic = True
                    time_stamp = record.get_time()
                    print('Taken!')
        if take_pic:
            tracked_image = self.isw.latest_images[device_name]['main_cam']
            image_data = tracked_image.image
            
            f = open(f'{self.completed_prints_path}{device_name}_{time_stamp}.jpg', 'wb')
            f.write(image_data)
            f.close()

    def run(self):
        while True:
            print(f'checking for finished prints by {self.machine_name}')
            #for pi_name in self.isw.latest_images:
            self.take_picture(self.machine_name)
            #time.sleep(30)
            self.destructible_sleep(30)


# This runs in a more integrated way... How to deal with multiple of the same machine,
# cannot have two screen sessions with the same name... Likely will not
# use screen?
# In pi_manager_backend, restart these on pi connect for each machine.
server_programs = {'save_pics': TakePicture, 'printer_off': PrinterOff}

server_apt_reqs = []
server_pip_reqs = []

# Should be provided by machines with tag 'printer', so that the homepage can display all of these queries
def printer_stat_query():
    pass

# Use the pictures taken above
def pdf_report_section():
    pass

def pi_server_api_callback(app, psapi):
    # Unload old:
    '''
    @app.post('/setup/makerbot/new_auth_token')
    def func(params):
        params = params['data']
        pi_name = params['parent']
        machine_name = params['name']
        return {'token': 'something'}
    '''

def pi_manager_backend_api_callback(app, rpi_api):
    # remove_fast_api_callback(app, '/setup/makerbot/new_auth_token')
    def func(params : dict = Body(...)):
        pi_name = params['parent']
        response = rpi_api.post(pi_name, '/setup/makerbot/new_auth_token', json = params)
        return response
    # app.add_api_route('/setup/makerbot/new_auth_token', func, methods=["POST"])

# Initial setup callback in machine module to do things like prompt to user to hit enter and then store the authentication code.
def setup_streamlit_page(st, mb, config, pi_name, machine_name, machine_type, dash_name, row):

    # TODO: Make API call to change config value...
    # mb.replace_pi_config(pi_name, new_config_json)

    st.write('Something?')
