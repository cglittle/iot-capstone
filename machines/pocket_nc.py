
# Pocket NC CNC Machine

from fastapi import FastAPI, Request, Body
import numpy as np
from misc import SEC, MIN, HOUR
from misc import is_server
# TODO: Remote this, better to import when used in the class...
if is_server():
    # We are importing for the server...
    pass
else:
    # We are importing for the pi...
    pass
import time
import traceback
import numpy.linalg as la
import time
from threading import Thread, Semaphore
import configparser
import os
import os.path
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime
from misc import ARMED, DETONATE

machine_type = 'Pocket NC CNC'
name_prefix = 'CNC'

tags = ['cnc', 'machine']

WINDOW = 800  # 16000

class CurrentClampStatus:
    
    def __init__(self, influx_writer, camera_man, machine_info, config_options, *args):

        self.machine_name = machine_info['name']
        self.destruct = ARMED

        self.lastest_image = None
        self.influx_writer = influx_writer

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def run(self):

        # Current clamp
        import Adafruit_GPIO.SPI as SPI
        import Adafruit_MCP3008
        import numpy.fft 

        # Accelerometer
        import smbus


        SPI_PORT   = 0
        SPI_DEVICE = 0
        mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

        xdata, ydata = [], []
        fdata = []
        last_vals = []

        count = 0

        print_start_time = None
        prev_step = 0

        offset = 512

        t = time.time()
        while True:
            if count % 10 == 0:
                self.destruct_check()
            current_time = time.time()
            delay = 0.01 - (current_time - t)
            t = current_time
            if delay > 0 :
                time.sleep(delay)
                #self.destructible_sleep(delay)
            else:
                pass  # print(f'Something Wrong, {delay}')
            s = mcp.read_adc(7)
         
            # Get the new data
            xdata.append(count)
            ydata.append(s)
                
            if len(xdata) > WINDOW:
                xdata.pop(0)
                ydata.pop(0)
                fdata.append(numpy.max(ydata[-50:]))   
                if len(fdata) > WINDOW:
                    fdata.pop(0)
                 
            PLOT_RAW = False
            if count%WINDOW == 0 and count > WINDOW:
                # Reset the data in the plot
                if PLOT_RAW:
                    tydata = ydata
                else:
                    tydata = []
                    if ydata:
                    
                        tydata = np.abs(numpy.fft.fft(np.array(ydata)- offset))
                        tydata[0:180] = 0
                        tydata[-180:] = 0
                        max_current = np.max(tydata)

                        print('Sub point:', max_current)
                        last_vals.append(max_current)
                        if len(last_vals) > 5:
                            last_vals.pop(0)
                        else:
                            count += 1
                            continue
                        max_current = sum(last_vals)/len(last_vals)

                        print('Max current:', max_current)
                        step = -1
                        if max_current > 30000:  #42000:  # 1000
                            step = 4
                        elif max_current > 20000:
                            step = 0


                        p = influxdb_client.Point(self.influx_writer.measurement_name) \
                            .tag("Name", self.machine_name) \
                            .field("Step", step)\
                            .field("Current", max_current)

                        if print_start_time is not None:
                            p = p.field("Elapsed Time", round(time.time()-print_start_time))
                        if prev_step == 0 and step == 4:
                            p = p.field('Job Start Event', 1)
                            print_start_time = time.time()
                        else:
                            p = p.field('Job Start Event', 0)
                        if prev_step == 4 and step == 0:
                            p = p.field('Job Finish Event', 1)
                            print_start_time = None
                        else:
                            p = p.field('Job Finish Event', 0)

                        self.influx_writer.write_influx(p)
                        print(f'This is the step: {step}')

                        prev_step = step
                        
                      
            count += 1

# Some MPU6050 registers and addresses
PWR_MGMT_1   = 0x6B
SMPLRT_DIV   = 0x19
CONFIG       = 0x1A
GYRO_CONFIG  = 0x1B
INT_ENABLE   = 0x38
ACCEL_XOUT_H = 0x3B
ACCEL_YOUT_H = 0x3D
ACCEL_ZOUT_H = 0x3F
GYRO_XOUT_H  = 0x43
GYRO_YOUT_H  = 0x45
GYRO_ZOUT_H  = 0x47

class AccelerometerData:
    
    def __init__(self, influx_writer, camera_man, machine_info, config_options, *args):

        self.machine_name = machine_info['name']
        self.destruct = ARMED

        self.lastest_image = None
        self.influx_writer = influx_writer

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def initialise_accelerometer(self):
        import smbus

        self.device_address = 0x68  # For MPU6050
        self.bus = smbus.SMBus(1)

        # write to sample rate register
        self.bus.write_byte_data(self.device_address, SMPLRT_DIV, 7)
        # Write to power management register
        self.bus.write_byte_data(self.device_address, PWR_MGMT_1, 1)
        # write to configuration register
        self.bus.write_byte_data(self.device_address, CONFIG, 0)
        # Write to Gyro configuration register
        self.bus.write_byte_data(self.device_address, GYRO_CONFIG, 24)
        # write to interrupt enable register
        self.bus.write_byte_data(self.device_address, INT_ENABLE, 1)

    def read_raw_value(self, addr):
        high = self.bus.read_byte_data(self.device_address, addr)
        low = self.bus.read_byte_data(self.device_address, addr+1)

        # Concatenate higher and lower value
        value = (high << 8) | low

        # Convert to signed value
        if value > 32768:
            value = value - 65536

        return value

    def read_raw_values(self, addrs):
        return [self.read_raw_value(addr) for addr in addrs]

    def read_accelerometer(self):
        acc = [acc_val/16384 for acc_val in self.read_raw_values([ACCEL_XOUT_H, ACCEL_YOUT_H, ACCEL_ZOUT_H])]
        gyro = [gyro_val/131 for gyro_val in self.read_raw_values([GYRO_XOUT_H, GYRO_YOUT_H, GYRO_ZOUT_H])]
        return acc, gyro

    def run(self):

        print('Initialising MPU6050 accelerometer...')
        self.initialise_accelerometer()
        print('Accelerometer initialised!')

        while True:
            (acc_x, acc_y, acc_z), (gyro_x, gyro_y, gyro_z) = self.read_accelerometer()

            acc = (acc_x**2 + acc_y**2 + acc_z**2)**0.5

            p = influxdb_client.Point(self.influx_writer.measurement_name) \
                .tag('Name', self.machine_name) \
                .field('Ax', acc_x) \
                .field('Ay', acc_y) \
                .field('Az', acc_z) \
                .field('A', acc)

            self.influx_writer.write_influx(p)

            # time.sleep(1)
            self.destructible_sleep(1)

# These run as separate programs on the pi...
pi_programs = {'cnc_current_clamp': CurrentClampStatus, 'cnc_accelerometer': AccelerometerData}

pi_apt_reqs = []
pi_pip_reqs = []


class MachineOff:
    def __init__(self, influx_writer, image_streaming_wrangler, pi_name, machine_info, config_options, found_machines, found_pis, *args):
        machine_name = machine_info['name']
        self.destruct = ARMED

        self.influx_writer = influx_writer
        self.machine_name = machine_name
        self.isw = image_streaming_wrangler

        self.found_machines = found_machines
        self.found_pis = found_pis

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def run(self):
        while True:
            print(f'Checking if {self.machine_name} is off...')

            printer_is_on = self.machine_name in self.found_machines

            if True or not printer_is_on:

                print('Off machine:', self.machine_name, 'has had job event 0.')
                p = influxdb_client.Point(self.influx_writer.measurement_name) \
                        .tag("Name", self.machine_name) \
                        .field("Print Event", 0) \
                        .field("Step", -1)
                p = p.field('Job Start Event', 0)
                p = p.field('Job Finish Event', 0)
                # Send that the printer
                self.influx_writer.write_influx(p)

            else:
                print('Skipped printer', self.machine_name)

            self.destructible_sleep(0.5*HOUR)
                    

server_programs = {'machine_off': MachineOff}

server_apt_reqs = []
server_pip_reqs = []

def pdf_report_section():
    pass

def pi_manager_backend_api_callback(app, rpi_api):
    pass

def pi_server_api_callback(app, psapi):
    pass

def setup_streamlit_page(st, mb, config, pi_name, machine_name, machine_type, dash_name, row):
    st.write('Hi')

    pass

