# One file to rule them all

from misc import SEC, MIN, HOUR
from misc import is_server
if is_server():
    # We are importing for the server...
    pass
else:
    # We are importing for the pi...
    pass
import influxdb_client
from influxdb_client.client.influxdb_client_async import InfluxDBClientAsync
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime
import random

# Eventually everything relating to this printer will go here.


# Do not include spaces (some .splits() in places grab the first word by spaces, and expect it to be this...)
# name_prefix = 'Printer'

#machine_type = 'Printer'


# Thse tags can be referenced in dashboard modifications to filter machines to display and grab data for in specific panels.
tags = ['printer']

ARMED = False
DETONATE = True

# Need code for modifying the main dashboard panels related to these machines.
# (what about other kinds of printers, should there be categories? I would prefer something more dynamic, like tag classes that deal with panels that can group the machines in arbitrary, including overlapping, ways)

# Should be provided by machines with tag 'printer', so that the homepage can display all of these queries
'''
def printer_stat_query():
    pass
'''

class CollectData:

    def __init__(self, influx_writer, camera_man, machine_info, config_options):

        machine_name = machine_info['name']
        self.destruct = ARMED

        self.influx_writer = influx_writer
        self.machine_name = machine_name

        pass

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def run(self):
   
        while True:
            print(f'Collecting some data for {self.machine_name}...')
            p = influxdb_client.Point(self.influx_writer.measurement_name) \
                    .tag('Name', self.machine_name) \
                    .field('Sensor Reading', random.random())
                    

            self.influx_writer.write_influx(p)
            pass
            self.destructible_sleep(1)

            # TODO: Exemplify making streams, and reading camera streams

class ServerCheck:

    def __init__(self, influx_writer, image_streaming_wrangler, pi_name, machine_info, config_options):
        machine_name = machine_info['name']
        self.destruct = ARMED

        self.influx_writer = influxdb_wrtier
        self.machine_name = machine_name
        self.isw = image_streaming_wrangler
        
        pass

    # TODO: Make this an inherited method!
    def destruct_check(self):
        if self.destruct:
            raise SelfDestructException('This program has detonated the self destruct!')

    # TODO: Make this an inherited method!
    def destructible_sleep(self, t):
        start_t = time.time()
        while time.time() - start_t < t:
            self.destruct_check()
            time.sleep(0.0001)

    def run(self):
        while True:
            print(f'Checking something for {self.machine_name}...')
            pass
            self.destructible_sleep(30)


def pi_manager_backend_api_callback(app, rpi_api):
    # Note there will be no mb.thing for it, so the requests session post will need to be performed.
    # (or an mb.named_post('name_here', ...) function to do that?)
    @app.post('/custom/thing')
    def func(params : dict = Body(...)):
        pi_name = params['parent']
        response = rpi_api.post(pi_name, '/propagated/custom/thing', params={}, filename=None, filestream=None, filecontent=None)
        pass
        return {}

def pi_server_api_callback(app):
    @app.post('/propagated/custom/thing')
    def func(params):
        params = params['data']
        pass
        return {}

'''
def initial_setup():
    pass
'''

# TODO: Initial setup callback in machine module to do things like prompt to user to hit enter and then store the authentication code.
#       (if there are steps to follow, the user needs to receive the instructions somewhere...)
def setup_streamlit_page(mb, pi_name, machine_name, machine_type, dash_name, row):
    pass
    result = mb.named_post('/custom/thing', json={})

pi_programs = {'collect_data': CollectData}

pi_apt_reqs = []
pi_pip_reqs = []

server_programs = {'server_check': ServerCheck}

server_apt_reqs = []
server_pip_reqs = []

