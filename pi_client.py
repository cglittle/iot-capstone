# NOTE: This will be removed soon, the previous function this served is now served by pi_server_api.py

import socket
import sys
import os
import os.path
#import configparser
import time
import traceback
import json

this_path = os.path.dirname(os.path.abspath(__file__))
os.chdir(this_path)

# Connect the socket to the port where the server is listening
# TODO: Load this ip from a config file that is uploaded to the pi!
server_address = ('192.168.0.219', 10000)

#printer_info = configparser.ConfigParser()
#printer_info.read('/home/capstone2023/Desktop/printer_info.ini')
#machine_name = printer_info['default']['printer_name']

MACHINE_FILE = 'machine_info.json'

def load_machines_file():
    with open(MACHINE_FILE) as f:
        content = f.read()
        machine_info = json.loads(content)

    if 'machines' not in machine_info:
        machine_name = machine_info['name']
        machine_type = machine_info['type']
        machine_info = {'name': f'Pi {machine_name}', 'machines': [machine_info]}
        
        with open(MACHINE_FILE, 'w') as f:
            f.write(json.dumps(machine_info, indent=4))
        print('Wrote new machine info!')

    return machine_info

machine_info = load_machines_file()

def pi_beacon():
    while True:
        try:
            # Create a TCP/IP socket
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print('connecting to {} port {}'.format(*server_address))
            sock.connect(server_address)
            # Send data
            print('connected')
            #message = f'{machine_name}\n'
            message = json.dumps(machine_info)
            sock.sendall(message.encode('utf8') + b'\n')

            # Look for the response
            #amount_received = 0
            #amount_expected = len(message)

            #while amount_received < amount_expected:
            #    data = sock.recv(16)
            #    amount_received += len(data)
            #    print('received {!r}'.format(data))
        except Exception as e:
            print(traceback.format_exc())
        finally:
            print('closing socket')
            sock.close()
        time.sleep(10)


if __name__ == '__main__':
    pass  # NOTE: This file has been disabled.
    # pi_beacon()

